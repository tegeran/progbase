
// Copyright © 2017 Vitaly Kryvenko, all rights reserved.
// USER FRIENDLY INTERFACE NCURSES DEMANDING FUNCTIONS

#pragma once

#include <ncurses.h>
#include <assert.h>
#include <ncursesw/curses.h>

#define UNICODE_CHAR_SIZE sizeof("╣")

struct __FrameSetup {
    char horizontal [UNICODE_CHAR_SIZE];
    char vertical   [UNICODE_CHAR_SIZE];
    //
    char leftUpper  [UNICODE_CHAR_SIZE];
    char rightUpper [UNICODE_CHAR_SIZE];
    //
    char leftLower  [UNICODE_CHAR_SIZE];
    char rightLower [UNICODE_CHAR_SIZE];
};


const struct __FrameSetup MY_OWN_IMMUTABLE_FRAME_SETUP = {
    .horizontal = "═",    
    .vertical   = "║",      
    .leftUpper  = "╔",     
    .rightUpper = "╗",    
    .leftLower  = "╚",     
    .rightLower = "╝"
};


