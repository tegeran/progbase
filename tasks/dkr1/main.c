#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <progbase/console.h>


typedef struct __Paragraph Paragraph;          //renaming structs for conveniency
typedef struct __ParagraphArray ParagraphArray;


#define PARAGRAPH_STR_LEN  0xfff  // Paragraph.string[PARAGRAPH_STR_LEN] - hardcoded constant
                                  // used as a string's length in struct Paragraph

#define CALLOC(quaintity, dataType)      \
    calloc((quaintity), sizeof(dataType))


#define REALLOC(pointer, quantity, dataType)         \
    realloc((pointer), (quantity) * sizeof(dataType))


//__________________________________FUNCTION_PROTOTYPES_AND_STRUCTS__________________________________
// TASK 1 vvvvvvvvvvvvvvvv

// returns second paragraph index or
// -1 if second paragraph is absent in string
int secondParagraphIndex(const char string[]);


// writes the first paragraph from string into buffer
// if BUFFERSIZE is not enough for the paragraph puts buffer[0] = '\0'
void firstParagraphToBuffer(const char string[], char buffer[], const unsigned int BUFFERSIZE);


// TASK 2 vvvvvvvvvvvvvvvv

struct __Paragraph {
    int index;
    char string[PARAGRAPH_STR_LEN];
};


struct __ParagraphArray {
    Paragraph * array;
    int arrSize;
};


// compares 2 Paragraphs
// returns 1 if they are identical or 0 if not
int parCmp(const Paragraph par1, const Paragraph par2);


// compares 2 ParagraphArrays
// returns 1 if they are identical or 0 if not
int parArrCmp(const ParagraphArray parArray1, const ParagraphArray parArray2);
Paragraph firstParagraph(const char string[]);


// TASK 3 vvvvvvvvvvvvvvvv

// AHTUNG!!! RETURNS POINTER ON DYNAMIC MEMORY, NEEDED TO BE FREED AFTER USAGE!!!
// returns a pointer on struct ParagraphArray which
// contains an information about all the paragraphs in str
ParagraphArray * getAllParagraphsArrayNew(const char * str);
void arrayFree(ParagraphArray * array);


// MY ADDITIONAL FUNCTIONS AND FUNCTIONS BY RUSLAN ANATATOLIYOVYCH vvvvvvvvvvvvvvvv
long getFileSize(const char *fileName);


// validates that function getAllParagraphsArrayNew(string)
// works properly according to string and parArr2 arguments (task3)
void task3AssertionSample(ParagraphArray * pParArr1, ParagraphArray parArr2, char * string);


// prints formated string using argv in order to
// type a path to a txt file in the message (task4)
void printTask4(const char * string, const char * argv);


// if pointer is NULL executes callBackFunction(cbArgument)
// and then exits programme with EXIT_FAILURE
// if you have no need in callBackFunctiont therefore put NULL for it and for cbArgument
void checkPointer(void * pointer, void(*callBackFunction)(void*), void * cbArgument);
//______________________________________________________________________________________________


int main(int argc, char *argv[]){
    if (argc != 2 && argc != 3){
        return EXIT_FAILURE;
    }
    char isTask[] = "task";
    int i = 0;
    for (; isTask[i] != '\0'; i++){    // checking wether "task" is present in argv[1]
        if (isTask[i] != argv[1][i]){
            return EXIT_FAILURE;
        }
    }
    if (argc == 2){ // checking wether it is not task4
        switch (argv[1][i]){ // choosing a task depending on the digit after "task_"
            case ('1'):{                // TASK 1 vvvvvvvvvvvvvvvv
                assert(secondParagraphIndex("L1 par \n\nSecond paragraph\n\nThird paragraph") == 9);
                assert(secondParagraphIndex("EFirst paragraph \n and no second one") == -1);
                assert(secondParagraphIndex("G 1 par\n\n") == -1);
                assert(secondParagraphIndex("E \n \n \n\n_") == 8);
                assert(secondParagraphIndex("Ztest \n\n\n abc") ==  8);
                assert(secondParagraphIndex("Atest \n\n\n\n test") == -1);
                assert(secondParagraphIndex("test \n\n_\n\n test") == 7);
                assert(secondParagraphIndex("") == -1);
                assert(secondParagraphIndex("\n\nSecond paragraph") ==  2);
                //---------------------------------------
                const unsigned int BUFFERSIZE = 20;
                char buffer[BUFFERSIZE];
                firstParagraphToBuffer("First paragraph 2\n\nSecond paragraph\n\nThird paragraph", buffer, BUFFERSIZE);
                assert(!strcmp("First paragraph 2", buffer));
                //
                firstParagraphToBuffer("First paragraph \n", buffer, BUFFERSIZE);
                assert(!strcmp("First paragraph \n", buffer));
                //
                firstParagraphToBuffer("\n\nSecond paragraph", buffer, BUFFERSIZE);
                assert(!strcmp("", buffer));
                //
                firstParagraphToBuffer("\n\n", buffer, BUFFERSIZE);
                assert(!strcmp("", buffer));
                //
                firstParagraphToBuffer("", buffer, BUFFERSIZE);
                assert(!strcmp("", buffer));
                //
                firstParagraphToBuffer("long long long paragraph is too long for \n\n buffer", buffer, BUFFERSIZE);
                assert(!strcmp("", buffer));
                //
                // next 3 cases check nearby limits for owerflowing buffer
                // when we have noting but one paragraph in string
                firstParagraphToBuffer("strlen == bufSize-1", buffer, BUFFERSIZE);
                assert(!strcmp("strlen == bufSize-1", buffer));
                //
                firstParagraphToBuffer("strlen == BUFFERSIZE", buffer, BUFFERSIZE);
                assert(!strcmp("", buffer));
                //
                firstParagraphToBuffer("strlen == buffrSize+1", buffer, BUFFERSIZE);
                assert(!strcmp("", buffer));
                //
                // next 3 cases check nearby limits for owerflowing buffer
                // when we have several paragraphs in string
                firstParagraphToBuffer("firstPar == bufSz-1\n\n dich diiiiich ditch", buffer, BUFFERSIZE);
                assert(!strcmp("firstPar == bufSz-1", buffer));
                //
                firstParagraphToBuffer("firstPar == bufrSize\n\n dich", buffer, BUFFERSIZE);
                assert(!strcmp("", buffer));
                //
                firstParagraphToBuffer("firstPar == bufSize+1\n\n ditch ditch", buffer, BUFFERSIZE);
                assert(!strcmp("", buffer));
                return EXIT_SUCCESS;
            }
            case ('2'):{                // TASK 2 vvvvvvvvvvvvvvvv
                Paragraph par1 = {4, "Comparison"};
                Paragraph par2 = {4, "Comparison"};
                assert(parCmp(par1, par2));
                //
                par1.index = 3;
                assert(!parCmp(par1, par2));
                //
                par1.index = 4;
                strcpy(par1.string, "1Comparison");
                assert(!parCmp(par1, par2));
                //
                strcpy(par2.string, "");
                assert(!parCmp(par1, par2));
                strcpy(par1.string, "");
                assert(parCmp(par1, par2));
                //---------------------------------------
                Paragraph par3 = {4, ""};
                Paragraph par4 = {4, ""};
                Paragraph par5 = {4, "28"};
                ParagraphArray parArr1 = {CALLOC(3, Paragraph), 0};
                checkPointer(parArr1.array, NULL, NULL);
                ParagraphArray parArr2 = {CALLOC(3, Paragraph), 0};
                checkPointer(parArr2.array, free, parArr1.array);
                assert(parArrCmp(parArr1, parArr2));
                parArr1.array[0] = par1;
                parArr1.array[1] = par2;
                parArr2.array[0] = par3;
                parArr2.array[1] = par4;
                parArr1.arrSize = 2;
                parArr2.arrSize = 2;
                assert(parArrCmp(parArr1, parArr2));
                //
                parArr1.arrSize = 3;
                assert(!parArrCmp(parArr1, parArr2));
                //
                parArr1.arrSize = 2;
                strcpy(parArr2.array[1].string, "Test");
                assert(!parArrCmp(parArr1, parArr2));
                //
                strcpy(parArr1.array[1].string, "Test");
                assert(parArrCmp(parArr1, parArr2));
                //
                parArr2.array[2] = par5;
                parArr2.arrSize = 3;
                assert(!parArrCmp(parArr1, parArr2));
                //------------------------------------------
                par1.index = 0;
                strcpy(par1.string, "Paragraph1");
                par2 = firstParagraph("Paragraph1\n\nParagraph2");
                // ternary operator inside an assertion to check two
                // different cases when hardcoded Paragraph.string length
                // is not enough for paragraph 
                // (just change PARAGRAPH_STR_LEN in order to test it)
                 assert((PARAGRAPH_STR_LEN > strlen(par1.string)) ? parCmp(par1, par2) : !parCmp(par1, par2));
                //
                par1.index = 2;
                assert(!parCmp(par1, firstParagraph("Paragraph1\n\nParagraph2")));
                //
                par1.index = 0;
                strcpy(par1.string, "Par1");
                assert(!parCmp(par1, firstParagraph("Paragraph1\n\nParagraph2")));
                assert((PARAGRAPH_STR_LEN > strlen(par1.string)) ? parCmp(par1, firstParagraph("Par1")) : !parCmp(par1, firstParagraph("Par1")));
                //
                strcpy(par1.string, "");
                assert(parCmp(par1, firstParagraph("\n\n\n")));
                assert(parCmp(par1, firstParagraph("")));
                assert(parCmp(par1, firstParagraph("\n\n")));
                assert(parCmp(par1, firstParagraph("\n\n dich ddddich")));
                //
                strcpy(par1.string, "\n ");
                assert(parCmp(par1, firstParagraph("\n \n\n")));
                free(parArr1.array);
                free(parArr2.array);
                return EXIT_SUCCESS;
            }
            case ('3'):{                // TASK 3 vvvvvvvvvvvvvvvv
                ParagraphArray parArr2;
                parArr2.array = CALLOC(4, Paragraph);
                checkPointer(parArr2.array, NULL, NULL);
                ParagraphArray * pParArr1 = NULL;         // testing pParArr1 = getAllParagraphsArrayNew(string)
                char string[] = "Portugal has had problems with forest fires this year.\n" 
                        "More than a third of forest\n fires in the EU all happened in Portugal.\n\n";
                parArr2.array[0] = firstParagraph(string);
                parArr2.arrSize = 1;
                task3AssertionSample(pParArr1, parArr2, string);
                //
                strcpy(string, "Test \n\n\n\n sest\\n\\n \n\n\n\n");
                parArr2.array[0] = firstParagraph(string);
                parArr2.array[1].index = 7;
                parArr2.array[1].string[0] = '\0';
                strcpy(parArr2.array[2].string, " sest\\n\\n ");
                parArr2.array[2].index = 9;
                parArr2.array[3].index = 21;
                parArr2.array[3].string[0] = '\0';
                parArr2.arrSize = 4;
                task3AssertionSample(pParArr1, parArr2, string);                               
                //
                strcpy(string, "\n\n");
                parArr2.array[0] = firstParagraph(string);
                parArr2.arrSize = 1;
                task3AssertionSample(pParArr1, parArr2, string);
                //
                strcpy(string, "");
                parArr2.array[0] = firstParagraph(string);
                task3AssertionSample(pParArr1, parArr2, string); 
                //
                assert (getAllParagraphsArrayNew(NULL) == NULL);
                //
                strcpy(string, "\n");
                parArr2.array[0] = firstParagraph(string);
                task3AssertionSample(pParArr1, parArr2, string);
                //----------------------------------------
                // there is actualy no need to test it so much
                // it is built on the same principes as previously tested functions
                free(parArr2.array);
                return EXIT_SUCCESS;
            }
            default:{
                return EXIT_FAILURE;
            }
        }
    }
                   // TASK 4 vvvvvvvvvvvvvvvv      
    if (argv[1][i] != '4'){
        return EXIT_FAILURE;
    }
    FILE * txt = fopen(argv[2], "r");
    checkPointer(txt, NULL, NULL); // checking wether a file exists or available
    long txtSize = getFileSize(argv[2]);
    if (txtSize == -1){
        fclose(txt);
        return EXIT_FAILURE;
    }
    char txtInString[txtSize + 1];  // leaving extra(+ 1) space for '\0'
    if (fread(&txtInString, 1, txtSize, txt) != txtSize){  // writing text from the file into a 'txtInString'
        fclose(txt);
        return EXIT_FAILURE;                               
    }
    txtInString[txtSize] = '\0';
    fclose(txt);
    printTask4("TASK 4.1(Outputting a text from \"%s\" allocating pagraphs with particular color):", argv[2]);
    ParagraphArray * text = getAllParagraphsArrayNew(txtInString);
    checkPointer(text, NULL, NULL);
    for (int parIndx = 0; parIndx < text->arrSize; parIndx++){
        if (parIndx != 0){ // separating paragraphs one from another
            puts("");
        }
        Console_setCursorAttributes(FG_BLACK); // for better experience
        Console_setCursorAttributes((strlen(text->array[parIndx].string) < 100) ? BG_RED :
                                    (strlen(text->array[parIndx].string) <= 150) ? BG_YELLOW : BG_GREEN );
        puts(text->array[parIndx].string);
        Console_reset();
    } 
    printTask4("TASK 4.2(Outputting a text from \"%s\" skipping paragraphs, which have less than 100 symbols):", argv[2]);
    const int OUTPUT_PARS_GREATER_THAN = 100; // task 4 condition
    short needSpace = 0;          // for avoiding extra space before the paragraph 
    for (int parIndx = 0; parIndx < text->arrSize; parIndx++){
        if (strlen(text->array[parIndx].string) >= OUTPUT_PARS_GREATER_THAN){
            if (needSpace){
                puts("");
            }
            puts(text->array[parIndx].string);
            needSpace = 1;
        }
    }
    arrayFree(text);
    return EXIT_SUCCESS;
}

//__________________________________FUNCTION_BODIES__________________________________
// TASK 1 vvvvvvvvvvvvvvvv
int secondParagraphIndex(const char string[]){
    for (int i = 0; string[i] != '\0'; i++){
        if (string[i] == '\n' && string[i + 1] == '\n' && string[i + 2] != '\0'){  //case "...\n\n\0"
            return(string[i + 2] != '\n' || string[i + 3] != '\n') ?(i + 2) : -1;  //case "...\n\n\n\n..."
        }
    }
   return -1;
}

void firstParagraphToBuffer(const char string[], char buffer[], const unsigned int bufferSize){
    int i = 0;
    while (string[i] != '\0' && !(string[i] == '\n' && string[i + 1] == '\n') && i < bufferSize){
        buffer[i] = string[i];
        i++;
    }
    if (i == bufferSize){ // if paragraph is too long for buffer
        i = 0;            // return blank buffer(with '\0' at buffer[0])
    }
    buffer[i] = '\0';
}

// TASK 2 vvvvvvvvvvvvvvvv
int parCmp(const Paragraph par1, const Paragraph par2){
    return (par1.index == par2.index && !strcmp(par1.string, par2.string)) ? 1 : 0;
}

int parArrCmp(const ParagraphArray parArray1, const ParagraphArray parArray2){
    if (parArray1.arrSize == parArray2.arrSize){
        for (int i = 0; i < parArray1.arrSize; i++){
            if (!parCmp(parArray1.array[i], parArray2.array[i])){
                return 0;
            }
        }
        return 1;
    }
    return 0;
}

Paragraph firstParagraph(const char string[]){
    Paragraph parOutput;
    parOutput.index = 0;
    firstParagraphToBuffer(string, parOutput.string, PARAGRAPH_STR_LEN);
    return parOutput;
}

// TASK 3 vvvvvvvvvvvvvvvv
ParagraphArray * getAllParagraphsArrayNew(const char * str){
    if (str == NULL){
        return NULL;
    }
    ParagraphArray * parArr = CALLOC(1, ParagraphArray);
    if (parArr == NULL){
        return NULL;
    }
    int capacity = 16;
    parArr->array = CALLOC(capacity, Paragraph);
    if (parArr->array == NULL){
        free(parArr);
        return NULL;
    }
    int localStrIndex = 0;
    int strIndex = 0;
    int parIndex = 0;
    short isEndOfString = 1; //a condition to break the loop when reached the end of string
    for (;isEndOfString; parIndex++, localStrIndex = 0){
        while (str[strIndex] != '\0' &&(str[strIndex] != '\n' || str[strIndex + 1] != '\n')){
            parArr->array[parIndex].string[localStrIndex] = str[strIndex]; // filling the string with all
            localStrIndex++;                                               // the symbols untill have met "\n\n" or '\0'
            if (localStrIndex == PARAGRAPH_STR_LEN - 1){  // case allocated Paragraph.string length 
                arrayFree(parArr);                        // is not enough for paragraph
                return NULL;
            }
            strIndex++;
        }
        parArr->array[parIndex].string[localStrIndex] = '\0';
        parArr->array[parIndex].index = strIndex - localStrIndex;
        if (str[strIndex] != '\0' && str[strIndex + 2] != '\0'){ // checking if there is at least one non'\0' symbol after "\n\n"
            if (parIndex == capacity - 1){ // check if we need to reallocate more memory
                parArr->array = REALLOC(parArr->array, (capacity *= 2), Paragraph);
                if (parArr->array == NULL){
                    arrayFree(parArr);
                    return NULL;
                }
            }
            strIndex += 2; //+2 to skip "\n\n"
        }
        else {
            isEndOfString = 0; //instead of break statement, ASD doesn't approve it =(
        }
    }
    parArr->arrSize = parIndex;
    return parArr;
}

void arrayFree(ParagraphArray * array){
    if (array != NULL){
        if (array->array != NULL){
            free(array->array);
        }
    free(array);
    }
}

// MY ADDITIONAL FUNCTIONS AND FUNCTIONS BY RUSLAN ANATATOLIYOVICH vvvvvvvvvvvvvvvv
long getFileSize(const char *fileName) {
    FILE *f = fopen(fileName, "rb");
    if (!f) return -1;  // error opening file
    fseek(f, 0, SEEK_END);  // rewind cursor to the end of file
    long fsize = ftell(f);  // get file size in bytes
    fclose(f);
    return fsize;
}

void task3AssertionSample(ParagraphArray * pParArr1, ParagraphArray parArr2, char * string){
    pParArr1 = getAllParagraphsArrayNew(string);
    checkPointer(pParArr1, free, parArr2.array);
    assert(parArrCmp(*pParArr1, parArr2));             
    arrayFree(pParArr1);
}

void printTask4(const char * string, const char * argv){
    Console_setCursorAttributes(BG_WHITE);
    Console_setCursorAttributes(FG_BLACK);
    fflush(stdout);
    printf(string, argv);
    Console_reset();
    puts("\n");
}

void checkPointer(void * pointer, void(*callBackFunction)(void*), void* cbArgument){
    if (pointer == NULL){
        if (callBackFunction != NULL && (callBackFunction != free || cbArgument != NULL)){
            callBackFunction(cbArgument);
        }
        exit(EXIT_FAILURE);
    }
}
//__________________________________________________________________________________