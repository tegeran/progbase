#include <stdio.h>
#include <progbase.h>
#include <progbase/console.h>

int main(void){
    Console_clear();
    int x = 0, y = 0;
    for (; y <= 16; y++){
        for (x = 0; x <= 28; x++){
            Console_setCursorPosition(y,x);
            if (y == 3){
            Console_setCursorAttribute(BG_GREEN);
            }
            else{
                Console_setCursorAttribute(BG_INTENSITY_BLACK);
            }
            putchar(' ');
        }
    }
    for (x = 11; x<= 18; x++){
        Console_setCursorPosition(1,x);
        Console_setCursorAttribute(BG_GREEN);
        putchar(' ');
    }
    for (y = 2; y <= 7; y++){
        for (x = 10; x <= 19; x++){
            Console_setCursorPosition(y,x);
            putchar(' ');
        }
    }
    Console_setCursorPosition(4,4);
    putchar(' ');
    Console_setCursorPosition(4,25);
    putchar(' ');
    Console_setCursorPosition(5,7);
    putchar(' ');
    Console_setCursorPosition(5,22);
    putchar(' ');
    Console_setCursorPosition(15,9);
    putchar(' ');
    Console_setCursorPosition(9,6);
    putchar(' ');
    for (x = 5; x<= 24; x++){
        if (x <=9 || x >= 20){
            Console_setCursorPosition(4,x);
            Console_setCursorAttribute(BG_YELLOW);
            putchar(' ');
            if (x == 8 || x == 9 || x == 20 || x == 21){
                Console_setCursorPosition(5,x);
                putchar(' ');
            }
        }
    }
    for (y = 7; y <= 13; y++){
        for (x = 10; x <=19; x++){
            if (x <= 12 || x>= 17){
                Console_setCursorPosition(y,x);
                Console_setCursorAttribute(BG_WHITE);
                putchar(' ');
            }
            else{
                if (y != 7){
                    Console_setCursorPosition(y,x);
                    Console_setCursorAttribute(BG_BLACK);
                    putchar(' ');
                }
            }
        }
    }
    Console_setCursorPosition(11,13);
    putchar(' ');
    Console_setCursorPosition(11,16);
    putchar(' ');
    for (y = 10; y<= 15; y++){
        Console_setCursorPosition(y,6);
        Console_setCursorAttribute(BG_BLACK);
        putchar(' ');
    }
    for (y = 4; y<= 5; y++){
        for (x = 12; x <= 17; x += 5){
            Console_setCursorPosition(y,x);
            putchar(' ');
        }
    }
    for (y = 14; y<= 15; y++){
        for(x = 10; x<= 19; x++){
            if (x <= 13 || x>= 16){
                Console_setCursorPosition(y,x);
                Console_setCursorAttribute(BG_GREEN);
                putchar(' ');
            }
        }
    }
for (y = 9; y <= 10; y++){
    for (x = 7; x<= 21; x++){
        if (x <= 9 || x>= 20){
            Console_setCursorPosition(y,x);
            putchar(' ');
        }
    }
}
    Console_reset();
    Console_setCursorPosition(17, 1);
    return 0;
}
