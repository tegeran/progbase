#include <stdio.h>
#include <progbase.h>
#include <progbase/console.h>

int main(void) {
    // TASK 1
    /* colors encoding table */
    const char colorsTable[16][2] = {
        {'0', BG_BLACK},
        {'1', BG_INTENSITY_BLACK},
        {'2', BG_GREEN,},
        {'3', BG_CYAN},
        {'4', BG_INTENSITY_GREEN},
        {'5', BG_INTENSITY_MAGENTA},
        {'6', BG_YELLOW},
        {'7', BG_INTENSITY_YELLOW},
        {'8', BG_BLUE},
        {'9', BG_RED},
        {'A', BG_INTENSITY_RED},
        {'B', BG_MAGENTA},
        {'C', BG_INTENSITY_BLUE},
        {'D', BG_INTENSITY_CYAN},
        {'E', BG_WHITE},
        {'F', BG_INTENSITY_WHITE}
    };
    int colorsTableLength = sizeof(colorsTable) / sizeof(colorsTable[0]);
    char colorsPalette[] = "0123456789ABCDEF";
    int colorsPaletteLength = sizeof(colorsPalette) / sizeof(colorsPalette[0]);
    int i = 0;
    int colorPairIndex = 0;
    Console_clear();
    for (i = 0; i < colorsPaletteLength; i++)
    {
        char colorCode = '\0';
        char color = '\0';
        /* get current color code from colorsPalette */
        colorCode = colorsPalette[i];
        /* find corresponding color in table */
        for (colorPairIndex = 0; colorPairIndex < colorsTableLength; colorPairIndex++) 
        {
            char colorPairCode = colorsTable[colorPairIndex][0];
            char colorPairColor = colorsTable[colorPairIndex][1];
            if (colorCode == colorPairCode) 
            {
                color = colorPairColor;
                break;  /* we have found our color, break the loop */
            }
        }
        /* print space with founded color background */
        Console_setCursorAttribute(color);
        putchar(' ');
    }
    puts("");
    Console_reset();
    // END OF TASK 1

    // TASK 2
    const unsigned int size = 28;
    char image[28][28] = {
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { 'F','F','F','F','F','F','F','F','F','F','0','F','F','F','F','0','F','F','F','0','F','F','F','F','F','F','F','F' },
        { 'F','F','F','F','F','F','0','0','F','F','F','0','6','6','0','6','6','F','0','F','F','F','F','F','F','F','F','F' },
        { 'F','F','F','F','F','F','F','F','0','F','6','6','6','6','6','6','6','0','F','F','F','0','F','F','F','F','F','F' },
        { 'F','F','F','F','F','F','F','F','F','6','6','6','E','E','E','E','6','6','6','F','0','F','F','F','F','F','F','F' },
        { 'F','F','F','F','F','F','F','F','6','6','6','E','F','F','F','F','E','6','6','6','F','F','F','F','F','F','F','F' },
        { 'F','F','F','F','F','F','F','0','0','0','E','F','F','C','C','F','F','E','0','0','F','F','F','F','F','F','F','F' },
        { 'F','F','F','F','F','F','F','0','0','0','E','F','F','C','C','F','F','E','0','0','F','F','F','F','F','F','F','F' },
        { 'F','F','F','F','F','F','F','6','6','6','6','E','F','F','F','F','E','6','6','6','F','F','F','F','F','F','F','F' },
        { 'F','F','F','F','F','F','F','6','6','6','6','6','E','E','E','E','6','6','6','6','F','F','6','6','F','F','F','F' },
        { 'F','F','F','F','F','6','6','6','6','6','7','6','6','6','6','6','6','7','6','6','F','F','6','6','F','F','F','F' },
        { 'F','F','F','F','6','6','F','8','8','6','6','7','7','7','7','7','7','6','8','8','6','6','6','F','F','F','F','F' },
        { 'F','F','F','6','6','F','F','6','6','8','6','6','6','6','6','6','6','8','6','6','F','F','F','F','F','F','F','F' },
        { 'F','F','F','6','6','F','F','8','8','8','8','8','8','8','8','8','8','8','8','8','F','F','F','F','F','F','F','F' },
        { 'F','F','F','F','F','F','F','8','8','8','8','8','D','D','D','8','8','8','8','8','F','F','F','F','F','F','F','F' },
        { 'F','F','F','F','F','F','F','F','8','8','8','8','8','8','8','8','8','8','8','F','F','F','F','F','F','F','F','F' },
        { 'F','F','F','F','F','F','F','F','F','F','F','0','F','F','F','0','F','F','F','F','F','F','F','F','F','F','F','F' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' },
        { '1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1' }
    };
    for (int i = 0; i < size; i++){
        for (int j = 0; j < size; j++){
            for (int k = 0; k < 16; k++){
                if (image[i][j] == colorsTable[k][0]){
                    Console_setCursorAttribute (colorsTable[k][1]);
                    break;
                }
            }
            putchar (' ');
        }
        putchar ('\n');
    }
    // END OF TASK 2

    // TASK 3
    const unsigned long int delay = 15;
    int j = 0;
    for (; j < size; j++){
        for (i = (j % 2 == 0) ? size - 1 : 0; i >= 0 && i < size; i = (j % 2 == 0) ? (i - 1) : (i + 1)){
            for (int k = 0; k < 16; k++){
                if (image[i][j] == colorsTable[k][0]){
                    Console_setCursorAttribute (colorsTable[k][1]);
                    break;
                }
            }
            Console_setCursorPosition(i + 2,j + 31);
            sleepMillis(delay);
            putchar (' ');
        }
    }
    // END OF TASK 3

    // TASK 4
    for (i = 0; i < size; i++){
        for (j = 0; j < size>>1; j++){
            int temporary = image[i][j];
            int i_updated = size - 1 - i;
            int j_updated = size - 1 - j;
            image[i][j] = image[i_updated][j_updated];
            image[i_updated][j_updated] = temporary;
        }
    }
    for (i = 0; i < size; i++){
        for (j = 0; j < size; j++){
            for (int k = 0; k < 16; k++){
                if (image[i][j] == colorsTable[k][0]){
                    Console_setCursorAttribute (colorsTable[k][1]);
                    break;
                }
                Console_setCursorPosition (i + 2, j + 61);
            }
            putchar (' ');
        }
    }
    putchar ('\n');
    Console_reset();
    // THE END))
    return 0;
}