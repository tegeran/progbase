#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <math.h>


#define ALPHABET_SIZE 26
#define ALPHABET_START 'A'
#define GCD_ARR_SIZE 5000 // size of array which contains current gcds for a paticular iteration of cycle (getKeyLen())
#define GCD_STAT_SIZE 50000 // size of array which under which indexes the quantity of identical gcds are written (getKeyLen())

const double LETTER_FREQUENCY[] = { // each index corresponds to according letter in the alphabet
    8.167,
    1.492,
    2.782,
    4.253,
    12.70,
    2.228,
    2.015,
    6.094,
    6.966,
    0.153,
    0.772,
    4.025,
    2.406,
    6.749,
    7.507,
    1.929,
    0.095,
    5.987,
    6.327,
    9.056,
    2.758,
    0.978,
    2.361,
    0.150,
    1.974,
    0.074,
};

enum __Option {
    KEY          = 'k',
    //
    ENCODE       = -1 ,
    DECODE       = 'd',
    //
    FROM_FILE    = 'f',
    //
    TO_CONSOLE   = -2 ,
    TO_FILE      = 'o'
};
typedef enum __Option Option; 

// ___________________________FUNCTIONS___________________________

// returns EXIT_FAILURE if value is 0
// and does nothing if not
// NOTE: value can be a result of logical operation
void check(const int value);


long getFileSize(const char *fileName); //precipitously stole RA function


// rewrites from string/file to buffer formated string
// FORMAT: deletes all nonalphabetic symbols and
// provides all alphabetic number being uppercase
void getProperFormatStringFromString(const char * string, char * buffer, const int bufferSize);
void getProperFormatStringFromFile(const char * fileName, char * buffer, const int bufferSize);


// encodes/decodes a string in (bufferToReadnWrite) and writes it in the same (bufferToReadnWrite)
void encodeByKeyInPlace(const char * key, char * bufferToReadnWrite, const long bufferSize, const int keySize);
void decodeByKeyInPlace(const char * key, char * bufferToReadnWrite, const long bufferSize, const int keySize);


// returns the most probable keyLen according to a (chipherString) chiphered by Vigenere chiper
int getKeyLen(const char * cipherString, const int cipherStringSize);


// writes the most probable key into a keyBuffer using the (keyLen)
void getKeyByKeyLen(char * keyBuffer, const int keyLen, const char * cipherString, const int cipherStringSize);


// sets all elements of (array) to zero
void setZeroArr(int * array, const int arraySize);


// return the index of the maximal element of (array)
int getMaxIndex(const int * array, const int arraySize);


// returns the greatest common divisor of two integers;
int gcd(int a, int b);


// consistently writes all the combinations of gcds of (array[]) elements into (gcdBuffer)
// and returns the quantity of written into (gcdBuffer) elements
int getArrGCDs(const int * array, const int arraySize, int * gcdBuffer, const int gcdBufferSize);

// _______________________________________________________________

// QUICK NOTE: (...Size) variables count '\0', (...Len) variables don't
int main(int argc, char * argv[]){
    check(argc > 1);
    Option mode = ENCODE; // determines whether text is to be encoded or decoded
    Option output = TO_CONSOLE;
    char * text = NULL;
    char * key = NULL;
    char * inputFileName = NULL;
    for (unsigned short arg = 1; arg < argc; arg++){
        if (argv[arg][0] == '-' && strlen(argv[arg]) == 2){
            switch (argv[arg][1]){
                case (KEY):{
                    check(key == NULL && argc >= arg + 2); // checking if arg is not repeating and an arg after it exists
                    if (strlen(argv[++arg]) == 2){
                        check(argv[arg][0] != '-');
                    }
                    key = argv[arg]; // skipping next argument as known to be a key string
                    break;
                }
                case (DECODE):{
                    check(mode != DECODE); //checking if arg is not repeating
                    mode = DECODE;
                    break;
                }
                case (TO_FILE):{
                    check(output == TO_CONSOLE && argc >= arg + 2);
                    if (strlen(argv[++arg]) == 2){
                        check(argv[arg][0] != '-');
                    }
                    check(freopen(argv[arg], "w", stdout) != NULL);
                    break;
                }
                case (FROM_FILE):{
                    check(inputFileName == NULL && argc >= arg + 2);
                    if (strlen(argv[++arg]) == 2){
                        check(argv[arg][0] != '-');
                    }
                    inputFileName = argv[arg];
                    break;
                }
                default:{
                    return EXIT_FAILURE;
                }
            }
        }
        else{
            check(text == NULL);
            text = argv[arg];
        }
    }
    check((text == NULL) ^ (inputFileName == NULL));
    long textLen = (inputFileName == NULL) ? strlen(text) : getFileSize(inputFileName);
    check(textLen > 0);
    char resString[textLen + 1];
    if (inputFileName == NULL){
        getProperFormatStringFromString(text, resString, textLen + 1);
    }
    else {
        getProperFormatStringFromFile(inputFileName, resString, textLen + 1);
    }
    check(resString[0] != '\0');
    if (key != NULL){
        int keySize = strlen(key) + 1;
        char properKey[keySize];
        getProperFormatStringFromString(key, properKey, keySize);
        if (mode == ENCODE) {
            encodeByKeyInPlace(properKey, resString, textLen + 1, keySize);
        }
        else {
            decodeByKeyInPlace(properKey, resString, textLen + 1, keySize);
        }
        puts(properKey);
    }
    else if (mode == DECODE){
        int resStrSize = strlen(resString) + 1;
        int keyLen = getKeyLen(resString, resStrSize);
        if (keyLen == 0){
            fprintf(stderr, "ERORR: Unable to decode the text.\nPossible reasons:\n- text "
            "is too short (%i symbols)\n- text is most likely to be an unconsistent sequence of letters\n", resStrSize);
            return EXIT_FAILURE;
        }
        char foundKey[keyLen + 1];
        getKeyByKeyLen(foundKey, keyLen, resString, resStrSize);
        decodeByKeyInPlace(foundKey, resString, textLen + 1, keyLen + 1);
        puts(foundKey);
    }
    else {
        return EXIT_FAILURE;
    }
    puts(resString);
    return EXIT_SUCCESS;
}



void check(const int value){
    if (!value){
        exit(EXIT_FAILURE);
    }        
}


long getFileSize(const char * fileName) {
    FILE *f = fopen(fileName, "rb");
    if (!f) return -1;  // error opening file
    fseek(f, 0, SEEK_END);  // rewind cursor to the end of file
    long fsize = ftell(f);  // get file size in bytes
    fclose(f);
    return fsize;
}


void getProperFormatStringFromString(const char * string, char * buffer, const int bufferSize){
    assert(bufferSize == strlen(string) + 1);
    int bufi = 0; // buffer index
    for (int i = 0; string[i] != '\0'; i++){
        if (isalpha(string[i])){
            buffer[bufi] = toupper(string[i]);
            bufi++;
        }
    }
    buffer[bufi] = '\0';

}


void getProperFormatStringFromFile(const char * fileName, char * buffer, const int bufferSize){
    assert(bufferSize == getFileSize(fileName) + 1);
    assert(fileName != NULL);
    assert(buffer != NULL);
    assert(bufferSize > 0);
    FILE * file = fopen(fileName, "r");
    check(file != NULL);
    int bufi = 0; // buffer index
    char c = fgetc(file);
    while(!feof(file)){
        if (isalpha(c)){
            buffer[bufi] = toupper(c);
            bufi++;
        }
        c = fgetc(file);        
    }
    buffer[bufi] = '\0';
    fclose(file);
}


void encodeByKeyInPlace(const char * key, char * buffer, const long bufferSize, const int keySize){
    assert(bufferSize >= strlen(buffer) + 1);
    assert(key != NULL);
    assert(buffer != NULL);
    assert(bufferSize > 0);
    assert(keySize > 0);
    int i = 0;
    while (buffer[i] != '\0'){
        buffer[i] = (key[i % (keySize - 1)] + buffer[i] - 2 * ALPHABET_START) % ALPHABET_SIZE + ALPHABET_START;
        i++;
    }
    buffer[i] = '\0';
}


void decodeByKeyInPlace(const char * key, char * buffer, const long bufferSize, const int keySize){
    assert(bufferSize >= strlen(buffer) + 1);
    assert(key != NULL);
    assert(buffer != NULL);
    assert(bufferSize > 0);
    assert(keySize > 0);
    int i = 0;
    while (buffer[i] != '\0'){
        buffer[i] = (buffer[i] - key[i % (keySize - 1)] + ALPHABET_SIZE) % ALPHABET_SIZE + ALPHABET_START;
        i++;
    }
    buffer[i] = '\0';
}


int getKeyLen(const char * text, const int textSize){
    assert(text != NULL);
    assert(textSize > 0);
    if (textSize <= 3){
        return 0;
    }
    char threeSymbols[4];
    threeSymbols[3] = '\0';
    int threeSymPos[textSize];
    int gcdArr[GCD_ARR_SIZE];
    int gcdStat[GCD_STAT_SIZE];
    setZeroArr(gcdStat, GCD_STAT_SIZE);
    for (int i = 2; i < textSize - 1; i++){
        threeSymbols[0] = text[i - 2];
        threeSymbols[1] = text[i - 1];
        threeSymbols[2] = text[i];
        threeSymPos[0] = i - 2;
        int j = 1;
        int lastPos = i - 2;
        for (; j < textSize ; j++){
            threeSymPos[j] = lastPos = strstr((text + lastPos + 3), threeSymbols) - text;
            if (lastPos > textSize || lastPos < 0){
                break;
            }
            threeSymPos[j - 1] =  lastPos - threeSymPos[j - 1];
        }
        int gcdQuan = getArrGCDs(threeSymPos, j - 1, gcdArr, GCD_ARR_SIZE); // gcdQuan - quaintity of 
        for (int i = 0; i < gcdQuan; i++){                                  // gcds of all the combinations
            gcdStat[gcdArr[i]]++;
        }                                  
    }
    return getMaxIndex(gcdStat, GCD_STAT_SIZE);
}


void getKeyByKeyLen(char * keyBuffer, const int keyLen, const char * text, const int textSize){
    assert(keyLen > 0);
    assert(keyBuffer != NULL);
    if (text == NULL || textSize < 1){
        return;
    }
    int statistic[keyLen][ALPHABET_SIZE];
    for (int i = 0; i < keyLen; i++){
        setZeroArr(statistic[i], ALPHABET_SIZE);
    }
    for (int shift = 0; shift < keyLen; shift++){
        int symbolsQuan = 0; // quantity of symbols written in statistic
        for (int i = 0; i * keyLen + shift < textSize - 1; i++){
            statistic[shift][text[i * keyLen + shift ] - ALPHABET_START]++;
            symbolsQuan++;
        }
        char probableKeyLetter = 'A';
        double minCloseness = 500; // 500 percents to be sure it will be less then this
        for (char keyLetter = 'A'; keyLetter <= 'Z'; keyLetter++){
            double closeness = 0; // sum of differences between assumable key probabilities and naturaral leter frequencies
            for (int i = 0; i < ALPHABET_SIZE; i++){
                closeness += fabs(((double)statistic[shift][(i  + keyLetter - ALPHABET_START) % ALPHABET_SIZE]) / ((double)symbolsQuan) * 100 - LETTER_FREQUENCY[i]);
            }
            if (closeness < minCloseness){
                minCloseness = closeness;
                probableKeyLetter = keyLetter;
            }
        }
        keyBuffer[shift] = probableKeyLetter;
    }
    keyBuffer[keyLen] = '\0';
}


void setZeroArr(int * array, const int arraySize){
    for (int i = 0; i < arraySize; i++) {
        array[i] = 0;
    }
}


int getMaxIndex(const int * array, const int arraySize){
    int maxIndex = 0;
    if (array != NULL && arraySize > 0){
        for (int i = 1; i < arraySize; i++){
            if (array[i] > array[maxIndex]){
                maxIndex = i;
            }
        }
    }
    return maxIndex;
}


int gcd(const int a, const int b){
    assert(a >= 0 && b >= 0);
    return b ? gcd(b, a % b) : a;
}


int getArrGCDs(const int * array, const int arraySize, int * gcdBuffer, const int gcdBufferSize){
    assert(array != NULL && arraySize >= 0);
    assert(gcdBuffer != NULL && gcdBufferSize > 0);
    if (arraySize == 1){
        gcdBuffer[0] = array[0];
        return 1;
    }
    if (arraySize == 0){
        return 0;
    }
    int bufIndx = 0;
    for (int i = 0; i <= arraySize - 2; i++){
        for (int j = i + 1; j <= arraySize - 1; j++){
            gcdBuffer[bufIndx++] = gcd(array[i], array[j]);
        }
    }
    return bufIndx - 1;
}