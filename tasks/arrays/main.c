#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <progbase/console.h>
#include <limits.h>

int main (){
    puts ("===========================================================================================");
    puts ("1. Creating an array with random numbers and outtputing it in direct and reverse order:");
    puts ("-------------------------------------------------------------------------------------------");
    srand (time(0));
    const int SIZEOFARRAY = 10;
    int A[SIZEOFARRAY];
    int H = 199;
    int L = -99;
    for (int i = 0; i < SIZEOFARRAY; i++){
        A[i] = rand()%(H - L + 1) + L;
        printf ("%3i  ", A[i]);
    }
    putchar ('\n');
    for (int i = SIZEOFARRAY - 1; i >= 0; i--){
        printf ("%3i  ", A[i]);
    }


    puts ("\n\n\n===========================================================================================");
    puts ("2. Outputting the array, emphasizing numbers greater than 100:");
    puts ("-------------------------------------------------------------------------------------------");
    for (int i = 0; i < SIZEOFARRAY; i++){
        if (A[i] > 100){
            Console_setCursorAttributes(BG_RED);
        }
            printf ("%3i", A[i]);
            Console_reset();
            printf ("  ");
    }


    puts ("\n\n\n===========================================================================================");
    puts ("3. Outputting the array, emphasizing numbers greater than 127:");
    puts ("-------------------------------------------------------------------------------------------");
    int count = 0;
    int sum = 0;
    double armean = 0;
    for (int i = 0; i < SIZEOFARRAY; i++){
        if (A[i] > 127){
            Console_setCursorAttributes(BG_BLUE);
            sum = sum + A[i];
            count++;
        }
            printf ("%3i", A[i]);
            Console_reset();
            printf ("  ");
    }
    puts ("\n-------------------------------------------------------------------------------------------");
    if (count == 0){
        puts ("There are no numbers greater than 127 to operate with.");
    }
    else{
    armean = (double)sum/(double)count;
    printf ("Count:    %i\n", count);
    printf ("Sum:      %i\n", sum);
    printf ("Average:  %f\n", armean);
    }

    
    puts ("\n\n===========================================================================================");
    puts ("4. Finding the first maximum in-between [-50 ; 50] and the first minimum positive value:");
    puts ("-------------------------------------------------------------------------------------------");
    int maxValue = INT_MIN;
    int maxIndex = -1;
    int minPosValue = INT_MAX;
    int minIndex = -1;
    for (int i = 0; i < SIZEOFARRAY; i++){
        if (A[i] > maxValue && abs(A[i]) <= 50){
            maxValue = A[i];
            maxIndex = i;
        }
        if (A[i] < minPosValue && A[i] > 0){
            minPosValue = A[i];
            minIndex = i;
        }
    }
    if (maxIndex == -1){
        puts ("There are no numbers in-between [-50 ; 50]");
    }
    else{
        printf ("The first maximum element in-between [-50 ; 50] is A[%i] = %i\n", maxIndex, maxValue);
        if (minIndex == -1){
            puts ("There are no positive numbers");
        }
        else {
            printf ("The first minimal positive element is A[%i] = %i\n", minIndex, minPosValue);
        }
    }

    
    puts ("\n\n===========================================================================================");
    puts ("5. Interpreting the array A into a string:");
    puts ("-------------------------------------------------------------------------------------------");
    char S[SIZEOFARRAY];
    S[SIZEOFARRAY - 1] = '\0';
    for (int i = 0; i < SIZEOFARRAY - 1; i++){
        S[i] = (abs(A[i]))%95 + 32;
    }
    puts (S);


    puts ("\n\n===========================================================================================");
    puts ("6. Outputing the array with emphasized zeroes instead of the values\n   less than -25 and greater than 25:");
    puts ("-------------------------------------------------------------------------------------------");
    for (int i = 0; i < SIZEOFARRAY; i++){
        if (abs(A[i]) > 25){
            A[i] = 0;
            Console_setCursorAttributes(BG_GREEN);
        }
            printf ("%3i", A[i]);
            Console_reset();
            printf ("  ");
    }
    putchar ('\n');
    
    return 0;
}
