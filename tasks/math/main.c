#include <math.h>
#include <stdio.h>

int main(void) {
    double x = 0;
    for(x = -10 ; x <= 10 ; x += 0.5){
        double F1 = pow(x,2) + 3; 
        double F2 = 0.5 * tan(x + 2);
        double sum = F1 + F2;
        double mult = F1 * F2;
        printf("x = %.1f\n", x);
        printf("F1(x) = %.2f\n", F1);
        printf("F2(x) = %f\n", F2);
        printf("F1(x) + F2(x) = %f\n", sum);
        printf("F1(x) * F2(x) = %f\n", mult);
        if (F2 == 0){
            puts("F1(x) / F2(x) Division by zero");
        }
        else {
            double div1 = F1/F2;
            printf("F1(x) / F2(x) = %f\n", div1);
        }
        if (F1 == 0){
            puts("F2(x) / F1(x) Division by zero");
        }
        else{
        double div2 = F2/F1;
        printf("F2(x) / F1(x) = %f\n", div2);
        }
        puts("___________________________\n");
        }
    return 0;
}