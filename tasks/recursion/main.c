#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <assert.h>
#include <progbase/console.h>

#define MAX_USERSTRING_LEN 0xfff
#define STRING_TERMINAL_SYMBOL '\n' // symbol to define the end of input string

//______________________FUNCTIONS______________________

// NOTE: ALL functions are recursive

// reads user's input in (buffer) untill have met (terminalSymbol) or buffer end
void readString(char * buffer, const int bufferSize, const char terminalSymbol);


// no automatical '\n' at the end of string
void printString(const char * string);


// returns the quantity of hexNumbers present in (string)
int countHexDigtis(const char * string);


// prints (string) allocating particular symbols according to (foregroundColor) and (backgroundColor)
// put a negative integer istead of (...Color) argument if it is needed
// NOTE: requires <progbase/console.h> included
void printSomeAllocated(const char * string, int (*condition)(int), const int foregroundColor, const int backgroundColor);


// prints String formated with (foregroundColor) and (backgroundColor)
// put a negative integer istead of (...Color) argument if it is needed
// NOTE: requires <progbase/console.h> included
void printAllocated(const char * string, const int foregroundColor, const int backgroundColor);

// ____________________________________________________

int main(void){
    //
    printAllocated("Input a string:", BG_GREEN, FG_BLACK);
    //
    printf(" ");
    //
    char userString[MAX_USERSTRING_LEN + 1];
    //
    readString(userString, MAX_USERSTRING_LEN + 1, STRING_TERMINAL_SYMBOL);
    //
    printAllocated("\nTASK: count all hexadecimal digits in your string:\n\n", FG_BLACK, BG_WHITE);
    //
    printSomeAllocated(userString, isxdigit, BG_BLUE, FG_BLACK);
    //
    printAllocated("\nTOTAL HEXADECIMAL DIGITS DETECTED:", FG_BLACK, BG_YELLOW);
    //
    printf("  %i\n", countHexDigtis(userString));

// -----------------ASSERTIONS-----------------
    assert(countHexDigtis("") == 0);
    //
    assert(countHexDigtis("No hx") == 0);
    //
    assert(countHexDigtis("SomE hEx\0 some 123 after") == 2);
    //
    assert(countHexDigtis("regular 123") == 5);
    //
    assert(countHexDigtis("1,i hAvE no imAginAtion!#@$%#$^%& e") == 6);
    return EXIT_SUCCESS;
}


void readString(char * buffer, const int bufferSize, const char terminalSymbol){
    if (bufferSize > 1 && ((*buffer = getchar()) != terminalSymbol)){
        readString(buffer + 1, bufferSize - 1, terminalSymbol);
    }
    else{
        buffer[0] = '\0';
    }
}


void printString(const char * string){
    if (*string != '\0'){
        putchar(*string);
        printString(string + 1);
    }
}


int countHexDigtis(const char * string){
    if (*string != '\0'){
        return  countHexDigtis(string + 1) + ((isxdigit(*string)) ? 1 : 0);
    }
    return 0;
}


void printSomeAllocated(const char * string, int (*condition)(int), const int foregroundColor, const int backgroundColor){
    if (*string != '\0'){
        if (condition != NULL && condition(*string)){
            if (foregroundColor > 0){
                Console_setCursorAttributes(foregroundColor);
            }
            if (backgroundColor > 0){
                Console_setCursorAttributes(backgroundColor);
            }
            putchar(*string);
            Console_reset();
        }
        else{
            putchar(*string);
        }
        printSomeAllocated(string + 1, condition, foregroundColor, backgroundColor);
    }
    else {
        putchar('\n');
    }
}


void printAllocated(const char * string, const int foregroundColor, const int backgroundColor){
    if (foregroundColor > 0){
        Console_setCursorAttributes(foregroundColor);
    }
    if (backgroundColor > 0){
        Console_setCursorAttributes(backgroundColor);
    }
    printString(string);
    Console_reset();
}