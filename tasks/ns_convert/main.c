#include <string.h>
#include <math.h>
#include <stdio.h>

char *ns_convert(char *destNumber, const char *sourceNumber, unsigned int sourceBase, unsigned int destBase);

int main(void){
   return 0;
}

char *ns_convert(char *destNumber, const char *sourceNumber, unsigned int sourceBase, unsigned int destBase){
    *destNumber = '\0';
    if (sourceBase < 2 || sourceBase > 36 || destBase < 2 || destBase > 36){
        return "\0";
    }
    char alphabet[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    int symbols = 0;
    int digits = 0;
    for (int i = 0; *(sourceNumber + i) != '\0'; i++){
        if (i > 64){
            return "\0";
        }
        short isDigit = 0;
        for (int j = 0; j < sourceBase; j++){
            if (*(sourceNumber + i) == alphabet[j]){
                isDigit = 1;
                digits++;
            }
        }
        if (isDigit == 0 && *(sourceNumber + i) != '.' && *(sourceNumber + i) != '-'){
            return "\0";
        }
        symbols++;
    }
    char srcNum[symbols + 1];
    for (int i = 0; i < symbols + 1; i++){
        srcNum[i] = *(sourceNumber + i);
    }
    int isNegative = 1;
    if (srcNum[0] == '-'){
        isNegative = -1;
        for (int i = 0; i < symbols; i++){
            srcNum[i] = srcNum[i + 1];
        }
    }
    double decimal = 0;
    int rubicon = -1;
    for (int i = 0; srcNum[i] != '\0'; i++){
        if (srcNum[i] == '.'){
            rubicon = i;
        }
    }
    rubicon = (rubicon == -1) ? digits : rubicon;
    int isRubicon = 0;
    for (int i = 0; srcNum[i] != '\0'; i++){
        if (i == rubicon){
            isRubicon = 1;
            continue;
        }
        for (int j = 0; j < sourceBase; j++){
            if (srcNum[i] == alphabet[j]){
                decimal += j * pow(sourceBase, rubicon - 1 - (i - isRubicon));
            }
        }
    }
    int integerPart = floor(decimal);
    double fractionalPart = decimal - floor(decimal);
    int i = -1;
    if (isNegative == -1){
        i = 0;
        destNumber[0] = '-';
    }
    while (integerPart >= destBase){
        i++;
        destNumber[i] = alphabet[integerPart % destBase];
        integerPart /= destBase;
    }
    int length = ++i;
    destNumber[i] = alphabet[integerPart];
    for (int j = (isNegative == -1) ? 1 : 0; j < i; j++, i--){
        int temp = destNumber[j];
        destNumber[j] = destNumber[i];
        destNumber[i] = temp;
    }
    if (fractionalPart == 0){
        destNumber[length + 1] = '\0';
        return destNumber;
    }
    i = length + 1;
    destNumber[i] = '.';
    i++;
    for (int j = 0; j < 12; i++, j++){
        fractionalPart = fractionalPart * destBase;
        destNumber[i] = alphabet[(int)floor(fractionalPart)];
        fractionalPart -= floor(fractionalPart);
        if (fractionalPart == 0){
            i++;
            break;
        }
    }
    destNumber[i] = '\0';
    return destNumber; 
}
