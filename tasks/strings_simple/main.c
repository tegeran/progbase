#include <stdio.h>
#include <progbase/console.h>
#include <ctype.h>


int isvowel (char input){
    return (input == 'a' || input == 'A' || input == 'e'|| input == 'E'||
            input == 'i' || input == 'I' || input == 'y'|| input == 'Y'||
            input == 'o' || input == 'O' || input == 'u'|| input == 'U' ) ? 1 : 0;
}

int main (){
    Console_setCursorAttributes(BG_BLUE);
    puts ("\n=================================================================================");
    puts ("1. Вивести текст повністю у консолі та загальну кількість символів у тексті.");
    puts ("=================================================================================\n");
    Console_reset();
    const char text[] = "I've watched through his eyes, I've listened through his ears, and I tell you he's the one.\n"
    "Or at least as close as we're going to get. That's what you said about the brother.\n"
    "The brother tested out impossible. For other reasons. Nothing to do with his ability.\n"
    "Same with the sister. And there are doubts about him. He's too malleable.\n"
    "Too willing to submerge himself in someone else's will. \n"
    "Not if the other person is his enemy.\n"
    "So what do we do? Surround him with enemies all the time? If we have to. "
    "I thought you said you liked this kid.\n"
    "If the buggers get him, they'll make me look like his favorite uncle. All right.\n"
    "We're saving the world, after all. Take him.";
    puts (text);
    Console_setCursorAttributes(BG_RED);
    puts ("\n__________________");
    const unsigned long int textSize = sizeof(text) / sizeof (text[1]);
    printf("TOTAL SYMBOLS: %zu\n\n", textSize);


    Console_setCursorAttributes(BG_BLUE);
    puts ("=================================================================================");
    puts ("2. Вивести текст без символів пунктуації та кількість виведених символів.");
    puts ("=================================================================================\n");
    Console_reset();
    int counter = 0;
    for (int i = 0; i < textSize; i++){
        if (!ispunct(text[i])){
            putchar (text[i]);
            counter++;
        }
    }
    Console_setCursorAttributes(BG_RED);
    puts ("\n__________________");
    printf ("TOTAL SYMBOLS: %i\n\n", counter);


    Console_setCursorAttributes(BG_BLUE);
    puts ("=================================================================================");
    puts ("3. Вивести всі речення тексту, кожне речення із нового рядка та із відміткою про \
    \nкількість символів у виведеному реченні. Речення розділяти спеціальними рядками.");
    puts ("=================================================================================\n");
    Console_reset();
    counter = 0;
    for (int i = 0; i < textSize; i++){
        if ((i != 0 && text[i-1] == '.' && text[i] == ' ' ) || text[i] == '\n'){
            continue;
        }
        putchar(text[i]);
        counter++;
            if(text[i] == '.'){
                Console_setCursorAttributes(BG_RED);
                puts("\n-----------");
                printf ("SYMBOLS: %2i\n", counter);
                Console_reset();
                puts("");
                counter = 0;
            }
    }


    Console_setCursorAttributes(BG_BLUE);
    puts ("=================================================================================");
    puts ("4. Вивести загальну кількість слів у тексті (всі слова - неперервні послідовності літер).");
    puts ("=================================================================================\n");
    counter = 0;
    for (int i = 0; i < textSize; i++){
        if (i != '\0' && isalpha(text[i]) && !isalpha(text[i+1]) && text[i+1] != '\'' ){
            counter++;
        }
    }
    Console_setCursorAttributes(BG_RED);
    printf ("TOTAL WORDS: %i\n\n", counter);

    
    Console_setCursorAttributes(BG_BLUE);
    puts ("=================================================================================");
    puts ("5. У одному рядку, через кому і один пробіл, вивести всі слова,\
    \nщо мають довжину <= 3 символів. Також вивести загальну кількість таких слів.");
    puts ("=================================================================================\n");
    Console_reset();
    counter = 0;
    int j = 0;
    for (int i = 0; i < textSize; i++){
        if ((i == 0 && isalpha(text[i])) || (i != 0 && ((isalpha(text[i]) || text[i] == '\'') && (!isalpha(text[i-1]) && text[i-1] != '\'')))){
            int letters = 1;
            for (j = i + 1; isalpha(text[j]) || text[j] == '\''; j++){
                letters++;
                if (letters > 3){
                    break;
                }
            }
            if (letters > 3){
                continue;
            }
            counter++;
            for (; i < j; i++){
                putchar (text[i]);
            }
            printf (", ");
            if (counter % 20 == 0){
                puts("");
            }
        }
    }
    Console_setCursorAttributes(BG_RED);
    printf ("\nWORDS: %i\n\n", counter);

    Console_setCursorAttributes(BG_BLUE);
    puts ("=================================================================================");
    puts ("6. У одному рядку, через кому і один пробіл, вивести всі слова, які починаються\
    \nта закінчуються на голосну літеру. Також вивести загальну кількість таких слів.");
    puts ("=================================================================================\n");
    counter = 0;
    Console_reset();
    for (int i = 0; i < textSize; i++){
        if (isvowel(text[i]) && (i == 0 || (i != 0 && (!isalpha(text[i-1]) && text[i-1] != '\'')))){
            for (j = i; isalpha(text[j]) || text[j] == '\''; j++){
            }
            if (isvowel(text[j-1])){
                counter++;
                for (; i < j; i++){
                    putchar (text[i]);
                }
                printf (", ");
                if (counter % 20 == 0){
                    puts("");
                }
            }
        }
    }
    Console_setCursorAttributes(BG_RED);
    printf ("\nWORDS: %i\n\n", counter);
    Console_reset();
    return 0;
}