#include <stdio.h>
#include <math.h>

const double PI = 3.141592653589793238462643;

int main (void){
    int n = 0;
    int m = 0;
    int i = 0;
    int j = 0;
    double sum1 = 0;
    double sum2 = 0;
    double x = 0;
    puts ("Input the higher limit n:");
    scanf ("%i", &n);
    if (n < 1){
        puts ("Error: invalid value of the higher limit n");
    }
    else{
        puts ("Input the higher limit m:");
        scanf ("%i", &m);
        if (m < 1) {
            puts ("Error: invalid value of the higher limit m");
        }
        else{
            putchar ('\n');
            printf ("n = %i\n", n);
            printf ("m = %i\n", m);
            puts ("------------------------------------------------");
            for (i = 1; i <= n; i++){
                for (j = 1, sum2 = 0; j <= m; j++){
                    sum2 = sum2 + pow(j , 2);
                    printf ("i = %i, j = %i: sum1 = %lf, sum2 = %lf\n", i, j, sum1, sum2);
                }
                sum1 = sum1 + sin((2 * PI) / (i + 0)) * cos(sum2);
            }
            x = sum1;
            puts ("------------------------------------------------");
            printf ("x = %lf\n\n", x);
        }
    }
    return 0;
}