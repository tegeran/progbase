#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main (void){
    int a = 0;
    int b = 0;
    int c = 0;
    puts ("Input the variable a:");
    scanf ("%i", &a);
    puts ("Input the variable b:");
    scanf ("%i", &b);
    puts ("Input the variable c:");
    scanf ("%i", &c);
    putchar ('\n');
    printf ("a = %i\n", a);
    printf ("b = %i\n", b);
    printf ("c = %i\n", c);
    int max = 0;
    int min = 0;
    if (a < 0 && b < 0 && c < 0){
        if (a <= b && a <= c){
            min = a;            
        }
        else{
            if (b <= c){
                min = b;
            }
            else{
                min = c;
            }
        }
        puts ("All negative");
        int modmin = abs(min);
        int sum2 = a + b + c - min;
        printf ("modmin = %i\n", modmin);
        printf ("sum2 = %i\n", sum2);
        if (abs(sum2) > modmin && sum2 > -256){
            puts ("Result = True");
        }
        else{
            puts ("Result = False");
        }
    }
    else{
        if (a >= 0 && b >= 0 && c >= 0){
            if (a >= b && a>= c){
                max = a;
                if (b <= c){
                    min = b;
                }
                else{
                    min = c;
                }
            }
            else{
                if (b >= c){
                    max = b;
                    if(a <= c){
                        min = a;
                    }
                    else{
                        min = c;
                    }
                }
                else{
                    max = c;
                    if (a <= b){
                        min = a;
                    }
                    else{
                        min = b;
                    }
                }
            }
            puts ("All positive");
            printf ("min = %i\n", min);
            printf ("max = %i\n", max);
            if ((max - min) > 32){
                puts ("Result = True");
            }
            else{
                puts ("Result = False");
            }
        }
        else{
            int negativeA = 0;
            int negativeB = 0;
            int negativeC = 0;
            if (a < 0){
                negativeA = 1;
            }
            if (b < 0){
                negativeB = 1;
            }
            if (c < 0){
                negativeC = 1;
            }
            if ((negativeA + negativeB + negativeC) == 1){
                if (negativeA){
                    printf ("One negative: %i\n", a);
                }
                else{
                    printf ("One negative: %i\n", negativeB ? b : c);
                }
                if ((negativeA*a + negativeB*b + negativeC*c) % 2 == 0){
                    puts ("Result = True");
                }
                else{
                    puts ("Result = False");
                }
            }
            if ((negativeA + negativeB + negativeC) == 2){
                int neg1 = 0;
                int neg2 = 0;
                if (!negativeA){
                    neg1 = b;
                    neg2 = c;
                }
                else{
                    if(!negativeB){
                        neg1 = a;
                        neg2 = c;
                    }
                    else{
                        neg1 = a;
                        neg2 = b;
                    }
                }
                printf ("Two negative: %i and %i\n", neg1, neg2);
                if ((neg1+neg2)*7 > -256){
                    puts("Result = True");
                }
                else{
                    puts ("Result = False");
                }
            }
        }
    }
    putchar('\n');
    return 0;
}