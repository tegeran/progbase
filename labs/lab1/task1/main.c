#include <math.h>
#include <stdio.h>

int main(void) 
{
    double a = 0;
    double a0 = 0;
    double a1 = 0;
    double a2 = 0;
    double x = 0;
    double y = 0;
    double z = 0;
    int validator = 0;
    puts ("Input the variable x:");
    scanf ("%lf", &x);
    puts ("Input the variable y:");
    scanf ("%lf", &y);
    puts ("Input the variable z:");
    scanf ("%lf", &z);
    putchar ('\n');
    printf ("x = %lf\n", x);
    printf ("y = %lf\n", y);
    printf ("z = %lf\n", z);
    if ((x <= 0 && (y+1) != round (y+1)) || ((x-y) <= 0 && (1/z) != round (1/z)) || (x == 0 && (y+1) < 0) || z == 0 || (x-y) == 0){
        puts ("a0 Can't be computed");
        validator++;
    }
    else{
        a0 = (pow (x, y + 1))/(pow (x - y, 1/z));
        printf ("a0 = %lf\n", a0);
    }
    if ((x+y) == 0){
            puts ("a1 Can't be computed");
            validator++;
    }
    else{
        a1 = y/(6*fabs(x+y));
        printf ("a1 = %lf\n", a1);
    }
    if (sin(y) == 0 || ((x+0) <= 0 && ((1/fabs(sin(y))) != round(1/fabs(sin(y)))))){
        puts ("a2 Can't be computed");
        validator++;
    }
    else{
        a2 = pow((x+0),1/fabs(sin(y)));
        printf ("a2 = %lf\n", a2);
    }
    if (validator != 0){
        puts ("a Can't be computed");
    }
    else{
        a = a0 + a1 + a2;
        printf ("a = %lf\n", a);
    }
    putchar ('\n');
    return 0;
}