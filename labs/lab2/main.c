#include <stdio.h>
#include <progbase/console.h>
#include <string.h>
#include <time.h>
#include <progbase.h>
#include <ctype.h>

enum indexIJ {i, j};

int getLimit(int number);
int min(int a, int b);
int minArrayElementIndex(int size, int array[]);
int maxArrayElementIndex(int size, int array[]);
int minMatrixElementIndex(int size, int matrix[][size], enum indexIJ indexType);
int maxMatrixElementIndex(int size, int matrix[][size], enum indexIJ indexType);

void vanish(void);
void cleanBuffer(void);

void setArrayToZero(int size, int array[]);
void setMatrixToZero(int size, int matrix[][size]);

void printMainMenu(void);
void printInputMessage(char inputMessage[]);
void printReferences(void);
void printErrorReport(char errorMessage[]);
void printHelp(char helpMessage[]);
void printVerticalLine(unsigned int start, unsigned int finish, unsigned int horizontalShift);
void printTask1Interface(int arraySize);
void printTask2Interface(int matrixSize);
void printTask3Interface(int stringBufferSize);
void printHorizontalLine(unsigned int start, unsigned int finish, unsigned int verticalShift);
void printArray(int size, int array[]);
void printMatrix(int size, int matrix[][size]);
void printStrings(char string[]);
void printSubstring(char string[], char substring[], int verticalShift);

void emphasizeArrayElement(int index, int array[]);
void emphasizeMatrixElement(int indexI, int indexJ, int size, int matrix[][size]);
void emphasizeSubstring(char string[], char substring[], int indexInString, enum conAttribute_e color);

int main(void)
{
    int mainTask = 0;
    srand(time(0));
    int cleanNeeded = 0;
    for (;;)
    {
        printMainMenu();
        printInputMessage("Select a task:");
        if (cleanNeeded)
        {
            cleanBuffer();
        }
        scanf("%i", &mainTask);
        int repeatSwitch = 0;
        do
        {
            switch (mainTask)
            {
            case 0:
            {
                return 0;
            }
            case 1:
            {
                int N = 0;
                printTask1Interface(N);
                printInputMessage("Input array's size:");
                cleanBuffer();
                scanf("%i", &N);
                while (N <= 0)
                {
                    printTask1Interface(N);
                    printErrorReport("Invalid size value.\nUse only natural numbers.");
                    printInputMessage("Input array's size:");
                    cleanBuffer();
                    scanf("%i", &N);
                }
                printTask1Interface(N);
                int array[N];
                setArrayToZero(N, array);
                printArray(N, array);
                int task;
                int stayAtTask1 = 1;
                while (stayAtTask1)
                {
                    printInputMessage("Select a task:");
                    cleanBuffer();
                    scanf("%i", &task);
                    switch (task)
                    {
                    case 0:
                    {
                        return 0;
                    }
                    case 1:
                    {
                        printTask1Interface(N);
                        printArray(N, array);
                        int lim1 = getLimit(1);
                        int lim2 = getLimit(2);
                        while (lim1 == lim2)
                        {
                            printTask1Interface(N);
                            printArray(N, array);
                            printErrorReport("Identical lower and higher limits.\n"
                                             "Unable to randomize.");
                            lim1 = getLimit(1);
                            lim2 = getLimit(2);
                        }
                        for (int i = 0; i < N; i++)
                        {
                            array[i] = rand() % (abs(lim1 - lim2) + 1) + min(lim1, lim2);
                        }
                        printTask1Interface(N);
                        printArray(N, array);
                        break;
                    }
                    case 2:
                    {
                        setArrayToZero(N, array);
                        printTask1Interface(N);
                        printArray(N, array);
                        break;
                    }
                    case 3:
                    {
                        int minIndex = minArrayElementIndex(N, array);
                        printTask1Interface(N);
                        printArray(N, array);
                        emphasizeArrayElement(minIndex, array);
                        printf("The minimum element is a[%i] = %-4i\n", minIndex, array[minIndex]);
                        break;
                    }
                    case 4:
                    {
                        long sum = 0;
                        for (int i = 0; i < N; i++)
                        {
                            sum += array[i];
                        }
                        printTask1Interface(N);
                        printArray(N, array);
                        printf("The total sum of the elements is %li\n", sum);
                        break;
                    }
                    case 5:
                    {
                        long prodOfNegatives = 1;
                        int countOfNegatives = 0;
                        printTask1Interface(N);
                        printArray(N, array);
                        for (int i = 0; i < N; i++)
                        {
                            if (array[i] < 0)
                            {
                                emphasizeArrayElement(i, array);
                                prodOfNegatives *= array[i];
                                countOfNegatives++;
                            }
                        }
                        if (countOfNegatives == 0)
                        {
                            printErrorReport("No negative numbers are present in the array.");
                        }
                        else
                        {
                            printf("The product of sequnce of negative numbers is %li\n", prodOfNegatives);
                        }
                        break;
                    }
                    case 6:
                    {
                        int minIndex = minArrayElementIndex(N, array);
                        int maxIndex = maxArrayElementIndex(N, array);
                        int temp = array[minIndex];
                        array[minIndex] = array[maxIndex];
                        array[maxIndex] = temp;
                        printTask1Interface(N);
                        printArray(N, array);
                        emphasizeArrayElement(minIndex, array);
                        emphasizeArrayElement(maxIndex, array);
                        printf("Swapped min[%i] and max[%i]\n", minIndex, maxIndex);
                        break;
                    }
                    case 7:
                    {
                        int coefficient = 0;
                        printTask1Interface(N);
                        printArray(N, array);
                        printInputMessage("Input a number to multiply by:");
                        cleanBuffer();
                        scanf("%i", &coefficient);
                        printTask1Interface(N);
                        printArray(N, array);
                        for (int i = 0; i < N; i++)
                        {
                            int wetherChanged = array[i];
                            array[i] *= coefficient;
                            if (wetherChanged != array[i])
                            {
                                emphasizeArrayElement(i, array);
                            }
                        }
                        break;
                    }
                    case 8:
                    {
                        printTask1Interface(N);
                        printArray(N, array);
                        printHelp("You have a one-dimensional array."
                                  "\n Read the information above and use keys from 1 to 7\n"
                                  "to interact with the array");
                        printReferences();
                        break;
                    }
                    case 9:
                    {
                        stayAtTask1 = 0;
                        break;
                    }
                    default:
                    {
                        printTask1Interface(N);
                        printArray(N, array);
                        printErrorReport("Unexpected value.\nUse only valid commands.");
                    }
                    }
                }
                repeatSwitch = 0;
                cleanNeeded = 1;
                break;
            }
            case 2:
            {
                int N = 0;
                printTask2Interface(N);
                printInputMessage("Input matrix's size:");
                cleanBuffer();
                scanf("%i", &N);
                while (N <= 0)
                {
                    printTask2Interface(N);
                    printErrorReport("Invalid size value.\nUse only natural numbers.");
                    printInputMessage("Input matrix's size:");
                    cleanBuffer();
                    scanf("%i", &N);
                }
                printTask2Interface(N);
                int matrix[N][N];
                setMatrixToZero(N, matrix);
                printMatrix(N, matrix);
                int task;
                int stayAtTask2 = 1;
                while (stayAtTask2)
                {
                    printInputMessage("Select a task:");
                    cleanBuffer();
                    scanf("%i", &task);
                    switch (task)
                    {
                    case 0:
                    {
                        return 0;
                    }
                    case 1:
                    {
                        printTask2Interface(N);
                        printMatrix(N, matrix);
                        int lim1 = getLimit(1);
                        int lim2 = getLimit(2);
                        while (lim1 == lim2)
                        {
                            printTask2Interface(N);
                            printMatrix(N, matrix);
                            printErrorReport("Identical lower and higher limits.\n"
                                             "Unable to randomize.");
                            lim1 = getLimit(1);
                            lim2 = getLimit(2);
                        }
                        for (int i = 0; i < N; i++)
                        {
                            for (int j = 0; j < N; j++)
                            {
                                matrix[i][j] = rand() % (abs(lim1 - lim2) + 1) + min(lim1, lim2);
                            }
                        }
                        printTask2Interface(N);
                        printMatrix(N, matrix);
                        break;
                    }
                    case 2:
                    {
                        setMatrixToZero(N, matrix);
                        printTask2Interface(N);
                        printMatrix(N, matrix);
                        break;
                    }
                    case 3:
                    {
                        int maxIndexI = maxMatrixElementIndex(N, matrix, i);
                        int maxIndexJ = maxMatrixElementIndex(N, matrix, j);
                        printTask2Interface(N);
                        printMatrix(N, matrix);
                        emphasizeMatrixElement(maxIndexI, maxIndexJ, N, matrix);
                        printf("The maximum element is m[%i][%i] = %-4i\n", maxIndexI, maxIndexJ, matrix[maxIndexI][maxIndexJ]);
                        break;
                    }
                    case 4:
                    {
                        printTask2Interface(N);
                        printMatrix(N, matrix);
                        long sideDiagonalSum = 0;
                        for (int i = 0; i < N; i++)
                        {
                            int j = N - 1 - i;
                            emphasizeMatrixElement(i, j, N, matrix);
                            sideDiagonalSum += matrix[i][j];
                        }
                        printf("The sum of the elements of the side diagonal is %li\n", sideDiagonalSum);
                        break;
                    }
                    case 5:
                    {
                        printTask2Interface(N);
                        printMatrix(N, matrix);
                        printInputMessage("Input a column to sum:");
                        int column = 0;
                        cleanBuffer();
                        scanf("%i", &column);
                        while (column < 0 || column >= N)
                        {
                            printTask2Interface(N);
                            printMatrix(N, matrix);
                            printErrorReport("Invalid column number.\n"
                                             "Use only natural numbers from 0 to N - 1");
                            printInputMessage("Input a column to sum:");
                            cleanBuffer();
                            scanf("%i", &column);
                        }
                        printTask2Interface(N);
                        printMatrix(N, matrix);
                        long columnSum = 0;
                        for (int i = 0; i < N; i++)
                        {
                            emphasizeMatrixElement(i, column, N, matrix);
                            columnSum += matrix[i][column];
                        }
                        printf("The sum of the column %i is %li\n", column, columnSum);
                        break;
                    }
                    case 6:
                    {
                        int minIndexI = minMatrixElementIndex(N, matrix, i);
                        int minIndexJ = minMatrixElementIndex(N, matrix, j);
                        int maxIndexI = maxMatrixElementIndex(N, matrix, i);
                        int maxIndexJ = maxMatrixElementIndex(N, matrix, j);
                        int temp = matrix[minIndexI][minIndexJ];
                        matrix[minIndexI][minIndexJ] = matrix[maxIndexI][maxIndexJ];
                        matrix[maxIndexI][maxIndexJ] = temp;
                        printTask2Interface(N);
                        printMatrix(N, matrix);
                        emphasizeMatrixElement(minIndexI, minIndexJ, N, matrix);
                        emphasizeMatrixElement(maxIndexI, maxIndexJ, N, matrix);
                        printf("Swapped min[%i][%i] and max[%i][%i]\n", minIndexI, minIndexJ, maxIndexI, maxIndexJ);
                        break;
                    }
                    case 7:
                    {
                        printTask2Interface(N);
                        printMatrix(N, matrix);
                        printInputMessage("Input the indexes of the element:");
                        printf("m[   ] [   ]");
                        Console_setCursorPosition(10, 37);
                        int I = -1;
                        cleanBuffer();
                        scanf("%i", &I);
                        int J = -1;
                        Console_setCursorPosition(10, 43);
                        cleanBuffer();
                        scanf("%i", &J);
                        while (I < 0 || I >= N || J < 0 || J >= N)
                        {
                            printTask2Interface(N);
                            printMatrix(N, matrix);
                            printErrorReport("Invalid index.\n"
                                             "Use only natural numbers from 0 to N - 1");
                            printInputMessage("Input the indexes of the element:");
                            printf("m[   ] [   ]");
                            Console_setCursorPosition(13, 37);
                            cleanBuffer();
                            scanf("%i", &I);
                            Console_setCursorPosition(13, 43);
                            cleanBuffer();
                            scanf("%i", &J);
                        }
                        printTask2Interface(N);
                        printMatrix(N, matrix);
                        emphasizeMatrixElement(I, J, N, matrix);
                        printInputMessage("Input new value:");
                        cleanBuffer();
                        scanf("%i", &matrix[I][J]);
                        printTask2Interface(N);
                        printMatrix(N, matrix);
                        emphasizeMatrixElement(I, J, N, matrix);
                        break;
                    }
                    case 8:
                    {
                        printTask2Interface(N);
                        printMatrix(N, matrix);
                        printHelp("You have a two-dimensional array."
                                  "\n Read the information above and use keys from 1 to 7\n"
                                  "to interact with the matrix");
                        printReferences();
                        break;
                    }
                    case 9:
                    {
                        stayAtTask2 = 0;
                        break;
                    }
                    default:
                    {
                        printTask2Interface(N);
                        printMatrix(N, matrix);
                        printErrorReport("Unexpected value. Use only valid commands.");
                    }
                    }
                }
                repeatSwitch = 0;
                cleanNeeded = 1;
                break;
            }
            case 3:
            {
                int N = 0;
                printTask3Interface(N);
                printInputMessage("Input string's size:");
                cleanBuffer();
                scanf("%i", &N);
                while (N < 1)
                {
                    printTask3Interface(N);
                    printErrorReport("Invalid size value.\nUse only natural numbers >= 1.");
                    printInputMessage("Input string's size:");
                    cleanBuffer();
                    scanf("%i", &N);
                }
                char string[N];
                string[N - 1] = '\0';
                for (int i = 0; i < N - 1; i++)
                {
                    string[i] = rand() % (126 - 33 + 1) + 33;
                }
                printTask3Interface(N);
                printStrings(string);
                int task;
                int stayAtTask3 = 1;
                int needCleanBuffer = 1;
                while (stayAtTask3)
                {
                    printInputMessage("Select a task:");
                    if (needCleanBuffer)
                    {
                        cleanBuffer();
                    }
                    else
                    {
                        needCleanBuffer = 1;
                    }
                    scanf("%i", &task);
                    switch (task)
                    {
                    case 0:
                    {
                        return 0;
                    }
                    case 1:
                    {
                        printTask3Interface(N);
                        printStrings(string);
                        printInputMessage("Input a new string to fill:");
                        puts("");
                        cleanBuffer();
                        getStringAr(string, N + 1);
                        printTask3Interface(N);
                        printStrings(string);
                        if (strlen(string) < N - 1)
                        {
                            needCleanBuffer = 0;
                        }
                        break;
                    }
                    case 2:
                    {
                        string[0] = '\0';
                        printTask3Interface(N);
                        printStrings(string);
                        break;
                    }
                    case 3:
                    {
                        printTask3Interface(N);
                        printStrings(string);
                        printInputMessage("Input a position for a substring to begin:");
                        int from = 0;
                        cleanBuffer();
                        scanf("%i", &from);
                        while (from < 0 || from > strlen(string))
                        {
                            printTask3Interface(N);
                            printStrings(string);
                            printErrorReport("Invalid position.\nThe position is out of string.");
                            printInputMessage("Input a position for a substring to begin:");
                            cleanBuffer();
                            scanf("%i", &from);
                        }
                        printTask3Interface(N);
                        printStrings(string);
                        printInputMessage("Input a substring's length:");
                        int length = 0;
                        cleanBuffer();
                        scanf("%i", &length);
                        while (length < 1)
                        {
                            printTask3Interface(N);
                            printStrings(string);
                            printErrorReport("Invalid length.\nUse only natural numbers.");
                            printInputMessage("Input a substring's length:");
                            cleanBuffer();
                            scanf("%i", &length);
                        }
                        char substring[length + 1];
                        int i = 0;
                        for (; i < length; i++)
                        {
                            if (string[from + i] == '\0')
                            {
                                break;
                            }
                            substring[i] = string[from + i];
                        }
                        substring[i] = '\0';
                        printTask3Interface(N);
                        printStrings(string);
                        emphasizeSubstring(string, substring, from, BG_BLUE);
                        printSubstring(string, substring, 0);
                        break;
                    }
                    case 4:
                    {
                        printTask3Interface(N);
                        printStrings(string);
                        printInputMessage("Input a delimiter: ");
                        char delimiter = ' ';
                        cleanBuffer();
                        scanf("%c", &delimiter);
                        char substring[N];
                        int index = 0;
                        int subIndex = 0;
                        int verticalShift = 0;
                        enum conAttribute_e color = 100;
                        printTask3Interface(N);
                        printStrings(string);
                        for (; string[index] != '\0'; index++)
                        {
                            if (string[index] != delimiter)
                            {
                                substring[subIndex] = string[index];
                                subIndex++;
                            }
                            else
                            {
                                substring[subIndex] = '\0';
                                emphasizeSubstring(string, substring, index - subIndex, color % 8 + 100);
                                printSubstring(string, substring, verticalShift);
                                verticalShift += (subIndex != 0) ? 2 + (strlen(substring) - 1) / 58 : 2;
                                subIndex = 0;
                                color++;
                            }
                        }
                        if (string[index - 1] != delimiter)
                        {
                            substring[subIndex] = '\0';
                            emphasizeSubstring(string, substring, index - subIndex, color % 8 + 100);
                            printSubstring(string, substring, verticalShift);
                        }
                        break;
                    }
                    case 5:
                    {
                        printTask3Interface(N);
                        printStrings(string);
                        char minWord[N];
                        int minLetters = N;
                        int minWordIndex = 0;
                        int words = 0;
                        for (int i = 0; string[i] != '\0'; i++)
                        {
                            if ((i == 0 && isalpha(string[i])) || (i != 0 && isalpha(string[i]) && !isalpha(string[i - 1])))
                            {
                                words++;
                                int letters = 0;
                                do
                                {
                                    letters++;
                                    i++;
                                } while (isalpha(string[i]) && letters != minLetters);
                                if (letters < minLetters)
                                {
                                    minLetters = letters;
                                    minWordIndex = i - letters;
                                }
                            }
                        }
                        if (words != 0)
                        {
                            int i = 0;
                            for (; i < minLetters; i++)
                            {
                                minWord[i] = string[minWordIndex + i];
                            }
                            minWord[i] = '\0';
                            printTask3Interface(N);
                            printStrings(string);
                            emphasizeSubstring(string, minWord, minWordIndex, BG_BLUE);
                            printf("The shortest word (%i letters): %s\n", minLetters, minWord);
                            break;
                        }
                        printErrorReport("There are no words in the string.");
                        break;
                    }
                    case 6:
                    {
                        printTask3Interface(N);
                        printStrings(string);
                        puts("Fractions:");
                        int counter = 0;
                        for (int i = 0; string[i] != '\0'; i++)
                        {
                            if (isdigit(string[i]))
                            {
                                counter++;
                                int wasPoint = 0;
                                char fractionInString[N];
                                int fisIndex = 0;
                                do
                                {
                                    fractionInString[fisIndex] = string[i];
                                    if (string[i] == '.')
                                    {
                                        wasPoint++;
                                        if (wasPoint == 2)
                                        {
                                            break;
                                        }
                                    }
                                    fisIndex++;
                                    i++;
                                } while (string[i] != '\0' && (isdigit(string[i]) || string[i] == '.'));
                                fractionInString[fisIndex] = '\0';
                                emphasizeSubstring(string, fractionInString, i - fisIndex, BG_BLUE);
                                Console_setCursorPosition(10 + counter, 1);
                                printf("%s ", fractionInString);
                                if (counter % 6 == 0 && counter != 0)
                                {
                                    puts("");
                                }
                            }
                        }
                        if (counter == 0)
                        {
                            puts("There are no fractions in the string");
                            break;
                        }
                        puts("");
                        break;
                    }
                    case 7:
                    {
                        printTask3Interface(N);
                        printStrings(string);
                        int counter = 0;
                        long product = 1;
                        for (int i = 0; string[i] != '\0'; i++)
                        {
                            if (isdigit(string[i]))
                            {
                                counter++;
                                char integerInString[N];
                                int iisIndex = 0;
                                do
                                {
                                    integerInString[iisIndex] = string[i];
                                    iisIndex++;
                                    i++;
                                } while (string[i] != '\0' && isdigit(string[i]));
                                integerInString[iisIndex] = '\0';
                                emphasizeSubstring(string, integerInString, i - iisIndex, BG_BLUE);
                                product *= atoi(integerInString);
                            }
                        }
                        if (counter == 0)
                        {
                            printErrorReport("There are no integers in the string");
                            break;
                        }
                        printf("Product of all the integers in the string: %li", product);
                        puts("");
                        break;
                    }
                    case 8:
                    {
                        printTask3Interface(N);
                        printStrings(string);
                        printHelp("You have a string."
                                  "\n Read the information above and use keys from 1 to 7\n"
                                  "to interact with the string");
                        printReferences();
                        break;
                    }
                    case 9:
                    {
                        stayAtTask3 = 0;
                        break;
                    }
                    default:
                    {
                        printTask3Interface(N);
                        printStrings(string);
                        printErrorReport("Unexpected value. Use only valid commands.");
                    }
                    }
                }
                repeatSwitch = 0;
                cleanNeeded = 1;
                break;
            }
            case 8:
            {
                printMainMenu();
                printHelp("You are currently in the main menu.\nChoose one task from the list above.");
                printInputMessage("Select a task:");
                cleanBuffer();
                scanf("%i", &mainTask);
                repeatSwitch = 1;
                break;
            }
            case 9:
            {
                printMainMenu();
                puts("You are still in the main menu.");
                printInputMessage("Select a task:");
                cleanBuffer();
                scanf("%i", &mainTask);
                repeatSwitch = 1;
                break;
            }
            default:
            {
                printMainMenu();
                printErrorReport("Unexpected value. Use only valid commands.");
                printInputMessage("Select a task:");
                cleanBuffer();
                scanf("%i", &mainTask);
                repeatSwitch = 1;
                break;
            }
            }
        } while (repeatSwitch == 1);
    }
    return 0;
}

void cleanBuffer(void)
{
    char c = 0;
    while (c != '\n')
    {
        c = getchar();
    }
}

void printMainMenu(void)
{
    vanish();
    puts("Tasks available:\n"
         "1. One-dimensional array.\n"
         "2. Two-dimensional array.\n"
         "3. Strings processing.");
    printReferences();
}

void printInputMessage(char inputMessage[])
{
    Console_setCursorAttributes(BG_BLUE);
    printf("%s", inputMessage);
    Console_reset();
    putchar(' ');
}

void printReferences(void)
{
    Console_setCursorAttributes(BG_INTENSITY_YELLOW);
    Console_setCursorAttributes(FG_BLACK);
    printf("Use \"8\" to view HELP.\n"
           "Use \"9\" to return to the main menu.\n"
           "Use \"0\" to stop the program.\n");
    Console_reset();
}

void printErrorReport(char errorMessage[])
{
    Console_setCursorAttributes(BG_RED);
    puts("ERROR:");
    puts(errorMessage);
    Console_reset();
}

void printHelp(char helpMessage[])
{
    Console_setCursorAttributes(BG_INTENSITY_GREEN);
    Console_setCursorAttributes(FG_BLACK);
    puts("HELP INFO:");
    puts(helpMessage);
    Console_reset();
}

void vanish(void)
{
    fflush(stdout);
    system("clear");
}

void printVerticalLine(unsigned int start, unsigned int finish, unsigned int horizontalShift)
{
    if (start > finish)
    {
        int temp = start;
        start = finish;
        finish = temp;
    }
    Console_setCursorAttributes(BG_WHITE);
    for (int i = start; i < finish; i++)
    {
        Console_setCursorPosition(i, horizontalShift);
        fflush(stdout);
        putchar(' ');
    }
    Console_reset();
    Console_setCursorPosition(1, 1);
}

void printTask1Interface(int arraySize)
{
    vanish();
    printVerticalLine(1, 100, 61);
    Console_setCursorAttributes(BG_INTENSITY_WHITE);
    Console_setCursorAttributes(FG_BLACK);
    puts("               TASK 1. ONE-DIMENSIONAL ARRAY.                                                                              ");
    if (arraySize > 0)
    {
        Console_setCursorPosition(1, 86);
        Console_setCursorAttributes(BG_BLACK);
        Console_setCursorAttributes(FG_INTENSITY_WHITE);
        printf("ARRAY'S SIZE: %i", arraySize);
        Console_setCursorPosition(2, 1);
    }
    Console_reset();
    puts("1. Fill the array with random numbers from L to H.\n"
         "2. Set all elements to zero.\n"
         "3. Find the minimum element and its index.\n"
         "4. Compute the sum of all the elements.\n"
         "5. Compute the product of sequence of negative elements.\n"
         "6. Swap minimum and maximum elements.\n"
         "7. Multiply elements by a number.");
    printHorizontalLine(1, 61, 9);
}

void printHorizontalLine(unsigned int start, unsigned int finish, unsigned int verticalShift)
{
    if (start > finish)
    {
        int temp = start;
        start = finish;
        finish = temp;
    }
    for (int i = start; i < finish; i++)
    {
        Console_setCursorAttributes(BG_INTENSITY_WHITE);
        Console_setCursorPosition(verticalShift, i);
        putchar(' ');
    }
    Console_reset();
    puts("");
}

void printArray(int size, int array[])
{
    for (int index = 0, i = 0, j = 0; index < size; index++, j++)
    {
        if (index % 12 == 0 && index != 0)
        {
            i++;
            j = 0;
        }
        Console_setCursorPosition(3 + i, 63 + 5 * j);
        printf("%4i", array[index]);
    }
    Console_setCursorPosition(10, 1);
}

void printMatrix(int size, int matrix[][size])
{
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            Console_setCursorPosition(3 + i, 63 + 5 * j);
            printf("%4i", matrix[i][j]);
        }
    }
    Console_setCursorPosition(10, 1);
}

void printTask2Interface(int matrixSize)
{
    vanish();
    printVerticalLine(1, 100, 61);
    Console_setCursorAttributes(BG_INTENSITY_WHITE);
    Console_setCursorAttributes(FG_BLACK);
    puts("               TASK 2. TWO-DIMENSIONAL ARRAY.                                                                              ");
    if (matrixSize > 0)
    {
        Console_setCursorPosition(1, 86);
        Console_setCursorAttributes(BG_BLACK);
        Console_setCursorAttributes(FG_INTENSITY_WHITE);
        printf("MATRIX'S SIZE: %i", matrixSize);
        Console_setCursorPosition(2, 1);
    }
    Console_reset();
    puts("1. Fill the matrix with random numbers from L to H.\n"
         "2. Set all elements to zero.\n"
         "3. Find the maximum element and its indexes.\n"
         "4. Compute the sum of all the elements of the side diagonal.\n"
         "5. Compute the sum of elements of the particular column.\n"
         "6. Swap minimum and maximum elements.\n"
         "7. Replace particular element by user's value.");
    printHorizontalLine(1, 61, 9);
}

void setArrayToZero(int size, int array[])
{
    for (int i = 0; i < size; i++)
    {
        array[i] = 0;
    }
}

void printAllocated(const char * string, const int foregroundColor, const int backgroundColor){
    if (string != NULL){
        if (foregroundColor > 0){
            Console_setCursorAttributes(foregroundColor);
        }
        if (backgroundColor > 0){
            Console_setCursorAttributes(backgroundColor);
        }
        printf("%s", string);
        Console_reset();
    }
}



int min(int a, int b)
{
    return (a <= b) ? a : b;
}

void setMatrixToZero(int size, int matrix[][size])
{
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            matrix[i][j] = 0;
        }
    }
}

int getLimit(int number)
{
    switch (number)
    {
    case 1:
    {
        printInputMessage("Input the first limit: ");
        break;
    }
    default:
    {
        printInputMessage("Input the second limit: ");
    }
    }
    int lim = 0;
    cleanBuffer();
    scanf("%i", &lim);
    return lim;
}

int minArrayElementIndex(int size, int array[])
{
    int minIndex = 0;
    int min = array[0];
    for (int i = 1; i < size; i++)
    {
        if (array[i] < min)
        {
            min = array[i];
            minIndex = i;
        }
    }
    return minIndex;
}

int maxArrayElementIndex(int N, int array[])
{
    int maxIndex = 0;
    int max = array[0];
    for (int i = 1; i < N; i++)
    {
        if (array[i] > max)
        {
            max = array[i];
            maxIndex = i;
        }
    }
    return maxIndex;
}

int minMatrixElementIndex(int size, int matrix[][size], enum indexIJ indexType)
{
    int minIndexI = 0;
    int minIndexJ = 0;
    int min = matrix[0][0];
    for (int i = 0; i < size; i++)
    {
        for (int j = 1; j < size; j++)
        {
            if (matrix[i][j] < min)
            {
                min = matrix[i][j];
                minIndexI = i;
                minIndexJ = j;
            }
        }
    }
    return (indexType == i) ? minIndexI : minIndexJ;
}

int maxMatrixElementIndex(int size, int matrix[][size], enum indexIJ indexType)
{
    int maxIndexI = 0;
    int maxIndexJ = 0;
    int max = matrix[0][0];
    for (int i = 0; i < size; i++)
    {
        for (int j = 1; j < size; j++)
        {
            if (matrix[i][j] > max)
            {
                max = matrix[i][j];
                maxIndexI = i;
                maxIndexJ = j;
            }
        }
    }
    return (indexType == i) ? maxIndexI : maxIndexJ;
}

void emphasizeArrayElement(int index, int array[])
{
    Console_setCursorAttributes(BG_BLUE);
    Console_setCursorPosition(index / 12 + 3, 63 + 5 * (index - 12 * (index / 12)));
    printf("%4i", array[index]);
    Console_reset();
    Console_setCursorPosition(10, 1);
}

void emphasizeMatrixElement(int indexI, int indexJ, int size, int matrix[][size])
{
    Console_setCursorAttributes(BG_BLUE);
    Console_setCursorPosition(3 + indexI, 63 + 5 * indexJ);
    printf("%4i", matrix[indexI][indexJ]);
    Console_reset();
    Console_setCursorPosition(10, 1);
}

void printTask3Interface(int stringBufferSize)
{
    vanish();
    printVerticalLine(1, 100, 61);
    Console_setCursorAttributes(BG_INTENSITY_WHITE);
    Console_setCursorAttributes(FG_BLACK);
    puts("               TASK 3. STRINGS PROCESSING.                                                                              ");
    if (stringBufferSize > 0)
    {
        Console_setCursorAttributes(BG_BLACK);
        Console_setCursorAttributes(FG_WHITE);
        Console_setCursorPosition(1, 78);
        printf("STRING'S BUFFER SIZE: %i", stringBufferSize);
        Console_setCursorPosition(2, 1);
    }
    Console_reset();
    puts("1. Fill the string with user's input.\n"
         "2. Clean the string.\n"
         "3. Output a particular substring.\n"
         "4. Output a list of substrings separated by user's symbol.\n"
         "5. Find the shortest word.\n"
         "6. Find all fractional numbers in the string.\n"
         "7. Compute a multiplication of all integers in the string.");
    printHorizontalLine(1, 61, 9);
}

void printStrings(char string[])
{
    int index = 0;
    for (int i = 0, j = 0; string[index] != '\0'; index++, j++)
    {
        if (index % 58 == 0 && index != 0)
        {
            i++;
            j = 0;
        }
        Console_setCursorPosition(3 + i, 63 + j);
        putchar(string[index]);
    }
    Console_setCursorPosition(2, 78);
    Console_setCursorAttributes(BG_WHITE);
    Console_setCursorAttributes(FG_BLACK);
    printf("CURRENT STRING'S LENGTH: %i", index + 1);
    Console_reset();
    Console_setCursorPosition(10, 1);
}

void printSubstring(char string[], char substring[], int verticalShift)
{
    int i = (strlen(string) != 0) ? 2 + (strlen(string) - 1) / 58 : 2;
    int j = 0;
    int subindex = 0;
    printHorizontalLine(62, 121, 3 + i);
    Console_setCursorAttributes(BG_WHITE);
    Console_setCursorAttributes(FG_BLACK);
    Console_setCursorPosition(3 + i, 85);
    printf("SUBSTRINGS");
    Console_reset();
    for (; substring[subindex] != '\0'; subindex++, j++)
    {
        if (subindex % 58 == 0 && subindex != 0)
        {
            i++;
            j = 0;
        }
        Console_setCursorPosition(4 + i + verticalShift, 63 + j);
        putchar(substring[subindex]);
    }
    Console_setCursorPosition(5 + i + verticalShift, 63);
    Console_setCursorAttributes(BG_WHITE);
    Console_setCursorAttributes(FG_BLACK);
    printf("Length: %i", subindex);
    Console_reset();
    Console_setCursorPosition(10, 1);
}

void emphasizeSubstring(char string[], char substring[], int indexInString, enum conAttribute_e color)
{
    if (color != BG_INTENSITY_BLACK)
    {
        Console_setCursorAttributes(FG_BLACK);
    }
    Console_setCursorAttributes(color);
    int i = (indexInString % 58 == 0 && indexInString != 0) ? (indexInString / 58) - 1 : indexInString / 58;
    int j = indexInString - i * 58;
    int subIndex = 0;
    for (; substring[subIndex] != '\0'; subIndex++, indexInString++, j++)
    {
        if (indexInString % 58 == 0 && indexInString != 0)
        {
            i++;
            j = 0;
        }
        Console_setCursorPosition(3 + i, 63 + j);
        putchar(string[indexInString]);
    }
    Console_reset();
    Console_setCursorPosition(10, 1);
}