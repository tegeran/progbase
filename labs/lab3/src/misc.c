#include "../include/misc.h"


short doubleCmp(double value1, double value2, const double accuracy){
    double difference = value1 - value2;
    if (difference < 0){
        difference *= -1;
    }
    return (difference < accuracy) ? 1 : 0;
}


void terminateIf(const int value, const int exitCode, void(*func)(void*), const int argQuantity, ...){
    if (value){
        if (func != NULL && argQuantity > 0){
            va_list argPtr;
            va_start (argPtr, argQuantity);
            for (int i = 0; i < argQuantity; i++){
                void * funcArg = va_arg(argPtr, void *);
                if ((func != free || funcArg != NULL) && 
                    (func != (void(*)())fclose || funcArg != NULL)){
                    //
                    func(funcArg);
                }
            }
            va_end (argPtr);
        }
        exit(exitCode);
    }
}

int totalArgs(const char * args){   
    int total = 1;                                                      
    for (int i = 0; args[i] != '\0'; i++){                                
        switch (args[i]){                                                 
            case '"':                                                      
            case '\'':{                                                    
                char quote = args[i];                                    
                do {                                                       
                    i++;                                                   
                    
                } while (args[i] != quote && args[i - 1] != '\\');                                                          
                break;                                                         
            }                                                              
            case ',':{                                                     
                total++;                                               
            }                                                              
        }                                                                  
    }                                                 
    return total;
} 