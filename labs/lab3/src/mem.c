#include "../include/mem.h"
bool setPointersToNULL(void ** pointerArray, size_t arraySize){
    if (pointerArray == NULL || arraySize == 0) return false;
    for (int i = 0; i < arraySize; i++)
        pointerArray[i] = NULL;
    return true;
}


void checkPointer(void * pointer, void(*callBackFunction)(void*), void* cbArgument){
    if (pointer == NULL){
        if (callBackFunction != NULL && (callBackFunction != free || cbArgument != NULL) &&
           (callBackFunction != (void(*)())fclose || cbArgument != NULL)){
            callBackFunction(cbArgument);
        }
        exit(EXIT_FAILURE);
    }
}


size_t readFileToString(const char *filePath, char *string, size_t stringSize) {
    if (filePath == NULL || string == NULL || stringSize < 1) return 0;
    FILE *file = fopen(filePath, "rb");
    if (file == NULL) return 0;
    //
    size_t READ_BYTES = fread(string, sizeof(char), stringSize - 1, file);
    if (ferror(file)){
        fclose(file);
        return 0;
    }
    fclose(file);
    string[READ_BYTES] = '\0';
    return READ_BYTES;
}

bool fileExists(const char * filePath){
    FILE * file;
    if (filePath == NULL || (file = fopen(filePath, "rb")) == NULL){
        return false;
    }
    fclose(file);
    return true;
}

bool reallocate(void ** ptr, size_t size){
    if (ptr == NULL || *ptr == NULL) return false;
    void * temp = *ptr;
    if ((*ptr = realloc(*ptr, size)) == NULL){
        *ptr = temp;
        return false;
    }
    return true;
}