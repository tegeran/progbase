#include "../include/provider.h"

Provider * newProviderDyn(const char * name, const double internetTariff, const double loss,
                       const int hostOffer, const double cloudCapacity, const double cloudTariff){
    if (name == NULL ||  name[0] == '\0' || internetTariff < 0 || loss < 0 || loss > 100
        || hostOffer < 0 || cloudCapacity < 0 || cloudTariff < 0){
        return NULL;
    }
    Provider * newProv = CALLOC(1, Provider);
    if (newProv == NULL){
        return NULL;
    }
    strcpy(newProv->name, name);
    newProv->tariff = internetTariff;
    newProv->loss = loss;
    newProv->hostOffer = hostOffer;
    newProv->cloud.capacity = cloudCapacity;
    newProv->cloud.tariff = cloudTariff;
    return newProv;        
}


short sortProviderList(ProviderList * providers){
    if (providers == NULL){
        return FAIL;
    }
    int nonNullProviders = 0;
    for (int i = 0; i < PROV_LIST_CAPACITY; i++){
        //
        if (providers->list[i] != NULL){
            //
            nonNullProviders++;
            if (i != 0){
                //
                for (int j = i; j > 0 && providers->list[j - 1] == NULL; j--){
                    providers->list[j - 1] = providers->list[j];
                    providers->list[j] = NULL;
                }
            }
        }
    }
    providers->size = nonNullProviders;
    return SUCCESS;    
}


short addToProviderList(Provider * provider, ProviderList * providers){
    if (provider == NULL || providers == NULL){
        return FAIL;
    }
    sortProviderList(providers);
    for (int i = 0; i < PROV_LIST_CAPACITY; i++){
        if (providers->list[i] == NULL){
            providers->list[i] = provider;
            providers->size = i + 1;
            return SUCCESS;
        }
    }
    return FAIL;
}


short removeProviderFromListDyn(const int index, ProviderList * providers, Provider ** removedProvider){
    if (providers == NULL || index < 0 ||
        index >= PROV_LIST_CAPACITY || providers->list[index] == NULL){
        *removedProvider = NULL;
        return FAIL;
    }
    *removedProvider = providers->list[index];
    providers->list[index] = NULL;
    return sortProviderList(providers);
}


short compareProvider(const Provider * provider1, const Provider * provider2){
    if (provider1 == NULL || provider2 == NULL){
        return FAIL;
    }
    return (!strcmp(provider1->name,     provider2->name) &&
            doubleCmp(provider1->tariff, provider2->tariff, ACCURACY) &&
            doubleCmp(provider1->loss,   provider2->loss, ACCURACY) && 
            provider1->hostOffer   ==    provider2->hostOffer &&
            compareDataCloud(&(provider1->cloud), &(provider2->cloud)) == 1)
            ? 1 : 0;
}


short compareProviderList(const ProviderList * list1, const ProviderList * list2){
    if (list1 == NULL || list2 == NULL){
        return FAIL;
    }
    if (list1->size != list2->size){
        return 0;
    }
    for (int i = 0; i < list1->size; i++){
        short result = compareProvider(list1->list[i], list2->list[i]);
        if (result != 1){
            return result;
        }
    }
    return 1;
}


short rewriteProvider(Provider * provider, const char * name, const double internetTariff, const double loss,
    const int hostOffer, const double cloudCapacity, const double cloudTariff){
    if (provider == NULL || name == NULL || name[0] == '\0' || internetTariff < 0 || loss < 0 || loss > 100
        || hostOffer < 0 || cloudCapacity < 0 || cloudTariff < 0){
        return FAIL;
    }
    strcpy(provider->name, name);
    provider->tariff = internetTariff;
    provider->loss = loss;
    provider->hostOffer = hostOffer;
    provider->cloud.capacity = cloudCapacity;
    provider->cloud.tariff = cloudTariff;
    return SUCCESS;  
}


short updateProvider(Provider * provider, char * field, char * newData){
    if (provider == NULL || field == NULL || newData == NULL){
        return FAIL;
    }
    if (!strcmp(field, "name")){
        if (strlen(newData) <= 0 || strchr(newData, ':') != NULL){
            return FAIL;
        }
        strcpy(provider->name, newData);
        return SUCCESS;
    }
    if (!strcmp(field, "tariff")){
        double readTariff;
        if (sscanf(newData, "%lf", &readTariff) == 0 || readTariff < 0){
            return FAIL;
        }
        provider->tariff = readTariff;
        return SUCCESS;
    }
    if (!strcmp(field, "loss")){
        double readLoss;
        if (sscanf(newData, "%lf", &readLoss) == 0 || readLoss < 0 || readLoss > 100){
            return FAIL;
        }
        provider->loss = readLoss;
        return SUCCESS;
    }
    if (!strcmp(field, "host offer")){
        int readOffer;
        if (sscanf(newData, "%i", &readOffer) == 0 || readOffer < 0){
            return FAIL;
        }
        provider->hostOffer = readOffer;
        return SUCCESS;
    }
    if (!strcmp(field, "cloud capacity")){
        double readCapacity;
        if (sscanf(newData, "%lf", &readCapacity) == 0 || readCapacity < 0){
            return FAIL;
        }
        provider->cloud.capacity = readCapacity;
        return SUCCESS;
    }
    if (!strcmp(field, "cloud tariff")){
        double readCloudTariff;
        if (sscanf(newData, "%lf", &readCloudTariff) == 0 || readCloudTariff < 0){
            return FAIL;
        }
        provider->cloud.tariff = readCloudTariff;
        return SUCCESS;
    }
    return FAIL;
}

short highestTariffProvidersDyn(const ProviderList * providers, ProviderList * result, const int quantity){
    if (providers == NULL || result == NULL || quantity <= 0 || quantity > providers->size){
        return FAIL;
    }
    if (copyProviderListDyn(providers, result) == FAIL){
        return FAIL;
    }
    //selection sort till (quantity) sorted providers in (result):
    int i = 0;
    for (; i < quantity; i++){
        int maxIndex = i;
        for (int j = i; j < result->size; j++){
            if (result->list[j]->tariff > result->list[maxIndex]->tariff){
                maxIndex = j;
            }
        }
        if (i != maxIndex){
            Provider * temp = result->list[maxIndex];
            for (int j = maxIndex; j > i; j--){
                result->list[j] = result->list[j - 1];
            }
            result->list[i] = temp;
        }
    }
    result->size = quantity;
    for(;i < providers->size; i++){
        assert(result->list[i] != NULL);
        if (result->list[i] != NULL){
            free(result->list[i]);
            result->list[i] = NULL;
        }
    }
    return SUCCESS;
}


short freeProviderListDyn(ProviderList * providers){
    if (providers == NULL){
        return FAIL;
    }
    for (int i = 0; i < PROV_LIST_CAPACITY; i++){
        if (providers->list[i] != NULL){
            free(providers->list[i]);
            providers->list[i] = NULL;
        }
    }
    providers->size = 0;
    return SUCCESS;
}

short freeExessProvidersDyn(ProviderList * providers){
    if (providers == NULL){
        return FAIL;
    }
    for (int i = providers->size; i < PROV_LIST_CAPACITY; i++){
        if (providers->list[i] != NULL){
            free(providers->list[i]);
            providers->list[i] = NULL;
        }
    }
    return SUCCESS;
}

// ASSERTIONS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
short copyProviderListDyn(const ProviderList * source, ProviderList * dest){
    if (source == NULL || dest == NULL){
        return FAIL;
    }
    if (mallocProviderList(dest, source->size) == FAIL){
        assert(0);
        return FAIL;
    }
    dest->size = source->size;
    int i = 0;
    for(; i < source->size; i++){
        *(dest->list[i]) = *(source->list[i]);
    }
    setPointersToNULL((void**)dest->list + i, PROV_LIST_CAPACITY - i);
    return SUCCESS;

}


int providerToString(const Provider * provider, char * string, const int stringSize){
    if (provider == NULL || string == NULL || stringSize <= 0){
        if (string != NULL){
            string[0] = '\0';
        }
        return FAIL;
    }
    int nwritten = snprintf(string, stringSize, "%s:%.2lf#%.2lf#%i$", provider->name, provider->tariff,
                          provider->loss, provider->hostOffer);
    if (nwritten >= stringSize){
        string[0] = '\0';
        return FAIL;
    }
    int nwrittenCloud = dataCloudToString(&(provider->cloud), string + nwritten, stringSize - nwritten);
    if (nwrittenCloud == FAIL){
        string[0] = '\0';
        return FAIL;
    }
    return nwritten + nwrittenCloud;
}


int dataCloudToString(const DataCloud * cloud, char * string, const int stringSize){
    if (cloud == NULL || string == NULL || stringSize <= 0){
        return FAIL;
    }
    int nwritten = snprintf(string, stringSize, "%.2lf@%.2lf\n", cloud->capacity, cloud->tariff);
    if (nwritten >= stringSize){
        string[0] = '\0';
        return FAIL;
    }
    return nwritten;
}


int stringToProvider(Provider * provider, const char * string){
    if (provider == NULL || string == NULL){
        return FAIL;
    }
    char formattingString[50];
    snprintf(formattingString, 50, "%%%i[^:]%%*c", PROVIDER_NAME_SIZE); 
    strcat(formattingString, "%lf#%lf#%i$%n");
    int nread;
    int fieldsFilled = sscanf(string, formattingString, &provider->name, &provider->tariff, 
                       &provider->loss, &provider->hostOffer, &nread);
    if (fieldsFilled != 4 || nread != strchr(string, '$') - string + 1||
        provider->tariff < 0 || 
        provider->loss < 0 || provider->loss > 100 || provider->hostOffer < 0 ||
        isnan(provider->tariff) || isnan(provider->loss)){
        return FAIL;
    }
    int returned = stringToDataCloud(&provider->cloud, string + nread);
    if (returned == FAIL){
        return FAIL;
    }
    return returned + nread;
}


int stringToDataCloud(DataCloud * cloud, const char * string){
    if (cloud == NULL || string == NULL){
        return FAIL;
    }
    int nread = 0;
    int fieldsFilled = sscanf(string, "%lf@%lf\n%n", &cloud->capacity, &cloud->tariff, &nread);
    int slashNPos = strchr(string, '\n') - string + 1;
    if (nread == slashNPos + 1 && isspace(string[nread - 1])){
        nread--;
    }
    if (fieldsFilled != 2 || nread <= 0 || cloud->capacity < 0 || nread != slashNPos || 
        cloud->tariff < 0 || isnan(cloud->capacity) || isnan(cloud->tariff)){
        return FAIL;
    }
    return nread;
}


short compareDataCloud(const DataCloud * cloud1, const DataCloud * cloud2){
    if (cloud1 == NULL || cloud2 == NULL){
        return FAIL;
    }
    return (doubleCmp(cloud1->capacity, cloud2->capacity, ACCURACY)
            && doubleCmp(cloud1->tariff, cloud2->tariff, ACCURACY)) ? 1 : 0;
}


int providerListToString(const ProviderList * list, char * string, const int stringSize){
    if (list == NULL || string == NULL || stringSize <= 0){
        if (string != NULL){
            string[0] = '\0';
        }
        return FAIL;
    }
    int nwritten = 0;
    for (int i = 0; i < list->size; i++){
        int returned = providerToString(list->list[i], string + nwritten, stringSize - nwritten);
        if (returned == FAIL){
            string[0] = '\0';
            return FAIL;
        }
        nwritten += returned;
    }
    if (nwritten == 0){
        string[0] = '\0';
    }
    return nwritten;
}


short stringToProviderList(ProviderList * list, const char * string){
    if (list == NULL || string == NULL){
        return FAIL;
    }
    list->size = 0;
    int read = 0;
    for (; string[read] != '\0'; list->size++){
        int returned = stringToProvider(list->list[list->size], string + read);
        if (returned == FAIL){
            list->size = 0;
            return FAIL;
        }
        read += returned;
    }
    return SUCCESS;
}


short mallocProviderList(ProviderList * list, const int quantity){
    if (list == NULL){
        return FAIL;
    }
    for(int i = 0; i < quantity; i++){
        if ((list->list[i] = CALLOC(1, Provider)) == NULL){
            list->size = i + 1;
            freeProviderListDyn(list);
            return FAIL;
        }
    }
    return SUCCESS;
}


short providerListToFile(const ProviderList * list, const char * filePath){
    if (list == NULL || filePath == NULL || !fileExists(filePath)){
        assert(0);
        return FAIL;
    }
    FILE * file = fopen(filePath, "w");
    if (file == NULL){
        return FAIL;
    }
    const long unsigned int STRINGSIZE = list->size * (PROVIDER_NAME_SIZE +
                                         10 + sizeof(double) * 8 * 4 + 8 * sizeof(int));
    char string [STRINGSIZE];
    if (providerListToString(list, string, STRINGSIZE) == FAIL){
        fclose(file);
        return FAIL;
    }
    if (fputs(string, file) == EOF){
        fclose(file);
        return FAIL;
    }
    fclose(file);
    return SUCCESS;
}


short fileToProviderList(ProviderList * list, const char * filePath){
    if (list == NULL || filePath == NULL){
        return FAIL;
    }
    const long unsigned int STRINGSIZE = PROV_LIST_CAPACITY * (PROVIDER_NAME_SIZE +
                                         10 + sizeof(double) * 8 * 4 + 8 * sizeof(int));
    char string [STRINGSIZE];
    if (readFileToString(filePath, string, STRINGSIZE) == FAIL){
        return FAIL;
    }
    if (string[0] == '\0' || stringToProviderList(list, string) == FAIL){
        return FAIL;
    }
    return SUCCESS;
}