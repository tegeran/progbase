#include "../include/user_friendly.h"
#include <progbase.h>

ConsoleCursorPosition printAt(const ConsolePrintSetup * pattern    , 
                                                 char * string     , 
                                                  int   row        , 
                                                  int   column     , 
                                                  int   charsPerRow){
    // verifying arguments
    ConsoleSize conSize = Console_size();
    if (pattern == NULL         || string == NULL     || pattern->stdFileStream == NULL || 
        row <= 0                || row > conSize.rows || column > conSize.columns       ||
        charsPerRow < AUTO_SIZE || charsPerRow + column - 1 > conSize.columns             ){
    //
        return (ConsoleCursorPosition){1, 1};                           
    }
    //
    if (charsPerRow == AUTO_SIZE)
        charsPerRow = getStringMaxWidth(string) + ((pattern->frame != NULL) ? 2 : 0);       
    //
    if (column == CENTER_ALIGN)
            column = (conSize.columns - charsPerRow) / 2 + 1;
    else if (column < 0)
            column =  conSize.columns - charsPerRow + column + 2;
    //
    const size_t INITIAL_ROW = row;
    if (pattern->frame != NULL){
        row++;
        column++;
        if (charsPerRow != AUTO_SIZE) charsPerRow -= 2;
    }
    if (pattern->frame != NULL && charsPerRow <= 2) 
        return (ConsoleCursorPosition){1, 1};   
    //
    bool consoleColorWasChanged = setColor(pattern->color);
    Console_setCursorPosition(row, column);
    int col = 0;
    for (int i = 0; string[i] != '\0'; i++){
        if ((string[i] == '\n' || col == charsPerRow) &&
            !isUnicodeExtraSymbol(string[i])){
            //
            while (col != charsPerRow){
                fputc(' ', pattern->stdFileStream);
                col++;
            }
            Console_setCursorPosition(++row, column);
            if (string[i] != '\n') i--;
            col = 0;
        } else {
            fputc(string[i], pattern->stdFileStream);
            if (!isUnicodeExtraSymbol(string[i])) col++;
        }
    }
    while (col != charsPerRow){
        fputc(' ', pattern->stdFileStream);
        col++;
    }
    if (pattern->frame != NULL)
        return printFrame(pattern->frame, INITIAL_ROW, column - 1, row - INITIAL_ROW + 2, charsPerRow + 2);
    if (consoleColorWasChanged) Console_reset();
    return (ConsoleCursorPosition){.row = row, .column = column + charsPerRow};
}

ConsoleCursorPosition printFrame(FramePrintSetup * frame      , 
                                    const size_t   row        , 
                                    const size_t   column     , 
                                    const size_t   frameHeight, 
                                    const size_t   frameWidth ){
    //
    ConsoleSize conSize = Console_size();
    if (frame == NULL            || frame->stdFileStream == NULL || row > conSize.rows ||
        column > conSize.columns || frameHeight == 0             || frameWidth ==0     || 
        row == 0                 || column == 0                  || 
        frameWidth + column - 1 > conSize.columns) {
    //
        return (ConsoleCursorPosition){1, 1};                          
    }
    //
    bool consoleColorWasChanged = setColor(frame->color);
    // printing corners
    Console_setCursorPosition(row, column);
    fputs(frame->leftUpper,  frame->stdFileStream);
    //
    Console_setCursorPosition(row, column + frameWidth - 1);
    fputs(frame->rightUpper, frame->stdFileStream);
    //
    Console_setCursorPosition(row + frameHeight - 1, column);
    fputs(frame->leftLower,  frame->stdFileStream);
    //
    Console_setCursorPosition(row + frameHeight - 1, column + frameWidth - 1);
    fputs(frame->rightLower, frame->stdFileStream);
    // printing lines
    printHorizontal(frame->stdFileStream, frame->horizontal, row                  , column + 1, frameWidth - 2);
    printHorizontal(frame->stdFileStream, frame->horizontal, row + frameHeight - 1, column + 1, frameWidth - 2);
    //
    printVertical(frame->stdFileStream, frame->vertical, row + 1, column,                  frameHeight - 2);
    printVertical(frame->stdFileStream, frame->vertical, row + 1, column + frameWidth - 1, frameHeight - 2);
    //
    if (consoleColorWasChanged) Console_reset();
    return (ConsoleCursorPosition){
        .row = row + frameHeight - 1,
        .column = column + frameWidth - 1
    };
}


ConsoleCursorPosition printHorizontal(FILE * stdFileStream, 
                              const   char   unicodeChar[], 
                              const size_t   row          ,
                              const size_t   column       , 
                              const size_t   length       ){
    //
    if (stdFileStream == NULL || unicodeChar == NULL || unicodeChar[0] == '\0' || 
        row == 0              || column == 0         || length == 0              ){
    //
        return (ConsoleCursorPosition){1, 1};
    }
    Console_setCursorPosition(row, column);
    for (int i = 0; i < length; i++){
        fputs(unicodeChar, stdFileStream);
    }
    return (ConsoleCursorPosition){.row = row, .column = column + length};
}


ConsoleCursorPosition printVertical(FILE * stdFileStream, 
                            const   char   unicodeChar[], 
                            const size_t   row          ,
                            const size_t   column       , 
                            const size_t   length       ){
    //
    if (stdFileStream == NULL || unicodeChar == NULL || unicodeChar[0] == '\0' || 
        row == 0              || column == 0         || length == 0              ){
    //
        return (ConsoleCursorPosition){1, 1};
    }
    for (int i = 0; i < length; i++){
        Console_setCursorPosition(row + i, column);
        fputs(unicodeChar, stdFileStream);
    }
    return (ConsoleCursorPosition){.row = row + length, .column = column};
}


bool setColor(const ColorSetup * color){
    //
    if (color == CURRENT_COLORS) return false;
    //
    bool wetherFGColorWasChanged = false;
    if (color == DEFAULT_COLORS || color->fg == DEFAULT_COLOR){
        //
        fwrite("\033[38;5;39m", 1, 10, stdout);
        wetherFGColorWasChanged = true;
        //
    } else if (color->fg != CURRENT_COLOR){
        //
        short fgRed   = (color->fg % 1000 - color->fg % 100) / 100;
        short fgGreen = (color->fg % 100 - color->fg % 10) / 10;
        short fgBlue  =  color->fg % 10;
        printf("\033[38;5;%im", 16 + 36 * fgRed + 6 * fgGreen + fgBlue);
        wetherFGColorWasChanged = true;
    }
    //
    if (color == DEFAULT_COLORS || color->bg == DEFAULT_COLOR){
        fwrite("\033[38;5;49m", 1, 10, stdout);
        return true;
    }
    if (color->bg != CURRENT_COLOR){
        short bgRed   = (color->bg % 1000 - color->bg % 100) / 100;
        short bgGreen = (color->bg % 100 - color->bg % 10) / 10;
        short bgBlue  =  color->bg % 10;
        printf("\033[48;5;%im", 16 + 36 * bgRed + 6 * bgGreen + bgBlue);
        return true;
    }
    return wetherFGColorWasChanged;
}


void vanish(const ColorSetup * newConsoleColor){
    if (newConsoleColor != CURRENT_COLORS)
        setColor(newConsoleColor);
    fflush(stdout);
    system("clear");
}

int readStrRecur(char * buffer        ,
         const size_t   bufferSize    ,
         const   char   terminalSymbol){
    //
    return (bufferSize > 1 && ((*buffer = getchar()) != terminalSymbol)) ? 
            1 + readStrRecur(buffer + 1, bufferSize - 1, terminalSymbol) :
            ((*buffer = '\0'), 0);
}

int readString(char * buffer        ,
       const size_t   bufferSize    ,
       const   char   terminalSymbol){
    //
    return (buffer == NULL || bufferSize == 0) ? 0 : readStrRecur(buffer, bufferSize, terminalSymbol);
}




ConsoleCursorPosition printProviderList(const ProviderList * list, ColorSetup * color, int row, int column){
    if (list == NULL){
        //
        return (ConsoleCursorPosition){1, 1};
    }

    if (list->size == 0){
        return printHeader(&HEADER_BIG_MSG_ALIGATOR2_GREEN_NO_FRAME,"\n"
            ":::::::::  :::            :::     ::::    ::: :::    :::\n"  
            ":+:    :+: :+:          :+: :+:   :+:+:   :+: :+:   :+: \n"  
            "+:+    +:+ +:+         +:+   +:+  :+:+:+  +:+ +:+  +:+  \n"  
            "+#++:++#+  +#+        +#++:++#++: +#+ +:+ +#+ +#++:++   \n"  
            "+#+    +#+ +#+        +#+     +#+ +#+  +#+#+# +#+  +#+  \n"  
            "#+#    #+# #+#        #+#     #+# #+#   #+#+# #+#   #+# \n"  
            "#########  ########## ###     ### ###    #### ###    ###\n\n"  
            "      :::        :::::::::::  ::::::::  :::::::::::\n"
            "      :+:            :+:     :+:    :+:     :+:    \n"
            "      +:+            +:+     +:+            +:+    \n"
            "      +#+            +#+     +#++:++#++     +#+    \n"
            "      +#+            +#+            +#+     +#+    \n"
            "      #+#            #+#     #+#    #+#     #+#    \n"
            "      ########## ###########  ########      ###    \n"
        , 2, -33, AUTO_SIZE);
    }
    if (column == CENTER_ALIGN){
        column = (Console_size().columns - PROV_SPREADSH_NUMERATION_WITDH - 
            PROV_SPREADSH_NAME_WIDTH - 5 * PROV_SPREADSH_VALUES_WIDTH - 8) / 2 + 1;
    }
    else if (column < 0){
        column = Console_size().columns - (PROV_TOTAL_FIELDS - 1) * PROV_SPREADSH_VALUES_WIDTH - 
                 PROV_SPREADSH_NAME_WIDTH - PROV_SPREADSH_NUMERATION_WITDH - 
                 PROV_TOTAL_FIELDS + column;
    }
    
    const SpreadsheetSetup LEFT_PART = {
        .stdFileStream = stdout,              
        .color         = color,                      
        .horizontal    = "═",      
        .vertical      = "║",        
        .leftUpper     = "╔",       
        .rightUpper    = "",      
        .leftLower     = "╚",       
        .rightLower    = "",      
        .leftCentral   = "╠",     
        .rightCentral  = "",    
        .centralUpper  = "╦",    
        .centralLower  = "╩",    
        .central       = "╬"          
    };
    const SpreadsheetSetup CENTRAL_PART = {
        .stdFileStream = stdout,              
        .color         = color,                      
        .horizontal    = "═",      
        .vertical      = "║",        
        .leftUpper     = "╦",       
        .rightUpper    = "╦",      
        .leftLower     = "╩",       
        .rightLower    = "╩",      
        .leftCentral   = "╬",     
        .rightCentral  = "╬",    
        .centralUpper  = "╦",    
        .centralLower  = "╩",    
        .central       = "╬"          
    };
    const SpreadsheetSetup RIGHT_PART = {
        .stdFileStream = stdout,              
        .color         = color,
        .horizontal    = "═",                    
        .vertical      = "║",                      
        .leftUpper     = "╦",                     
        .rightUpper    = "╗",                    
        .leftLower     = "╩",                     
        .rightLower    = "╝",                    
        .leftCentral   = "╬",                   
        .rightCentral  = "╣",                  
        .centralUpper  = "╦",                  
        .centralLower  = "╩",                  
        .central       = "╬" 
    };
    printSpreadsheet(&LEFT_PART, row, column, list->size + 1, 1,
                     PROV_SPREADSH_ROW_HEIGHT,
                     PROV_SPREADSH_NUMERATION_WITDH);
    printSpreadsheet(&CENTRAL_PART, row, column + PROV_SPREADSH_NUMERATION_WITDH + 1,
                     list->size + 1, 1, PROV_SPREADSH_ROW_HEIGHT,
                     PROV_SPREADSH_NAME_WIDTH);
    const ConsolePrintSetup CON_PRINT_PROV_SPREADSH = {
        .stdFileStream = stdout,            
        .color  = color,                              
        .frame = NULL   
    };
    printAt(&CON_PRINT_PROV_SPREADSH, "＃", row + 1, column + 1, 1);
    //
    printAt(&CON_PRINT_PROV_SPREADSH, "Name", row + 1, column + PROV_SPREADSH_NUMERATION_WITDH + 2,
            PROV_SPREADSH_NAME_WIDTH);
    //
    printAt(&CON_PRINT_PROV_SPREADSH, "Tariff ", row + 1, column +
            PROV_SPREADSH_NUMERATION_WITDH + 
            PROV_SPREADSH_NAME_WIDTH + 3, PROV_SPREADSH_VALUES_WIDTH);
    //
    printAt(&CON_PRINT_PROV_SPREADSH, " Avg\n packetloss %", row + 1, column + PROV_SPREADSH_NUMERATION_WITDH + 
            PROV_SPREADSH_NAME_WIDTH + PROV_SPREADSH_VALUES_WIDTH + 4, PROV_SPREADSH_VALUES_WIDTH);
    //
    printAt(&CON_PRINT_PROV_SPREADSH, "Total\n domens ", row + 1, column + PROV_SPREADSH_NUMERATION_WITDH + 
            PROV_SPREADSH_NAME_WIDTH + 2 * (PROV_SPREADSH_VALUES_WIDTH) + 5, PROV_SPREADSH_VALUES_WIDTH);
    //            
    printAt(&CON_PRINT_PROV_SPREADSH, "Data cloud\n  capacity ", row + 1, column + PROV_SPREADSH_NUMERATION_WITDH + 
            PROV_SPREADSH_NAME_WIDTH + 3 * (PROV_SPREADSH_VALUES_WIDTH) + 6, PROV_SPREADSH_VALUES_WIDTH);
    //
    printAt(&CON_PRINT_PROV_SPREADSH, "Data cloud\n   tariff ", row + 1, column + PROV_SPREADSH_NUMERATION_WITDH + 
            PROV_SPREADSH_NAME_WIDTH + 4 * (PROV_SPREADSH_VALUES_WIDTH) + 7, PROV_SPREADSH_VALUES_WIDTH);
    //            
    char temp[PROVIDER_NAME_SIZE];
    for (int i = 0; i < list->size; i++){
        snprintf(temp, PROVIDER_NAME_SIZE, "%i", i + 1);
        printAt(&CON_PRINT_PROV_SPREADSH, temp, row + i * (PROV_SPREADSH_ROW_HEIGHT + 1) 
                + PROV_SPREADSH_ROW_HEIGHT + 2,
                column + 1, PROV_SPREADSH_NUMERATION_WITDH);
        //
        if (strcmp("", list->list[i]->name)){
            printAt(&CON_PRINT_PROV_SPREADSH, list->list[i]->name, row + i * (PROV_SPREADSH_ROW_HEIGHT + 1) 
            + PROV_SPREADSH_ROW_HEIGHT + 2,
            column + PROV_SPREADSH_NUMERATION_WITDH + 2,
            PROV_SPREADSH_NAME_WIDTH);
        }
        //
        if (list->list[i]->tariff >= 0){
            snprintf(temp, PROVIDER_NAME_SIZE, "%.2lf", list->list[i]->tariff);
            printAt(&CON_PRINT_PROV_SPREADSH, temp, row + i * (PROV_SPREADSH_ROW_HEIGHT + 1) + PROV_SPREADSH_ROW_HEIGHT + 2,
                    column + PROV_SPREADSH_NUMERATION_WITDH + 
                    PROV_SPREADSH_NAME_WIDTH + 3, PROV_SPREADSH_VALUES_WIDTH);
        }
        //
        if (list->list[i]->loss >= 0 && list->list[i]->loss <= 100){
            snprintf(temp, PROVIDER_NAME_SIZE, "%.2lf", list->list[i]->loss);        
            printAt(&CON_PRINT_PROV_SPREADSH, temp, row + i * (PROV_SPREADSH_ROW_HEIGHT + 1) 
                    + PROV_SPREADSH_ROW_HEIGHT + 2,
                    column + PROV_SPREADSH_NUMERATION_WITDH + 
                    PROV_SPREADSH_NAME_WIDTH + PROV_SPREADSH_VALUES_WIDTH + 4, PROV_SPREADSH_VALUES_WIDTH);
        } 
        //
        if (list->list[i]->hostOffer >= 0){
            snprintf(temp, PROVIDER_NAME_SIZE, "%i", list->list[i]->hostOffer);     
            printAt(&CON_PRINT_PROV_SPREADSH, temp, row + i * (PROV_SPREADSH_ROW_HEIGHT + 1) 
                    + PROV_SPREADSH_ROW_HEIGHT + 2,
                    column + PROV_SPREADSH_NUMERATION_WITDH + 
                PROV_SPREADSH_NAME_WIDTH + 2 * (PROV_SPREADSH_VALUES_WIDTH) + 5, PROV_SPREADSH_VALUES_WIDTH);
        }
        //
        if (list->list[i]->cloud.capacity > 0){
            //
            snprintf(temp, PROVIDER_NAME_SIZE, "%.2lf", list->list[i]->cloud.capacity);
            printAt(&CON_PRINT_PROV_SPREADSH, temp, row + i * (PROV_SPREADSH_ROW_HEIGHT + 1) 
                    + PROV_SPREADSH_ROW_HEIGHT + 2,
                    column + PROV_SPREADSH_NUMERATION_WITDH + 
                    PROV_SPREADSH_NAME_WIDTH + 3 * (PROV_SPREADSH_VALUES_WIDTH) + 6, PROV_SPREADSH_VALUES_WIDTH);
            //
            if (list->list[i]->cloud.tariff >= 0){
                snprintf(temp, PROVIDER_NAME_SIZE, "%.2lf", list->list[i]->cloud.tariff); 
                printAt(&CON_PRINT_PROV_SPREADSH, temp, row + i * (PROV_SPREADSH_ROW_HEIGHT + 1) 
                        + PROV_SPREADSH_ROW_HEIGHT + 2,
                        column + PROV_SPREADSH_NUMERATION_WITDH + 
                        PROV_SPREADSH_NAME_WIDTH + 4 * (PROV_SPREADSH_VALUES_WIDTH) + 7, PROV_SPREADSH_VALUES_WIDTH);
            }   
        }
    }  
    return printSpreadsheet(&RIGHT_PART, row, column + PROV_SPREADSH_NUMERATION_WITDH + 
                     PROV_SPREADSH_NAME_WIDTH + 2, list->size + 1, PROV_TOTAL_FIELDS - 1, 
                     PROVIDER_NAME_SIZE / PROV_SPREADSH_NAME_WIDTH, 
                     PROV_SPREADSH_VALUES_WIDTH);
}


void getProviderData(Provider * provider){
    assert (provider != NULL);
    Console_setCursorAttributes(ATTR_BRIGHT);
    ConsoleCursorPosition cursor = printAt(&CON_PRINT_MSG_FULL_GREEN, 
                                     "\n          Name (2 - 50 symbols, no ':' symbol)          \n\n\n\n\n",
                                     20, CENTER_ALIGN, AUTO_SIZE);
    setColor(&COLOR_BLACK_FG_GREEN_BG);
    Console_setCursorPosition(cursor.row - 3, cursor.column - 48);
    while (readString(provider->name, PROVIDER_NAME_SIZE, TERMINAL_SYMBOL) <= 1 ||
           strchr(provider->name, ':') != NULL){
        Console_setCursorAttributes(ATTR_BRIGHT);
        printAt(&CON_PRINT_MSG_RED, "\n          Name (2 - 50 symbols, no ':' symbol)          \n\n\n\n\n",
                         20, CENTER_ALIGN, AUTO_SIZE);
        setColor(&COLOR_WHITE_FG_RED_BG);
        Console_setCursorPosition(cursor.row - 3, cursor.column - 48);
    }
    //
    //
    Console_setCursorAttributes(ATTR_BRIGHT);
    printAt(&CON_PRINT_MSG_FULL_GREEN, "\n                      Tariff ( ⩾ 0)                     \n\n\n\n\n",
                     20, CENTER_ALIGN, AUTO_SIZE);
    setColor(&COLOR_BLACK_FG_GREEN_BG);
    Console_setCursorPosition(cursor.row - 3, cursor.column - 48);
    fflush(stdout);
    int scaned = 0;
    while ((scaned = scanf("%lf", &provider->tariff)) == 0 || provider->tariff < 0){
        if (scaned == 0){
            getchar();
        }
        Console_setCursorAttributes(ATTR_BRIGHT);
        printAt(&CON_PRINT_MSG_RED, "\n                      Tariff ( ⩾ 0)                     \n\n\n\n\n",
                         20, CENTER_ALIGN, AUTO_SIZE);
        setColor(&COLOR_WHITE_FG_RED_BG);
        Console_setCursorPosition(cursor.row - 3, cursor.column - 48);
    }
    //
    //
    Console_setCursorAttributes(ATTR_BRIGHT);
    printAt(&CON_PRINT_MSG_FULL_GREEN, "\n                PacketLoss ( ⩾ 0 & ⩽ 100)               \n\n\n\n\n",
                     20, CENTER_ALIGN, AUTO_SIZE);
    setColor(&COLOR_BLACK_FG_GREEN_BG);
    Console_setCursorPosition(cursor.row - 3, cursor.column - 48);
    while ((scaned = scanf("%lf", &provider->loss)) == 0 || provider->loss < 0 || provider->loss > 100){
        if (scaned == 0){
            getchar();
        }
        Console_setCursorAttributes(ATTR_BRIGHT);
        printAt(&CON_PRINT_MSG_RED, "\n                PacketLoss ( ⩾ 0 & ⩽ 100)               \n\n\n\n\n",
                         20, CENTER_ALIGN, AUTO_SIZE);
        setColor(&COLOR_WHITE_FG_RED_BG);
        Console_setCursorPosition(cursor.row - 3, cursor.column - 48);
    }
    //
    //
    Console_setCursorAttributes(ATTR_BRIGHT);
    printAt(&CON_PRINT_MSG_FULL_GREEN, "\n                   Total domens ( ⩾ 0)                  \n\n\n\n\n",
                     20, CENTER_ALIGN, AUTO_SIZE);
    setColor(&COLOR_BLACK_FG_GREEN_BG);
    Console_setCursorPosition(cursor.row - 3, cursor.column - 48);
    while ((scaned = scanf("%i", &provider->hostOffer)) == 0 || provider->hostOffer < 0){
        if (scaned == 0){
            getchar();
        }
        Console_setCursorAttributes(ATTR_BRIGHT);
        printAt(&CON_PRINT_MSG_RED, "\n                   Total domens ( ⩾ 0)                  \n\n\n\n\n",
                         20, CENTER_ALIGN, AUTO_SIZE);
        setColor(&COLOR_WHITE_FG_RED_BG);
        Console_setCursorPosition(cursor.row - 3, cursor.column - 48);
    }
    Console_setCursorAttributes(ATTR_BRIGHT);
    printAt(&CON_PRINT_MSG_FULL_GREEN, "\n                    Add data cloud?                     \n\n\n\n\n",
                         20, CENTER_ALIGN, AUTO_SIZE);
    if (userMenu(NULL, &MENU_HORI_FULL_GREEN, cursor.row - 3, CENTER_ALIGN, AUTO_SIZE, 2,
        "YES", "NO") == 0){
        //
        //
        Console_setCursorAttributes(ATTR_BRIGHT);
        printAt(&CON_PRINT_MSG_FULL_GREEN, "\n               Data cloud capacity ( > 0)               \n\n\n\n\n",
                20, CENTER_ALIGN, AUTO_SIZE);
        setColor(&COLOR_BLACK_FG_GREEN_BG);
        Console_setCursorPosition(cursor.row - 3, cursor.column - 48);
        while ((scaned = scanf("%lf", &provider->cloud.capacity)) == 0 || 
               provider->cloud.capacity <= 0){
            if (scaned == 0){
                getchar();
            }
            Console_setCursorAttributes(ATTR_BRIGHT);
            printAt(&CON_PRINT_MSG_RED, "\n               Data cloud capacity ( > 0)               \n\n\n\n\n",
                    20, CENTER_ALIGN, AUTO_SIZE);
            setColor(&COLOR_WHITE_FG_RED_BG);
            Console_setCursorPosition(cursor.row - 3, cursor.column - 48);
        }
        //
        //
        Console_setCursorAttributes(ATTR_BRIGHT);
        printAt(&CON_PRINT_MSG_FULL_GREEN, "\n                Data cloud tariff ( ⩾ 0)                \n\n\n\n\n",
                20, CENTER_ALIGN, AUTO_SIZE);
        setColor(&COLOR_BLACK_FG_GREEN_BG);
        Console_setCursorPosition(cursor.row - 3, cursor.column - 48);
        while ((scaned = scanf("%lf", &provider->cloud.tariff)) == 0 || provider->cloud.tariff < 0){
            if (scaned == 0){
                getchar();
            }
            Console_setCursorAttributes(ATTR_BRIGHT);
            printAt(&CON_PRINT_MSG_RED, "\n                Data cloud tariff ( ⩾ 0)                \n\n\n\n\n",
                    20, CENTER_ALIGN, AUTO_SIZE);
            setColor(&COLOR_WHITE_FG_RED_BG);
            Console_setCursorPosition(cursor.row - 3, cursor.column - 48);
        }
    }
    else{
        provider->cloud.capacity = 0;
        provider->cloud.tariff = 0;
    } 
}

ConsoleCursorPosition printSpreadsheet(const SpreadsheetSetup * setup     ,
                                                          int   row       ,
                                                          int   column    ,
                                                          //
                                                          int   rows      ,
                                                          int   columns   ,
                                                          //
                                                          int   rowSize   ,
                                                          int   columnSize){
    ConsoleSize conSize = Console_size();
    if (setup == NULL || column + columns * (columnSize + 1) > conSize.columns ||
        row + rows * (rowSize + 1) > conSize.rows){
        return (ConsoleCursorPosition){1, 1};
    }
    if (column == CENTER_ALIGN)
        column = (conSize.columns - rows * (rowSize + 1) + 1) / 2 + 1;
    else if (column < 0)
        column = conSize.columns - columns * (columnSize + 1) + column + 1;
    bool consoleColorWasChanged = setColor(setup->color);
    // left side
    Console_setCursorPosition(row, column);
    fputs(setup->leftUpper, setup->stdFileStream);
    for (int i = 1; i < rows * (rowSize + 1); i++){
        Console_setCursorPosition(row + i, column);
        fputs((i % (rowSize + 1) != 0) ? setup->vertical : setup->leftCentral,
               setup->stdFileStream);
    }
    Console_setCursorPosition(row + rows * (rowSize + 1), column);
    fputs(setup->leftLower, setup->stdFileStream);
    // central lines
    for (int i = 1; i < columns; i++){
        Console_setCursorPosition(row, column + i * (columnSize + 1));
        fputs(setup->centralUpper, setup->stdFileStream);
        for (int j = 1; j < rows * (rowSize + 1); j++){
            Console_setCursorPosition(row + j, column + i * (columnSize + 1));
            fputs((j % (rowSize + 1) != 0) ? setup->vertical : setup->central,
                   setup->stdFileStream);
        }
        Console_setCursorPosition(row + rows * (rowSize + 1), column + i * (columnSize + 1));
        fputs(setup->centralLower, setup->stdFileStream);
    }
    // right side
    Console_setCursorPosition(row, column + columns * (columnSize + 1));
    fputs(setup->rightUpper, setup->stdFileStream);
    for (int i = 1; i < rows * (rowSize + 1); i++){
        Console_setCursorPosition(row + i, column + columns * (columnSize + 1));
        fputs((i % (rowSize + 1) != 0) ? setup->vertical : setup->rightCentral,
               setup->stdFileStream);
    }
    Console_setCursorPosition(row + rows * (rowSize + 1), column + columns * (columnSize + 1));
    fputs(setup->rightLower, setup->stdFileStream);
    // horizontal lines
    for (int i = 0; i <= rows * (rowSize + 1); i += rowSize + 1){
        for (int j = 1; j < columns * (columnSize + 1); j+= columnSize + 1){
            printHorizontal(setup->stdFileStream, setup->horizontal, row + i, column + j, columnSize);
        }
    }
    if (consoleColorWasChanged) Console_reset();
    return (ConsoleCursorPosition){
        .row = row + rows * (rowSize + 1),
        .column = column + columns * (columnSize + 1)
    };
}


ConsoleCursorPosition printRectangle(const ConsolePrintSetup * pattern, 
                                                         int   row    , 
                                                         int   column ,
                                                         int   height ,
                                                         int   width  ){
    //
    ConsoleSize conSize = Console_size();
    if (pattern == NULL          || pattern->stdFileStream == NULL || row > conSize.rows || 
        column > conSize.columns || height <= 0                    || width <= 0         || 
        column <= 0              || row <= 0                       || 
        height + row - 1 > conSize.rows || width + column - 1 > conSize.columns){
        //
        return Console_cursorPosition();
    }
    if (pattern->frame != NULL){
        printFrame(pattern->frame, row, column, height, width);
        row++;
        column++;
        height -= 2;
        width -= 2;
    }
    bool consoleColorWasChanged = setColor(pattern->color);
    for (int i = 0; i < height; i++){
        Console_setCursorPosition(row + i, column);
        for (int j = 0; j < width; j++){
            fputc(' ', pattern->stdFileStream);
        }
    }
    if (consoleColorWasChanged) Console_reset();
    return (ConsoleCursorPosition){
        .row = row + height - 1,
        .column = column + width - 1
    };    
}

int getStringMaxWidth(char * string){
    if (string == NULL) return FAIL;
    int maxWidth = 0;
    int curWidth = 0;
    for (int i = 0; string[i] != '\0'; i++){
        if (string[i] == '\n'){
            if (curWidth > maxWidth) 
                maxWidth = curWidth;
            curWidth = 0;
        }
        else if(!isUnicodeExtraSymbol(string[i])){
            curWidth++;
        }
    }
    return (curWidth > maxWidth) ? curWidth : maxWidth;
}

short userMenu(ConsoleCursorPosition * cursor     , 
                           MenuSetup * menuSetup  ,
                                 int   row        , 
                                 int   column     , 
                                 int   charsPerRow, 
                               short   optQuantity, 
                                    ...            ){
    //
    if (menuSetup == NULL || optQuantity <= 0 || menuSetup->textPattern == NULL ||
        menuSetup->selectorPattern == NULL){
        //
        return FAIL;
    }
    ConsolePrintSetup * textPattern     = menuSetup->textPattern;
    ConsolePrintSetup * selectorPattern = menuSetup->selectorPattern;
    va_list argPtr;
    va_start(argPtr, optQuantity);
    char * options[optQuantity];
    ConsoleCursorPosition optPos[optQuantity];
    optPos[0].row = row;
    if (charsPerRow == AUTO_SIZE){
        for (int i = 0; i < optQuantity; i++){
            if((options[i] = va_arg(argPtr, char *)) == NULL){
                va_end(argPtr);
                return FAIL;
            }
            int currentSize = getStringMaxWidth(options[i]);
            if (currentSize > charsPerRow)
                charsPerRow = currentSize;
        }
        if (textPattern->frame != NULL)
            charsPerRow += 2;
        if (column == CENTER_ALIGN){
            column = (menuSetup->orientation == VERTICAL_ORIENT) ? 
                    //
                    (Console_size().columns - charsPerRow) / 2 + 1 :
                    //
                    (Console_size().columns - optQuantity * 
                    (charsPerRow + ((textPattern->frame != NULL) ? 2 : 0) + 
                    menuSetup->optionsSpace - 2)) / 2 + 2;
        }
        optPos[0].column = column;
        for (int i = 0; i < optQuantity; i++){
            if (i + 1 < optQuantity){
                fflush(stdout);
                if (menuSetup->orientation == VERTICAL_ORIENT){
                    optPos[i + 1].row = printAt(textPattern, options[i], optPos[i].row, 
                                                optPos[i].column, charsPerRow).row +
                    menuSetup->optionsSpace;
                    optPos[i + 1].column = column;
                } else {
                    optPos[i + 1].column = printAt(textPattern, options[i], optPos[i].row, 
                                                   optPos[i].column, charsPerRow).column +
                    menuSetup->optionsSpace;
                    optPos[i + 1].row = row;
                }
            } else {
                printAt(textPattern, options[i], optPos[i].row, optPos[i].column, charsPerRow);
            }
        }
    } else {
        if (column == CENTER_ALIGN){
            if (menuSetup->orientation == VERTICAL_ORIENT){
                column = (menuSetup->orientation == VERTICAL_ORIENT) ?
                //
                (Console_size().columns - charsPerRow) / 2 + 1 :
                //
                (Console_size().columns - optQuantity * 
                (charsPerRow + ((textPattern->frame != NULL) ? 2 : 0) + 
                menuSetup->optionsSpace)) / 2 + 1;
            }
        }
        optPos[0].column = column;
        for (int i = 0; i < optQuantity; i++){
            if((options[i] = va_arg(argPtr, char *)) == NULL){
                va_end(argPtr);
                return FAIL;
            }
            if (i + 1 < optQuantity){
                fflush(stdout);
                if (menuSetup->orientation == VERTICAL_ORIENT){
                    optPos[i + 1].row = printAt(textPattern, options[i], optPos[i].row, 
                                                optPos[i].column, charsPerRow).row +
                    menuSetup->optionsSpace;
                    optPos[i + 1].column = column;
                } else {
                    optPos[i + 1].column = printAt(textPattern, options[i], optPos[i].row, 
                                                   optPos[i].column, charsPerRow).column +
                    menuSetup->optionsSpace;
                    optPos[i + 1].row = row;
                } 
            }
            else {
                printAt(textPattern, options[i], optPos[i].row, optPos[i].column, charsPerRow);
            }
        }
    }
    va_end(argPtr);
    if (cursor != NULL) *cursor = Console_cursorPosition();
    short selector = 0;
    Console_lockInput();
    Console_hideCursor();
    while(1){
        printAt(selectorPattern, options[selector], optPos[selector].row,
                optPos[selector].column, charsPerRow);
        short moveSelector = 0;
        while (!moveSelector){
            switch (Console_getChar()){
                case UPWARDS_H:
                case UPWARDS_L:{
                    if (menuSetup->orientation == VERTICAL_ORIENT && selector >= 1){
                        printAt(textPattern, options[selector], optPos[selector].row,
                            optPos[selector].column, charsPerRow);
                        selector--;
                        moveSelector = 1;
                    }
                    break;
                }
                case DOWNWARDS_H:
                case DOWNWARDS_L:{
                    if (menuSetup->orientation == VERTICAL_ORIENT && selector < optQuantity - 1){
                        printAt(textPattern, options[selector], optPos[selector].row,
                            optPos[selector].column, charsPerRow);
                        selector++;
                        moveSelector = 1;
                    }
                    break;
                }
                case LEFTWARDS_H:
                case LEFTWARDS_L:{
                    if (menuSetup->orientation == HORIZONTAL_ORIENT && selector >= 1){
                        printAt(textPattern, options[selector], optPos[selector].row,
                            optPos[selector].column, charsPerRow);
                        selector--;
                        moveSelector = 1;
                    }
                    break;
                }
                case RIGHTWARDS_H:
                case RIGHTWARDS_L:{
                    if (menuSetup->orientation == HORIZONTAL_ORIENT && selector < optQuantity - 1){
                        printAt(textPattern, options[selector], optPos[selector].row,
                            optPos[selector].column, charsPerRow);
                        selector++;
                        moveSelector = 1;
                    }
                    break;
                }    
                case ENTER:{
                    Console_showCursor();
                    Console_unlockInput();
                    return selector;
                }
            }  
        }
    }
}

bool isUnicodeExtraSymbol(char symbol){
    return ((((symbol >> 7) & 1) == 1) && (((symbol >> 6) & 1) == 0)) ? true : false;
}

ConsoleCursorPosition printHeader(HeaderColorScheme * colorScheme,
                                               char * string     ,
                                                int   row        ,
                                                int   column     ,
                                                int   charsPerRow){
    // verifying arguments
    ConsoleSize conSize = Console_size();
    if (colorScheme == NULL || colorScheme->stdFileStream == NULL || string == NULL          ||
        row <= 0            || column > conSize.columns           || charsPerRow < AUTO_SIZE || 
        charsPerRow + column - 1 > conSize.columns ){
    //
        return Console_cursorPosition();                           
    }
    //
    if (charsPerRow == AUTO_SIZE){
        charsPerRow = getStringMaxWidth(string);
        if (colorScheme->frame != NULL) charsPerRow += 2;
    }
    //
    if (column == CENTER_ALIGN)
            column = (conSize.columns - charsPerRow) / 2 + 1;
    else if (column < 0)
            column = conSize.columns - charsPerRow + column + 2;
    //
    int initialRow = row;
    if (colorScheme->frame != NULL){
        row++;
        column++;
        if (charsPerRow == AUTO_SIZE) charsPerRow -= 2;
    }
    //
    if (colorScheme->frame != NULL && charsPerRow <= 2)
        return (ConsoleCursorPosition){1, 1};          
    //
    Console_setCursorPosition(row, column);
    int col = 0;
    for (int i = 0; string[i] != '\0'; i++){
        if (string[i] == '\n' || col == charsPerRow){
            setColor(headerColorDeterminator(colorScheme, ' '));
            while (col != charsPerRow){
                fputc(' ', colorScheme->stdFileStream);
                col++;
            }
            Console_setCursorPosition(++row, column);
            if (string[i] != '\n') i--;
            col = 0;
        } else {
            if (i > 0 && string[i] != string[i - 1])
                setColor(headerColorDeterminator(colorScheme, string[i]));
            fputc(' ', colorScheme->stdFileStream);
            col++;
        }
    }
    if (col != charsPerRow){
        ColorSetup newColor = *headerColorDeterminator(colorScheme, ' ');
        newColor.fg = CURRENT_COLOR;
        setColor(&newColor);
        while (col != charsPerRow){
            fputc(' ', colorScheme->stdFileStream);
            col++;
        }
    }
    Console_reset();
    return (colorScheme->frame != NULL) ?
        //  
        printFrame(colorScheme->frame, initialRow, column - 1, row - initialRow + 2, charsPerRow + 2) :
        //
        (ConsoleCursorPosition){
        .row = row, 
        .column = column + col
        };
}


ColorSetup * headerColorDeterminator(HeaderColorScheme * headerScheme, char symbol){
    if (headerScheme == NULL) return CURRENT_COLORS;
    for (int i = 0; headerScheme->symbols[i] != '\0'; i++){
        if (headerScheme->symbols[i] == symbol)
            return headerScheme->colors[i];
    }
    return CURRENT_COLORS;
}

void exitProgram(Option colorTheme, const int exitCode, char * message, void(*func)(void*), const int argQuantity, ...){
    vanish(&COLOR_DEEP_BLACK);
    HeaderColorScheme * headerSetup = &HEADER_PIC_GREEN;
    ConsolePrintSetup * conPrintSetup = &CON_PRINT_MSG_GREEN;
    MenuSetup * menuSetup = &MENU_HORI_GREEN;
    if (colorTheme == RED_THEME){
        headerSetup = &HEADER_PIC_RED;
        conPrintSetup = &CON_PRINT_MSG_RED;
        menuSetup = &MENU_HORI_RED;
    }
    ConsoleCursorPosition cursor = printHeader(headerSetup,
        "                 000000                     \n"
        "              000      000                  \n"
        "             0            0                 \n"
        "           00              00               \n"
        "           0                00              \n"
        "          0                  0              \n"
        "         0                    0             \n"
        "         000000   000000       0            \n"
        "        0 000      0000        0            \n"
        "        0 000      0000        0            \n"
        "          000      0000         0           \n"
        "       0  000      000        000           \n"
        "       0                       00           \n"
        "       0            0000        0           \n"
        "       0          0000000       0           \n"
        "        0       0000000000       0          \n"
        "        0      000000000000     0           \n"
        "        0     0000000000  0     0           \n"
        "         0       0000000 0  0 00            \n"
        "         0                   00             \n"
        "          0                  0              \n"
        "           0                0               \n"
        "            0              00               \n"
        "  00         00           0000    00000000  \n"
        "  00000        00      000   00000000000000 \n"
        "    0000000        00       00000           \n"
        "       000000000        0000000000000       \n"
        "            00000000000000000   00  000000  \n"
        "                            0    0       00 \n"
    , 2, CENTER_ALIGN, AUTO_SIZE);
    cursor = printAt(conPrintSetup, message,
            cursor.row + 2, CENTER_ALIGN, AUTO_SIZE);
    if (userMenu(NULL, menuSetup, cursor.row + 2, CENTER_ALIGN, AUTO_SIZE, 2,
             "✔ CONFIRM ", "🗙  CANCEL ") == 0){
        if (func != NULL && argQuantity > 0){
            va_list argPtr;
            va_start (argPtr, argQuantity);
            for (int i = 0; i < argQuantity; i++){
                void * funcArg = va_arg(argPtr, void *);
                if ((func != free || funcArg != NULL) && 
                    (func != (void(*)())fclose || funcArg != NULL)){
                    //
                    func(funcArg);
                }
            }
            va_end (argPtr);
        }
    vanish(DEFAULT_COLORS);
    exit(exitCode);
    }
}