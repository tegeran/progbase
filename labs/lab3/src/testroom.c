#include "../include/testroom.h"

/*long my_strlen(char* str){
    long count=0;

    while(*str!=0){
        if(!((((*str >> 7)&1) ==1) && (((*str>>6)&1) == 0)))
            count++;
        
        str++;
    }

    return count;
}*/

void testFunc(void(*function)(void*)){
    if (function == (void(*)())newProviderDyn){
        // regular case
        Provider expectedProv = {
            .name = "Test",
            .tariff = 100500,
            .loss = 2,
            .hostOffer = 10,
            .cloud.capacity = 200,
            .cloud.tariff = 25
        };
        Provider * testProv = newProviderDyn("Test", 100500, 2, 10, 200, 25);
        assert(testProv != NULL); // verifying wether it is NULL pointer
        assert(compareProvider(testProv, &expectedProv));
        //
        strcpy(expectedProv.name, "");
        expectedProv.tariff = 80;
        expectedProv.loss = 12.156896;
        expectedProv.hostOffer = 1089;
        expectedProv.cloud.capacity = 205580.5;
        expectedProv.cloud.tariff = 25.5664;

        free(testProv);
        testProv = newProviderDyn("", 80, 12.156896, 1089, 205580.5, 25.5664);
        assert(testProv == NULL);
        //
        strcpy(expectedProv.name, "\n dich\0 something after \0");
        expectedProv.tariff = 0;
        expectedProv.loss = 58.96;
        expectedProv.hostOffer = 856;
        expectedProv.cloud.capacity = 0;
        expectedProv.cloud.tariff = 2588888.664;

        testProv = newProviderDyn("\n dich", 0, 58.96, 856, 0.0,  2588888.664);
        assert(testProv != NULL);
        assert(compareProvider(testProv, &expectedProv));
        //
        free(testProv);
        testProv = newProviderDyn(NULL, 0, 58.96, 856, 2.5,  2588888.664);
        assert(testProv == NULL);
        //
        testProv = newProviderDyn("Test", -1, 58.96, 856, 2.2,  2588888.664);
        assert(testProv == NULL);
        //
        testProv = newProviderDyn("Test", 1, -58.96, 856, 2.3,  2588888.664);
        assert(testProv == NULL);
        //
        testProv = newProviderDyn("Test", 1, 40000, 856, 2.1,  2588888.664);
        assert(testProv == NULL);
        //
        testProv = newProviderDyn("Test", 1, 58.96, -12, 2.5,  2588888.664);
        assert(testProv == NULL);
        //
        testProv = newProviderDyn("Test", 1, 58.96, 856, -89.5,  2588888.664);
        assert(testProv == NULL);
        //
        testProv = newProviderDyn("Test", 1, 58.96, 856, 0.0, -895);
        assert(testProv == NULL);
        //
        testProv = newProviderDyn(NULL, -1, -58.96,- 856, 89.12, -2588888.664);
        assert(testProv == NULL);
        //
        testProv = newProviderDyn("\n dich", 0, 58.96, 856, 0.6, 2588888.664);
        assert(testProv != NULL);
        free(testProv);
    }
    if (function == (void(*)())compareProvider){
        Provider prov1 = {
            .name = "Test",
            .tariff = 100500,
            .loss = 2,
            .hostOffer = 10,
            .cloud.capacity = 200,
            .cloud.tariff = 25
        };
        Provider prov2 = {
            .name = "Test",
            .tariff = 100500,
            .loss = 2,
            .hostOffer = 10,
            .cloud.capacity = 200,
            .cloud.tariff = 25
        };
        assert(compareProvider(&prov1, &prov2) == 1);
        //
        strcpy(prov1.name, "Different name");
        assert(compareProvider(&prov1, &prov2) == 0);
        //
        strcpy(prov2.name, "Different name");
        prov1.tariff = 100501;
        assert(compareProvider(&prov1, &prov2) == 0);
        //
        prov2.tariff = 100501;
        prov1.loss = 100;
        assert(compareProvider(&prov1, &prov2) == 0);
        //
        prov2.hostOffer = 15;
        assert(compareProvider(&prov1, &prov2) == 0);
        //
        prov2.loss = 100;
        assert(compareProvider(&prov1, &prov2) == 0);
        //
        prov1.hostOffer = 15;
        assert(compareProvider(&prov1, &prov2) == 1);
        //
        prov1.cloud.capacity = 125;
        assert(compareProvider(&prov1, &prov2) == 0);
        //
        prov2.cloud.capacity = 125;
        prov2.cloud.tariff = 0;
        assert(compareProvider(&prov1, &prov2) == 0);
        //
        prov1.cloud.tariff = 0;
        assert(compareProvider(&prov1, &prov2) == 1);
        //
        assert(compareProvider(&prov1, NULL) == FAIL);
        //
        assert(compareProvider(NULL, &prov2) == FAIL);
        //
        assert(compareProvider(NULL, NULL) == FAIL);
    }
    if (function == (void(*)())compareProviderList){
        ProviderList list1;
        ProviderList list2;
        assert(setPointersToNULL((void**)list1.list, PROV_LIST_CAPACITY) == SUCCESS);
        assert(setPointersToNULL((void**)list2.list, PROV_LIST_CAPACITY) == SUCCESS);
        list1.size = 0;
        list2.size = 0;
        //
        assert(compareProviderList(&list1, &list2) == 1);
        //
        list1.size = 1;
        //
        assert(compareProviderList(&list1, &list2) == 0);
        //
        list1.list[0] = newProviderDyn("Test0", 15, 9, 25, 25, 78);
        list2.list[0] = newProviderDyn("Test0", 15, 9, 25, 25, 78);
        //
        assert(compareProviderList(&list1, &list2) == 1);
        //
        list1.list[1] = newProviderDyn("Test1", 16, 10, 26, 26, 79);
        list2.list[1] = newProviderDyn("Test1", 16, 10, 26, 26, 79);
        list1.list[2] = newProviderDyn("Test2", 17, 11, 27, 28, 80);
        list2.list[2] = newProviderDyn("Test2", 17, 11, 27, 28, 80);
        list1.size = 3;
        list2.size = 3;
        //
        assert(compareProviderList(&list1, &list2) == 1);
        assert(compareProviderList(NULL, &list2) == FAIL);
        assert(compareProviderList(&list1, NULL) == FAIL);
        //
        updateProvider(list1.list[1], "name", "Different name");
        //
        assert(compareProviderList(&list1, &list2) == 0);
        //
        updateProvider(list2.list[1], "name", "Different name");
        assert(compareProviderList(&list1, &list2) == 1);
        updateProvider(list2.list[1], "cloud capacity", "125");
        //
        assert(compareProviderList(&list1, &list2) == 0);
        //
        freeProviderListDyn(&list1);
        freeProviderListDyn(&list2);
        list1.size = 0;
        list2.size = 0;
        //
        assert(compareProviderList(&list1, &list2) == 1);

    }
    if (function == (void(*)())doubleCmp){
        assert(doubleCmp(0.0, 0.0, ACCURACY));
        assert(!doubleCmp(20.12, 2.71828, ACCURACY));
        assert(doubleCmp(11.00, 11.00999999, ACCURACY));
        assert(doubleCmp(11.01, 11.00999999, ACCURACY));
        assert(!doubleCmp(11.01, 11.02, ACCURACY));
        assert(doubleCmp(3.1415, 3.1415, ACCURACY));
        assert(doubleCmp(-8.5968, -8.5968, ACCURACY));
        assert(!doubleCmp(-20.0, 20.0, ACCURACY));
        assert(doubleCmp(20.00, 19.999, ACCURACY));
        assert(!doubleCmp(-20.00001, 20.00002, ACCURACY));
        assert(!doubleCmp(-20.589, 20.589, ACCURACY));
        assert(doubleCmp( 900000000.25889,  900000000.25889, ACCURACY));
        assert(doubleCmp(-900000000.25851, -900000000.25851, ACCURACY));
        assert(!doubleCmp(900000000.25851, -900000000.25851, ACCURACY));
    }
    if (function == (void(*)())sortProviderList){
        assert(sortProviderList(NULL) == FAIL);
        //
        ProviderList providers;
        assert(setPointersToNULL((void**)providers.list, PROV_LIST_CAPACITY) == SUCCESS);
        //
        assert(sortProviderList(&providers) == SUCCESS);
        assert(providers.size == 0);
        for (int i = 0; i < PROV_LIST_CAPACITY; i++){
            assert(providers.list[i] == NULL);
        }
        //
        Provider toPointOn[4];
        providers.list[0] = &toPointOn[0];
        providers.list[2] = &toPointOn[1];
        providers.list[8] = &toPointOn[2];
        providers.list[9] = &toPointOn[3];
        //
        assert(sortProviderList(&providers) == SUCCESS);
        assert(providers.size == 4);
        for (int i = 0; i < 4; i++){
            assert(providers.list[i] == &toPointOn[i]);
        }
        for (int i = 4; i < PROV_LIST_CAPACITY; i++){
            assert(providers.list[i] == NULL);
        }
        // repeating again in order to test when the list is sorted
        assert(sortProviderList(&providers) == SUCCESS);
        assert(providers.size == 4);
        for (int i = 0; i < 4; i++){
            assert(providers.list[i] == &toPointOn[i]);
        }
        for (int i = 4; i < PROV_LIST_CAPACITY; i++){
            assert(providers.list[i] == NULL);
        }
        //
        assert(setPointersToNULL((void**)providers.list, PROV_LIST_CAPACITY) == SUCCESS);
        for (int i = PROV_LIST_CAPACITY - 1; i >= 7; i--){
            providers.list[i] = &toPointOn[-(i - PROV_LIST_CAPACITY + 1)];
        }
        assert(sortProviderList(&providers) == SUCCESS);
        assert(providers.size == 3);
        for (int i = 0; i < 3; i++){
            assert(providers.list[i] == &toPointOn[2 - i]);
        }
        for (int i = 3; i < PROV_LIST_CAPACITY; i++){
            assert(providers.list[i] == NULL);
        }
        //
        assert(setPointersToNULL((void**)providers.list, PROV_LIST_CAPACITY) == SUCCESS);
        providers.list[9] = &toPointOn[0];
        assert(sortProviderList(&providers) == SUCCESS);
        assert(providers.size == 1);
        assert(providers.list[0] = &toPointOn[0]);
        for(int i = 1; i < PROV_LIST_CAPACITY; i++){
            assert(providers.list[i] == NULL);
        }
        // provider.list is filled
        for (int i = 1; i < PROV_LIST_CAPACITY; i++){
            providers.list[i] = &toPointOn[i % 4];
        }
        assert(sortProviderList(&providers) == SUCCESS);
        assert(providers.size == PROV_LIST_CAPACITY);
        for (int i = 1; i < PROV_LIST_CAPACITY; i++){
            assert(providers.list[i] == &toPointOn[i % 4]);
        }
        // firts provider == NULL and other != NULL
        providers.list[0] = NULL;
        assert(sortProviderList(&providers) == SUCCESS);
        assert(providers.size == PROV_LIST_CAPACITY - 1);
        for (int i = 0; i < PROV_LIST_CAPACITY - 1; i++){
            assert(providers.list[i] == &toPointOn[(i + 1) % 4]);
        }
        assert(providers.list[PROV_LIST_CAPACITY - 1] == NULL);
        // NULL provider is in the middle of the list
        providers.list[PROV_LIST_CAPACITY - 1] = &toPointOn[2];
        providers.list[PROV_LIST_CAPACITY / 2] = NULL;
        assert(sortProviderList(&providers) == SUCCESS);
        assert(providers.size == PROV_LIST_CAPACITY - 1); 
        for (int i = 0; i < PROV_LIST_CAPACITY / 2; i++){
            assert(providers.list[i] == &toPointOn[(i + 1) % 4]);
        }
        for (int i = PROV_LIST_CAPACITY / 2; i < PROV_LIST_CAPACITY - 1; i++){
            assert(providers.list[i] == &toPointOn[(i + 2) % 4]);
        }
        assert(providers.list[PROV_LIST_CAPACITY - 1] == NULL);
    }
    if (function == (void(*)())setPointersToNULL){
        const short ARRSIZE = 10;
        char * charArray[ARRSIZE];
        char charToPointOn = 'U';
        for (int i = 0; i < ARRSIZE; i++){
            charArray[i] = &charToPointOn;
        }
        assert(setPointersToNULL((void**)charArray, ARRSIZE) == SUCCESS);
        for (int i = 0; i < ARRSIZE; i++){
            assert(charArray[i] == NULL);
        }
        //---------------------------------
        int * intArray[ARRSIZE];
        int intToPointOn = 125;
        for (int i = 0; i < ARRSIZE; i++){
            intArray[i] = &intToPointOn;
        }
        assert(setPointersToNULL((void**)intArray, ARRSIZE) == SUCCESS);
        for (int i = 0; i < ARRSIZE; i++){
            assert(intArray[i] == NULL);
        }
        //---------------------------------
        void * voidArray[ARRSIZE];
        for (int i = 0; i < ARRSIZE; i++){
            voidArray[i] = (void*)&intToPointOn;
        }
        assert(setPointersToNULL(voidArray, ARRSIZE) == SUCCESS);
        for (int i = 0; i < ARRSIZE; i++){
            assert(voidArray[i] == NULL);
        }
        assert(setPointersToNULL(NULL, 5) == false);
        assert(setPointersToNULL(voidArray, 0) == false);
    }
    if (function == (void(*)())addToProviderList){
        Provider toAdd;
        ProviderList providers;
        assert(setPointersToNULL((void**)providers.list, PROV_LIST_CAPACITY) == SUCCESS);
        //
        for (int i = 0; i < PROV_LIST_CAPACITY; i++){
            assert(providers.list[i] == NULL);
            assert(addToProviderList(&toAdd, &providers) == SUCCESS);
            assert(providers.size == i + 1);
            assert(providers.list[i] == &toAdd);
        }
        //
        assert(addToProviderList(&toAdd, &providers) == FAIL);
        assert(providers.size == PROV_LIST_CAPACITY);
        //
        providers.list[0] = NULL;
        providers.list[PROV_LIST_CAPACITY / 2] = NULL;
        providers.list[PROV_LIST_CAPACITY - 1] = NULL;
        assert(addToProviderList(&toAdd, &providers) == SUCCESS);
        assert(providers.size == PROV_LIST_CAPACITY - 2);
        //
        assert(addToProviderList(&toAdd, &providers) == SUCCESS);
        assert(providers.size == PROV_LIST_CAPACITY - 1);
        //
        assert(addToProviderList(&toAdd, &providers) == SUCCESS);
        assert(providers.size == PROV_LIST_CAPACITY);
        //
        assert(addToProviderList(&toAdd, &providers) == FAIL);
        assert(providers.size == PROV_LIST_CAPACITY);
        //
        for (int i = 0; i < PROV_LIST_CAPACITY; i++){
            assert(providers.list[i] == &toAdd);
        }
    }
    if (function == (void(*)())removeProviderFromListDyn){
        Provider prov;
        ProviderList providers;
        for (int i = 0; i < PROV_LIST_CAPACITY; i++){
            providers.list[i] = &prov;
        }
        providers.size = PROV_LIST_CAPACITY;
        Provider * removed = NULL;
        //
        assert(removeProviderFromListDyn(0, &providers, &removed) == SUCCESS);
        assert(providers.size == PROV_LIST_CAPACITY - 1);
        assert(removed == &prov);
        for(int i = 0; i < PROV_LIST_CAPACITY - 1; i++){
            assert(providers.list != NULL);
        }
        assert(providers.list[PROV_LIST_CAPACITY - 1] == NULL);
        //
        assert(removeProviderFromListDyn(-1, &providers, &removed) == FAIL);
        assert(removed == NULL);
        //
        assert(removeProviderFromListDyn(5, &providers, &removed) == SUCCESS);
        assert(providers.size == PROV_LIST_CAPACITY - 2);
        assert(removed == &prov);
        for(int i = 0; i < PROV_LIST_CAPACITY - 2; i++){
            assert(providers.list[i] != NULL);
        }
        assert(providers.list[PROV_LIST_CAPACITY - 2] == NULL);
        // removing NULL provider
        assert(removeProviderFromListDyn(PROV_LIST_CAPACITY - 1, &providers, &removed) == FAIL);
        assert(providers.size == PROV_LIST_CAPACITY - 2);
        assert(removed == NULL);
    }
    if (function == (void(*)())rewriteProvider){
        // regular case
        Provider expectedProv = {
            .name = "Test",
            .tariff = 100500,
            .loss = 2,
            .hostOffer = 10,
            .cloud.capacity = 200.2,
            .cloud.tariff = 25
        };
        Provider testProv = {
            .name = "Not test",
            .tariff = 1588,
            .loss = 52,
            .hostOffer = 135,
            .cloud.capacity = 20000.54,
            .cloud.tariff = 2598
        };
        //
        assert(rewriteProvider(&testProv, "Test", 100500, 2, 10, 200.2, 25) == SUCCESS);
        assert(compareProvider(&testProv, &expectedProv));
        //
        strcpy(expectedProv.name, " ");
        expectedProv.tariff = 80;
        expectedProv.loss = 12.156896;
        expectedProv.hostOffer = 1089;
        expectedProv.cloud.capacity = 205580.5;
        expectedProv.cloud.tariff = 25.5664;
        //
        assert(rewriteProvider(&testProv, " ", 80, 12.156896, 1089, 205580.5, 25.5664) == SUCCESS);
        assert(compareProvider(&testProv, &expectedProv));
        //
        strcpy(expectedProv.name, "\n dich\0 something after \0");
        expectedProv.tariff = 0;
        expectedProv.loss = 58.96;
        expectedProv.hostOffer = 856;
        expectedProv.cloud.capacity = 0.0;
        expectedProv.cloud.tariff = 2588888.664;
        //
        assert(rewriteProvider(&testProv, "\n dich", 0, 58.96, 856, 0.0,  2588888.664) == SUCCESS);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(rewriteProvider(&testProv, NULL, 0, 58.96, 856, 2,  2588888.664) == FAIL);
        assert(compareProvider(&testProv, &expectedProv)); // verifying wether input provider wasn't changed
        //
        assert(rewriteProvider(&testProv, "Test", -1, 58.96, 856, 2,  2588888.664) == FAIL);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(rewriteProvider(&testProv, "Test", 1, -58.96, 856, 2,  2588888.664) == FAIL);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(rewriteProvider(&testProv, "Test", 1, 40000, 856, 2,  2588888.664) == FAIL);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(rewriteProvider(&testProv, "Test", 1, 58.96, -12, 2,  2588888.664) == FAIL);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(rewriteProvider(&testProv, "Test", 1, 58.96, 856, -89,  2588888.664) == FAIL);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(rewriteProvider(&testProv, "Test", 1, 58.96, 856, 0,  -895) == FAIL);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(rewriteProvider(&testProv, NULL, -1, -58.96,- 856, 89,  -2588888.664) == FAIL);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(rewriteProvider(&testProv, "", 1, 58.96, 856, 0,  -895) == FAIL);
        assert(compareProvider(&testProv, &expectedProv));
    }
    if (function == (void(*)())updateProvider){
        // regular case
        Provider expectedProv = {
            .name = "Test",
            .tariff = 100500,
            .loss = 2,
            .hostOffer = 10,
            .cloud.capacity = 200.2,
            .cloud.tariff = 25
        };
        Provider testProv = {
            .name = " Not Test",
            .tariff = 100500,
            .loss = 2,
            .hostOffer = 10,
            .cloud.capacity = 200.2,
            .cloud.tariff = 25
        };
        //
        assert(updateProvider(&testProv, "name", "Test") == SUCCESS);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(updateProvider(&testProv, "name", "") == FAIL);
        assert(compareProvider(&testProv, &expectedProv));// verifying wether input provider wasn't changed
        //
        assert(updateProvider(&testProv, NULL, "Test") == FAIL);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(updateProvider(&testProv, "dich", "Test") == FAIL);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(updateProvider(&testProv, "", "Test") == FAIL);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(updateProvider(&testProv, "loss", NULL) == FAIL);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(updateProvider(&testProv, NULL, NULL) == FAIL);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(updateProvider(NULL, "name", "Test") == FAIL);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(updateProvider(&testProv, "tariff", "-2dich dich") == FAIL);
        assert(compareProvider(&testProv, &expectedProv)); 
        //
        assert(updateProvider(&testProv, "tariff", "not a number") == FAIL);
        assert(compareProvider(&testProv, &expectedProv)); 
        //
        expectedProv.tariff = 25.4;
        assert(updateProvider(&testProv, "tariff", "25.4") == SUCCESS);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(updateProvider(&testProv, "loss", "-25") == FAIL);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(updateProvider(&testProv, "loss", "not a number") == FAIL);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(updateProvider(&testProv, "loss", "100.05dich") == FAIL);
        assert(compareProvider(&testProv, &expectedProv));
        //
        expectedProv.loss = 15;
        assert(updateProvider(&testProv, "loss", "15") == SUCCESS);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(updateProvider(&testProv, "host offer", "not a number") == FAIL);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(updateProvider(&testProv, "host offer", "-2") == FAIL);
        assert(compareProvider(&testProv, &expectedProv));
        //
        expectedProv.hostOffer = 15;
        assert(updateProvider(&testProv, "host offer", "15L") == SUCCESS);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(updateProvider(&testProv, "cloud capacity", "-8something useless") == FAIL);
        assert(compareProvider(&testProv, &expectedProv));
        //
        expectedProv.cloud.capacity = 0;
        assert(updateProvider(&testProv, "cloud capacity", "0something useless") == SUCCESS);
        assert(compareProvider(&testProv, &expectedProv));
        //
        expectedProv.cloud.capacity = 15;
        assert(updateProvider(&testProv, "cloud capacity", "15.000001something useless") == SUCCESS);
        assert(compareProvider(&testProv, &expectedProv));
        //
        assert(updateProvider(&testProv, "cloud tariff", "-2dich dich") == FAIL);
        assert(compareProvider(&testProv, &expectedProv)); 
        //
        assert(updateProvider(&testProv, "cloud tariff", "not a number") == FAIL);
        assert(compareProvider(&testProv, &expectedProv)); 
        //
        expectedProv.cloud.tariff = 25.4;
        assert(updateProvider(&testProv, "cloud tariff", "25.4") == SUCCESS);
        assert(compareProvider(&testProv, &expectedProv));
    }
    if (function == (void(*)())highestTariffProvidersDyn){
        ProviderList result;
        ProviderList source;
        source.list[0] = newProviderDyn("Test0", 5, 9, 25, 25, 78);
        source.list[1] = newProviderDyn("Test1", 10, 10, 26, 26, 79);
        source.list[2] = newProviderDyn("Test2", 15, 11, 27, 28, 80);
        source.size = 3;

        ProviderList expected;
        expected.list[0] = newProviderDyn("Test2", 15, 11, 27, 28, 80);
        expected.list[1] = newProviderDyn("Test1", 10, 10, 26, 26, 79);
        expected.size = 2;
        //
        assert(highestTariffProvidersDyn(&source, &result, 2) == 1);
        if(compareProviderList(&result, &expected) == 0){
            assert(freeProviderListDyn(&result) == SUCCESS);
            assert(freeProviderListDyn(&source) == SUCCESS);
            assert(freeProviderListDyn(&expected) == SUCCESS);
            assert(0);
        }
        assert(freeProviderListDyn(&result) == SUCCESS);
        //
        assert(rewriteProvider(source.list[0], "Test0", 15.5, 9, 25, 25, 78) == SUCCESS);
        assert(rewriteProvider(source.list[1], "Test1", 15.5, 10, 26, 26, 79) == SUCCESS);
        assert(rewriteProvider(source.list[2], "Test2", 15.5, 11, 27, 28, 80) == SUCCESS);
        //
        assert(rewriteProvider(expected.list[0], "Test0", 15.5, 9, 25, 25, 78) == SUCCESS);
        assert(rewriteProvider(expected.list[1], "Test1", 15.5, 10, 26, 26, 79) == SUCCESS);
        //
        assert(highestTariffProvidersDyn(&source, &result, 2) == 1);
        if(compareProviderList(&result, &expected) == 0){
            assert(freeProviderListDyn(&result) == SUCCESS);
            assert(freeProviderListDyn(&source) == SUCCESS);
            assert(freeProviderListDyn(&expected) == SUCCESS);
            assert(0);
        }
        assert(freeProviderListDyn(&result) == SUCCESS);
        //
        assert(updateProvider(source.list[0], "tariff", "12.7") == SUCCESS);
        assert(updateProvider(source.list[1], "tariff", "4.2") == SUCCESS);
        assert(updateProvider(source.list[2], "tariff", "8.3") == SUCCESS);
        //
        assert(updateProvider(expected.list[0], "tariff","12.7") == SUCCESS);
        assert(rewriteProvider(expected.list[1], "Test2", 8.3, 11, 27, 28, 80) == SUCCESS);
        //
        assert(highestTariffProvidersDyn(&source, &result, 2) == 1);
        if(compareProviderList(&result, &expected) == 0){
            assert(freeProviderListDyn(&result) == SUCCESS);
            assert(freeProviderListDyn(&source) == SUCCESS);
            assert(freeProviderListDyn(&expected) == SUCCESS);
            assert(0);
        }
        //
        assert(highestTariffProvidersDyn(NULL, &result, 2) == FAIL);
        assert(compareProviderList(&result, &expected) == 1);
        //
        assert(highestTariffProvidersDyn(&source, NULL, 2) == FAIL);
        assert(highestTariffProvidersDyn(NULL, NULL, 2) == FAIL);
        assert(highestTariffProvidersDyn(&source, NULL, 2) == FAIL);
        //
        assert(highestTariffProvidersDyn(&source, &result, 0) == FAIL);
        assert(compareProviderList(&result, &expected) == 1);
        //
        assert(highestTariffProvidersDyn(&source, &result, -2) == FAIL);
        assert(compareProviderList(&result, &expected) == 1);
        //
        assert(freeProviderListDyn(&result) == SUCCESS);
        assert(freeProviderListDyn(&source) == SUCCESS);
        assert(freeProviderListDyn(&expected) == SUCCESS);
        //
        for (int i = 0; i < PROV_LIST_CAPACITY; i++){
            assert((source.list[i] = newProviderDyn("Test0", i, 9, 25, 25, 78)) != NULL);
            assert((expected.list[i] = newProviderDyn("Test0", PROV_LIST_CAPACITY - 1 - i, 9, 25, 25, 78)) != NULL);
        }
        source.size = PROV_LIST_CAPACITY;
        expected.size = PROV_LIST_CAPACITY;
        //
        assert(highestTariffProvidersDyn(&source, &result, PROV_LIST_CAPACITY) == 1);
        if(compareProviderList(&result, &expected) == 0){
            assert(freeProviderListDyn(&result) == SUCCESS);
            assert(freeProviderListDyn(&source) == SUCCESS);
            assert(freeProviderListDyn(&expected) == SUCCESS);
            assert(0);
        }
        //
        assert(freeProviderListDyn(&result) == SUCCESS);
        assert(freeProviderListDyn(&source) == SUCCESS);
        assert(freeProviderListDyn(&expected) == SUCCESS);
    }
    if (function == (void(*)())copyProviderListDyn){
        ProviderList dest;
        ProviderList source;
        assert(setPointersToNULL((void**)source.list, PROV_LIST_CAPACITY) == SUCCESS);
        source.list[0] = newProviderDyn("Test0", 5, 9, 25, 25, 78);
        source.list[1] = newProviderDyn("Test1", 10, 10, 26, 26, 79);
        source.list[2] = newProviderDyn("Test2", 15, 11, 27, 28, 80);
        source.size = 3;
        //
        if (copyProviderListDyn(&source, &dest) == FAIL){
            assert(freeProviderListDyn(&source) == SUCCESS);
            assert(0);
        }
        if (compareProviderList(&source, &dest) == 0){
            assert(freeProviderListDyn(&source) == SUCCESS);
            assert(freeProviderListDyn(&dest) == SUCCESS);
            assert(0);
        }
        assert(freeProviderListDyn(&dest) == SUCCESS);
        //
        assert(freeProviderListDyn(&source) == SUCCESS);
        source.list[0] = newProviderDyn("Test3", 54, 52, 255, 252, 781);
        source.size = 1;
        //
        if (copyProviderListDyn(&source, &dest) == FAIL){
            assert(freeProviderListDyn(&source) == SUCCESS);
            assert(0);
        }
        if (compareProviderList(&source, &dest) == 0){
            assert(freeProviderListDyn(&source) == SUCCESS);
            assert(freeProviderListDyn(&dest) == SUCCESS);
            assert(0);
        }
        assert(copyProviderListDyn(&source, NULL) == FAIL);
        assert(copyProviderListDyn(NULL, &dest) == FAIL);
        //
        assert(freeProviderListDyn(&source) == SUCCESS);
        assert(freeProviderListDyn(&dest) == SUCCESS);
        //
        assert(copyProviderListDyn(&source, &dest) == SUCCESS);
        assert(compareProviderList(&source, &dest) == 1);
    }
    if (function == (void(*)())dataCloudToString){
        const short STRSIZE = 15;
        char str[STRSIZE];
        assert(dataCloudToString(&(DataCloud){12.5, 4.0}, str, STRSIZE) == 11);
        assert(!strcmp(str, "12.50@4.00\n"));
        //
        assert(dataCloudToString(&(DataCloud){0.529, 8}, str, STRSIZE) == 10);
        assert(!strcmp(str, "0.53@8.00\n"));
        //
        assert(dataCloudToString(&(DataCloud){125.12, 454.75}, str, STRSIZE) == 14);
        assert(!strcmp(str, "125.12@454.75\n"));
        //
        assert(dataCloudToString(&(DataCloud){125.12, 4545.75}, str, STRSIZE) == FAIL); // cloud needs 16 characters
        assert(!strcmp(str, ""));
        //
        assert(dataCloudToString(&(DataCloud){1282311.12, 4542115.75}, str, STRSIZE) == FAIL);
        assert(!strcmp(str, ""));
        //
        assert(dataCloudToString(NULL, str, STRSIZE) == FAIL);
        assert(!strcmp(str, ""));
        //
        assert(dataCloudToString(&(DataCloud){1282311.12, 4542115.75}, NULL, STRSIZE) == FAIL);
        assert(!strcmp(str, ""));
        //
        assert(dataCloudToString(&(DataCloud){1282311.12, 4542115.75}, str, 0) == FAIL);
        assert(!strcmp(str, ""));
        //
        assert(dataCloudToString(&(DataCloud){1282311.12, 4542115.75}, str, -2) == FAIL);
        assert(!strcmp(str, ""));
    }
    if (function == (void(*)())providerToString){
        const short STRSIZE = 35;
        char str[STRSIZE];
        assert(providerToString(&(Provider){"Name", 125.5, 12.6, 15, 
               ((DataCloud){12.8, 21.58})}, str, STRSIZE) == 33);
        assert(!strcmp("Name:125.50#12.60#15$12.80@21.58\n", str));
        //
        assert(providerToString(&(Provider){"Name", 215.586, 0.523, 15, 
               ((DataCloud){12.8, 21.58})}, str, STRSIZE) == 32);
        assert(!strcmp("Name:215.59#0.52#15$12.80@21.58\n", str));
        //
        assert(providerToString(&(Provider){"AtEdge", 25.5, 13.6, 15, 
               ((DataCloud){12.8, 21.58})}, str, STRSIZE) == 34);
        assert(!strcmp("AtEdge:25.50#13.60#15$12.80@21.58\n", str));
        //
        assert(providerToString(&(Provider){"EXESSIV", 25.5, 12.6, 15, // provider needs 36 characters
               ((DataCloud){12.8, 21.58})}, str, STRSIZE) == FAIL);
        assert(!strcmp("", str));
        //
        assert(providerToString(&(Provider){"STRSIZE is not enough", 25.5, 123.6, 15, 
               ((DataCloud){12.8, 21.58})}, str, STRSIZE) == FAIL);
        assert(!strcmp("", str));
        //
        assert(providerToString(&(Provider){"", 25.5, 123.6, 15, 
               ((DataCloud){12.8, 21.58})},str, STRSIZE) == 29);
        assert(!strcmp(":25.50#123.60#15$12.80@21.58\n", str));
        //
        assert(providerToString(NULL, str, STRSIZE) == FAIL);
        assert(!strcmp("", str));
        //
        assert(providerToString(&(Provider){"STRSIZE is not enough", 25.5, 123.6, 15, 
               ((DataCloud){12.8, 21.58})}, NULL, STRSIZE) == FAIL);
        assert(!strcmp("", str));
        //
        assert(providerToString(&(Provider){"STRSIZE is not enough", 25.5, 123.6, 15, 
               ((DataCloud){12.8, 21.58})},str, 0) == FAIL);
        assert(!strcmp("", str));
        //
        assert(providerToString(&(Provider){"STRSIZE is not enough", 25.5, 123.6, 15, 
               ((DataCloud){12.8, 21.58})},str, -2) == FAIL);
        assert(!strcmp("", str));
    }
    if (function == (void(*)())compareDataCloud){
        assert(compareDataCloud(&(DataCloud){123.58, 1538.8}, &(DataCloud){123.58, 1538.8}) == 1);
        //
        assert(compareDataCloud(&(DataCloud){0.0, 0.0}, &(DataCloud){0.0, 0.0}) == 1);
        //
        assert(compareDataCloud(&(DataCloud){12.4, 1225}, &(DataCloud){12.4000001, 1225.0000001}) == 1);
        //
        assert(compareDataCloud(&(DataCloud){123.53, 1538.8}, &(DataCloud){123.58, 1538.8}) == 0);
        //
        assert(compareDataCloud(&(DataCloud){122, 12.8}, &(DataCloud){0, 1538.8}) == 0);
        //
        assert(compareDataCloud(NULL, &(DataCloud){123.58, 1538.8}) == FAIL);
        //
        assert(compareDataCloud(&(DataCloud){123.58, 1538.8}, NULL) == FAIL);
        //
        assert(compareDataCloud(NULL, NULL) == FAIL);
    }
    if (function == (void(*)())stringToDataCloud){
        DataCloud actual;
        assert(stringToDataCloud(&actual, "12.6@125.8\n") == 11);
        assert(compareDataCloud(&(DataCloud){12.6, 125.8}, &actual) == 1);
        //
        assert(stringToDataCloud(&actual, "12.6322@125.3812\n") == 17);
        assert(compareDataCloud(&(DataCloud){12.63, 125.38}, &actual) == 1);
        //
        assert(stringToDataCloud(&actual, "0.001@0.0999\n some thing else") == 13);
        assert(compareDataCloud(&(DataCloud){0, 0.09}, &actual) == 1);
        //
        assert(stringToDataCloud(&actual, "0.001@0.0999") == FAIL);
        assert(stringToDataCloud(&actual, "12.6322@\n") == FAIL);
        assert(stringToDataCloud(&actual, "-23.2@458.5\n") == FAIL);
        assert(stringToDataCloud(&actual, "123.62@-85\n") == FAIL);
        assert(stringToDataCloud(&actual, "-89@-85\n") == FAIL);
        assert(stringToDataCloud(&actual, "12.6322_152.5\n") == FAIL);
        assert(stringToDataCloud(&actual, "12.6322\n") == FAIL);
        assert(stringToDataCloud(&actual, "@98.2\n") == FAIL);
        assert(stringToDataCloud(&actual, "123.6322@\n") == FAIL);
        assert(stringToDataCloud(&actual, "") == FAIL);
        assert(stringToDataCloud(&actual, "NaN@12.2\n") == FAIL);
        assert(stringToDataCloud(&actual, "NaN\n") == FAIL);
        assert(stringToDataCloud(&actual, "23.65@NaN\n") == FAIL);
        assert(stringToDataCloud(&actual, "sometext34@556.8\n") == FAIL);
        assert(stringToDataCloud(&actual, NULL) == FAIL);
        assert(stringToDataCloud(NULL, "12.6322@") == FAIL);
        assert(stringToDataCloud(NULL, NULL) == FAIL);
    }
    if (function == (void(*)())stringToProvider){
        Provider actual;
        assert(stringToProvider(&actual, "RadioSystems:12.60#58.40#12$12.50@17.80\n") == 40);
        assert(compareProvider(&(Provider){"RadioSystems", 12.6, 58.4, 12,
               (DataCloud){12.5, 17.8}}, &actual) == 1);
        //
        assert(stringToProvider(&actual, "R:12.60#58.40#12$12.50@17.80\n") == 29);
        assert(compareProvider(&(Provider){"R", 12.6, 58.4, 12,
               (DataCloud){12.5, 17.8}}, &actual) == 1);
        //
        assert(stringToProvider(&actual, " :12.6#58.40#12$12.50@17.80\n") == 28);
        assert(compareProvider(&(Provider){" ", 12.6, 58.4, 12,
               (DataCloud){12.5, 17.8}}, &actual) == 1);
        //
        assert(stringToProvider(&actual, "R:12.60#58.40#12$12.50@17.80") == FAIL);
        assert(stringToProvider(&actual, " :12.6#58.40#12$12.50@17.80SomeUselessText\n") == FAIL);
        assert(stringToProvider(&actual, ":12.60#5.5#16$14.52@18\n") == FAIL);
        assert(stringToProvider(&actual, "Provider:\n") == FAIL);
        assert(stringToProvider(&actual, "R:12.60#5.5#1614.52@18\n") == FAIL);
        assert(stringToProvider(&actual, "R:12.60#5.516$14$52@18\n") == FAIL);
        assert(stringToProvider(&actual, "R:12.605.5#16$14.52@18\n") == FAIL);
        assert(stringToProvider(&actual, "R:12.60$5.5#16$14.52@18\n") == FAIL);
        assert(stringToProvider(&actual, "R:12.60#5.5@16$14.52@18\n") == FAIL);
        assert(stringToProvider(&actual, "R&12.60#5.5#16$14.52@18\n") == FAIL);
        assert(stringToProvider(&actual, "R:12.60#5|5#16$14.52@18\n") == FAIL);
        assert(stringToProvider(&actual, "#$#@%#$#GU*BHGJOPIFMF()D()#)\n") == FAIL);
        assert(stringToProvider(&actual, "R:#5.5#16$14.52@18\n") == FAIL);
        assert(stringToProvider(&actual, "R:12.60##16$14.52@18\n") == FAIL);
        assert(stringToProvider(&actual, "R:12.60#5.5#16extratext$14.52@18\n") == FAIL);
        assert(stringToProvider(&actual, "R:12.60#5.5#16$extratext14.52@18\n") == FAIL);
        assert(stringToProvider(&actual, "R:-12.60#5.5#16$14.52@18\n") == FAIL);
        assert(stringToProvider(&actual, "R:12.60#-5.5#16$14.52@18\n") == FAIL);
        assert(stringToProvider(&actual, "R:12.60#155#16$14.52@18\n") == FAIL);
        assert(stringToProvider(&actual, "R:NaN#5.5#16$14.52@18\n") == FAIL);
        assert(stringToProvider(&actual, "R:12#Nan#16$14.52@18\n") == FAIL);
        assert(stringToProvider(&actual, "R:12#5.5#NaN$14.52@18\n") == FAIL);
        assert(stringToProvider(&actual, "R:12.60#58.40#-12$12.50@17.80\n") == FAIL);
        assert(stringToProvider(&actual, "R:12.60#58.40#12$-12.50@17.80\n") == FAIL);
        assert(stringToProvider(&actual, "R:12.60#58.40#12$12.50@-17.80\n") == FAIL);
        assert(stringToProvider(&actual, "R:-12.60#-58.40#-12$-12.50@-17.80\n") == FAIL);
        assert(stringToProvider(&actual, NULL) == FAIL);
        assert(stringToProvider(NULL, "R:12.60#58.40#12$12.50@17.80\n") == FAIL);
        assert(stringToProvider(NULL, NULL) == FAIL);
    }
    if (function == (void(*)())providerListToString){
        ProviderList list;
        assert(setPointersToNULL((void**)list.list, PROV_LIST_CAPACITY) == SUCCESS);
        const short STRSIZE = 105;
        char str[STRSIZE];
        list.list[0] = newProviderDyn("Test1", 16, 10, 26, 28, 79);
        list.list[1] = newProviderDyn("Test2", 17, 11, 27, 29, 80);
        list.list[2] = newProviderDyn("Test3", 18, 12, 28, 30, 81);
        list.size = 3;
        assert(providerListToString(&list, str, STRSIZE) == 99);
        assert(!strcmp(str, "Test1:16.00#10.00#26$28.00@79.00\n"
                            "Test2:17.00#11.00#27$29.00@80.00\n"
                            "Test3:18.00#12.00#28$30.00@81.00\n"));
        //
        assert(rewriteProvider(list.list[0], "Test1", 16, 10, 26, 28, 79) == SUCCESS);
        list.size = 1;
        assert(providerListToString(&list, str, STRSIZE) == 33);
        assert(!strcmp(str, "Test1:16.00#10.00#26$28.00@79.00\n"));
        //
        assert(rewriteProvider(list.list[0], "Test1", 16, 10, 26, 28, 79) == SUCCESS);
        list.size = 1;
        assert(providerListToString(&list, str, STRSIZE) == 33);
        assert(!strcmp(str, "Test1:16.00#10.00#26$28.00@79.00\n"));
        //
        assert(rewriteProvider(list.list[0], "exessive", 16, 10, 26, 28, 79) == SUCCESS);
        assert(rewriteProvider(list.list[1], "list", 17, 11, 27, 29, 80) == SUCCESS);
        assert(rewriteProvider(list.list[2], "forString", 18, 12, 28, 30, 81) == SUCCESS);
        list.size = 3;
        assert(providerListToString(&list, str, STRSIZE) == FAIL); // list needs 105 characters
        assert(!strcmp("", str));
        //
        assert(providerListToString(NULL, str, STRSIZE) == FAIL);
        assert(!strcmp("", str));
        //
        assert(providerListToString(&list, NULL, STRSIZE) == FAIL);
        assert(!strcmp("", str));
        //
        assert(providerListToString(&list, str, 0) == FAIL);
        assert(!strcmp("", str));
        //
        assert(providerListToString(&list, str, -8) == FAIL);
        assert(!strcmp("", str));
        //
        assert(providerListToString(NULL, NULL, STRSIZE) == FAIL);
        assert(!strcmp("", str));
        //
        assert(providerListToString(&(ProviderList){.size = 0}, str, STRSIZE) == 0);
        assert(!strcmp("", str));
        //
        freeProviderListDyn(&list);
    }
    if (function == (void(*)())stringToProviderList){
        ProviderList actual;
        assert(mallocProviderList(&actual, PROV_LIST_CAPACITY) == SUCCESS);
        ProviderList expected;
        expected.list[0] = newProviderDyn("Test1", 16, 10, 26, 28, 79);
        expected.list[1] = newProviderDyn("Test2", 17, 11, 27, 29, 80);
        expected.list[2] = newProviderDyn("Test3", 18, 12, 28, 30, 81);
        expected.size = 3;
        assert(stringToProviderList(&actual, "Test1:16.00#10.00#26$28@79\n"
                                             "Test2:17.00#11.00#27$29@80\n"
                                             "Test3:18.00#12.00#28$30@81\n") == SUCCESS);
        assert(compareProviderList(&expected, &actual) == 1);
        //
        assert(rewriteProvider(expected.list[0], " ", 16, 10, 26, 28, 79) == SUCCESS);
        assert(rewriteProvider(expected.list[1], " ", 17, 11, 27, 29, 80) == SUCCESS);
        assert(rewriteProvider(expected.list[2], " ", 18, 12, 28, 30, 81) == SUCCESS);;
        expected.size = 3;
        assert(stringToProviderList(&actual, " :16.00#10.00#26$28@79      \n"
                                             " :17.00#11.00#27$29@80   \n"
                                             " :18.00#12.00#28$30@81\n") == SUCCESS);
        assert(compareProviderList(&expected, &actual) == 1);
        //
        assert(stringToProviderList(&actual, "RADIOSYSTEMS:16.0010.00#26$28@79\n"
                                             "RADIOSYSTEMS2:16.00#10.00#26$28@79\n"   ) == FAIL);
        //
        assert(stringToProviderList(&actual, "RADIOSYSTEMS:16.00#10.00#2628@79\n"
                                             "RADIOSYSTEMS2:16.00#10.00#26$28@79\n"   ) == FAIL);
        //
        assert(stringToProviderList(&actual, "RADIOSYSTEMS:16.00#10.00#26$28@79"
                                             "RADIOSYSTEMS2:16.00#10.00#26$28@79\n"   ) == FAIL); 
        //
        assert(stringToProviderList(&actual, "RADIOSYSTEMS:16.00#10.00#26$28@79\n"
                                             "RADIOSYSTEMS2:16.00#10.00#26$28@79"   ) == FAIL);    
        //
        assert(stringToProviderList(&actual, ":16.00#10.00#26$28@79\n"
                                             "RADIOSYSTEMS2:16.00#10.00#26$28@79\n"   ) == FAIL);   
        //
        assert(stringToProviderList(&actual, "RADIOSYSTEMS:16.00#10.00#26$28@79\n"
                                             "RADIOSYSTEMS216.00#10.00#26$28@79\n"   ) == FAIL);             
        //
        assert(stringToProviderList(&actual, "RADIOSYSTEMS:16.00#10.00#26$28@79W\n"
                                             "RADIOSYSTEMS2:16.00#10.00#26$28@79\n"   ) == FAIL);
        //
        assert(stringToProviderList(&actual, "RADIOSYSTEMS:16.00#10.00#26$28@79\n"
                                             "RADIOSYSTEMS2:"   ) == FAIL);              
        //
        assert(stringToProviderList(&actual, "RADIOSYSTEMS:16.00#10.00#26$28@79\n"
                                             "RADIOSYSTEMS2:16.00#10.00#26$28@79\n something useless"   ) == FAIL);
        //
        assert(stringToProviderList(&actual, "RADIOSYSTEMS:NaN#10.00#26$28@79\n"
                                             "RADIOSYSTEMS2:16.00#10.00#26$28@79\n"   ) == FAIL);  
        //
        assert(stringToProviderList(NULL, "RADIOSYSTEMS:16.00#10.00#26$28@79\n"
                                             "RADIOSYSTEMS2:16.00#10.00#26$28@79\n"   ) == FAIL);
        //
        assert(stringToProviderList(&actual, NULL) == FAIL);      
        assert(stringToProviderList(NULL, NULL) == FAIL); 
        freeProviderListDyn(&expected);
        freeProviderListDyn(&actual);
    }
    if (function == (void(*)())getStringMaxWidth){
        assert(getStringMaxWidth("123\n123\n12\n1234\n") == 4);
        assert(getStringMaxWidth("1234567\n123\n12\n1234\n") == 7);
        assert(getStringMaxWidth("\n123\n12\n1234\n") == 4);
        assert(getStringMaxWidth("123\n\n\n12\n") == 3);
        assert(getStringMaxWidth("123\n12345") == 5);
        assert(getStringMaxWidth("123456") == 6);
        assert(getStringMaxWidth("") == 0);
        assert(getStringMaxWidth("1") == 1);
        assert(getStringMaxWidth(NULL) == FAIL);
        assert(getStringMaxWidth("🖄") == 1);
        assert(getStringMaxWidth("⛰dich") == 5);
        assert(getStringMaxWidth("dich⛰") == 5);
        assert(getStringMaxWidth("dich⛰\n⛰⛰⛰⛰⛰⛰") == 6);
        assert(getStringMaxWidth("d⛰⛰ich⛰\n⛰⛰⛰⛰⛰⛰\n") == 7);
    }
}