
// Copyright © 2017 Vitaly Kryvenko, all rights reserved.
// USER FRIENDLY INTERFACE FUNCTIONS

#pragma once

#include <pthread.h>
#include <stdio.h>
#include <progbase/console.h>
#include <progbase.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include "provider.h"
#include "misc.h"


#define USER_MENU(cursor, menuSetup, row, column, charsPerRow, ...) \
userMenu((cursor), (menuSetup), (row), (column), (charsPerRow), totalArgs(#__VA_ARGS__), __VA_ARGS__)

#define PROV_SPREADSH_NAME_WIDTH 25

#define PROV_SPREADSH_NUMERATION_WITDH 2

#define PROV_SPREADSH_VALUES_WIDTH 13

#define PROV_SPREADSH_ROW_HEIGHT (PROVIDER_NAME_SIZE / PROV_SPREADSH_NAME_WIDTH)

#define TOTAL_ATTRIBUTES 8

#define UNICODE_CHAR_SIZE sizeof("╣")

enum __Option {
    // color shortcuts
    DEFAULT_COLOR = -2,
    CURRENT_COLOR,
    // (printAt()) shortcuts
    RIGHT_ALIGN = -1,
    CENTER_ALIGN = 0,   
    AUTO_SIZE = 0,  // defines charsPerRow in (printAt()) as the max string's width on the screen
    LEFT_ALIGN = 1, 
    // menu setup shortcuts 
    VERTICAL_ORIENT,
    HORIZONTAL_ORIENT,
    // (exitProgram()) themes
    GREEN_THEME,
    RED_THEME,       
    // 
    TERMINAL_SYMBOL = '\n',// symbol to finish reading user's input in (readString())
    // keyboard buttons definitions
    UPWARDS_L = 'w',
    DOWNWARDS_L = 's',
    ENTER = '\n',
    LEFTWARDS_L = 'a',
    RIGHTWARDS_L = 'd',
    UPWARDS_H = 'W',
    DOWNWARDS_H  = 'S',
    LEFTWARDS_H  = 'A',
    RIGHTWARDS_H  = 'D'
};

typedef enum __Option Option;

typedef struct __ConsolePrintSetup ConsolePrintSetup;
typedef struct __FramePrintSetup FramePrintSetup;
typedef struct __ColorSetup ColorSetup;
typedef struct __HeaderColorScheme HeaderColorScheme;
typedef struct __MenuSetup MenuSetup;
typedef struct __SpreadsheetSetup SpreadsheetSetup;


// color chanel intensity must ∈ [0,5] range
struct __ColorSetup{
    short fg;  // foreground color
    short bg;  // backgroud color
};

#define DEFAULT_COLORS (void*)2
#define CURRENT_COLORS NULL

struct __MenuSetup{
    struct __ConsolePrintSetup * textPattern;
    struct __ConsolePrintSetup * selectorPattern;
    short orientation;
    short optionsSpace;
};


struct __HeaderColorScheme{
    FILE * stdFileStream;
    char * symbols; // a string with symbols, each i-th index corresponds to colors[i] pattern
    struct __ColorSetup ** colors;
    struct __FramePrintSetup * frame;
};

struct __FramePrintSetup {
    FILE * stdFileStream; // stream frame should be printed to
    //
    struct __ColorSetup * color; 
    //         
    char horizontal [UNICODE_CHAR_SIZE];
    char vertical   [UNICODE_CHAR_SIZE];
    //
    char leftUpper  [UNICODE_CHAR_SIZE];
    char rightUpper [UNICODE_CHAR_SIZE];
    //
    char leftLower  [UNICODE_CHAR_SIZE];
    char rightLower [UNICODE_CHAR_SIZE];
};

struct __ConsolePrintSetup{
    FILE * stdFileStream;          // stream string should be printed to
    struct __ColorSetup * color;   // background color
    struct __FramePrintSetup * frame;
};

struct __SpreadsheetSetup{
    FILE * stdFileStream;
    struct __ColorSetup * color;
    char horizontal  [UNICODE_CHAR_SIZE];
    char vertical    [UNICODE_CHAR_SIZE];
    // 
    char leftUpper   [UNICODE_CHAR_SIZE];
    char rightUpper  [UNICODE_CHAR_SIZE];
    // 
    char leftLower   [UNICODE_CHAR_SIZE];
    char rightLower  [UNICODE_CHAR_SIZE];
    //
    char leftCentral [UNICODE_CHAR_SIZE];
    char rightCentral[UNICODE_CHAR_SIZE];
    //
    char centralUpper[UNICODE_CHAR_SIZE];
    char centralLower[UNICODE_CHAR_SIZE];
    //
    char central     [UNICODE_CHAR_SIZE];

};

//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ COLOR_SETUPS ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓

#define COLOR_DEEP_BLACK (ColorSetup){\
    .fg = 000,                        \
    .bg = 000                         \
}

#define COLOR_GREEN_FG_BLACK_BG (ColorSetup){\
    .fg = 10,                                \
    .bg = 0                                  \
}

#define COLOR_BLACK_FG_GREEN_BG (ColorSetup){\
    .fg = 0,                                 \
    .bg = 10                                 \
}

#define COLOR_WHITE_FG_RED_BG (ColorSetup){\
    .fg = 555,                             \
    .bg = 100                              \
}

#define COLOR_GREEN_SPREADSH (ColorSetup){\
    .fg = 40,                             \
    .bg = 0                               \
}


//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ MENU_SETUPS ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓

#define MENU_VERT_GREEN (MenuSetup){        \
    .textPattern = &(ConsolePrintSetup){    \
        .stdFileStream = stdout,            \
        .color = &(ColorSetup){             \
            .fg = 50,                       \
            .bg = 0                         \
        },                                  \
        .frame = &(FramePrintSetup){        \
            .stdFileStream = stdout,        \
            .color = &(ColorSetup){         \
                .fg = 50,                   \
                .bg = 0                     \
            },                              \
            .horizontal = "═",              \
            .vertical = "║",                \
                                            \
            .leftUpper = "╔",               \
            .rightUpper = "╗",              \
                                            \
            .leftLower = "╚",               \
            .rightLower = "╝"               \
        }                                   \
    },                                      \
    .selectorPattern = &(ConsolePrintSetup){\
        .stdFileStream = stdout,            \
        .color = &(ColorSetup){             \
                .fg = 0,                    \
                .bg = 10                    \
        },                                  \
        .frame = &(FramePrintSetup){        \
            .stdFileStream = stdout,        \
            .color = &(ColorSetup){         \
                .fg = 0,                    \
                .bg = 10                    \
            },                              \
            .horizontal = "═",              \
            .vertical = "║",                \
                                            \
            .leftUpper = "╔",               \
            .rightUpper = "╗",              \
                                            \
            .leftLower = "╚",               \
            .rightLower = "╝"               \
        }                                   \
    },                                      \
    .orientation = VERTICAL_ORIENT,         \
    .optionsSpace = 1                       \
}

#define MENU_VERT_GREEN_INVERSE (MenuSetup){ \
    .textPattern = &(ConsolePrintSetup){     \
        .stdFileStream = stdout,             \
        .color = &(ColorSetup){              \
            .fg = 0,                         \
            .bg = 10                         \
        },                                   \
        .frame = &(FramePrintSetup){         \
            .stdFileStream = stdout,         \
            .color = &(ColorSetup){          \
                .fg = 0,                     \
                .bg = 10                     \
            },                               \
            .horizontal = "═",               \
            .vertical = "║",                 \
                                             \
            .leftUpper = "╔",                \
            .rightUpper = "╗",               \
                                             \
            .leftLower = "╚",                \
            .rightLower = "╝"                \
        }                                    \
    },                                       \
    .selectorPattern = &(ConsolePrintSetup){ \
        .stdFileStream = stdout,             \
        .color = &(ColorSetup){              \
                .fg = 50,                    \
                .bg = 0                      \
        },                                   \
        .frame = &(FramePrintSetup){         \
            .stdFileStream = stdout,         \
            .color = &(ColorSetup){          \
                .fg = 50,                    \
                .bg = 0                      \
            },                               \
            .horizontal = "═",               \
            .vertical = "║",                 \
                                             \
            .leftUpper = "╔",                \
            .rightUpper = "╗",               \
                                             \
            .leftLower = "╚",                \
            .rightLower = "╝"                \
        }                                    \
    },                                       \
    .orientation = VERTICAL_ORIENT,          \
    .optionsSpace = 1                        \
}

#define MENU_VERT_RED (MenuSetup){           \
    .textPattern = &(ConsolePrintSetup){     \
        .stdFileStream = stdout,             \
        .color = &(ColorSetup){              \
            .fg = 500,                       \
            .bg = 0                          \
        },                                   \
        .frame = &(FramePrintSetup){         \
            .stdFileStream = stdout,         \
            .color = &(ColorSetup){          \
                .fg = 500,                   \
                .bg = 0                      \
            },                               \
            .horizontal = "═",               \
            .vertical = "║",                 \
                                             \
            .leftUpper = "╔",                \
            .rightUpper = "╗",               \
                                             \
            .leftLower = "╚",                \
            .rightLower = "╝"                \
        }                                    \
    },                                       \
    .selectorPattern = &(ConsolePrintSetup){ \
        .stdFileStream = stdout,             \
        .color = &(ColorSetup){              \
                .fg = 555,                   \
                .bg = 300                    \
        },                                   \
        .frame = &(FramePrintSetup){         \
            .stdFileStream = stdout,         \
            .color = &(ColorSetup){          \
                .fg = 0,                     \
                .bg = 300                    \
            },                               \
            .horizontal = "═",               \
            .vertical = "║",                 \
                                             \
            .leftUpper = "╔",                \
            .rightUpper = "╗",               \
                                             \
            .leftLower = "╚",                \
            .rightLower = "╝"                \
        }                                    \
    },                                       \
    .orientation = VERTICAL_ORIENT,          \
    .optionsSpace = 1                        \
}

#define MENU_HORI_GREEN (MenuSetup){        \
    .textPattern = &(ConsolePrintSetup){    \
        .stdFileStream = stdout,            \
        .color = &(ColorSetup){             \
            .fg = 50,                       \
            .bg = 0                         \
        },                                  \
        .frame = &(FramePrintSetup){        \
            .stdFileStream = stdout,        \
            .color = &(ColorSetup){         \
                .fg = 50,                   \
                .bg = 0                     \
            },                              \
            .horizontal = "═",              \
            .vertical = "║",                \
                                            \
            .leftUpper = "╔",               \
            .rightUpper = "╗",              \
                                            \
            .leftLower = "╚",               \
            .rightLower = "╝"               \
        }                                   \
    },                                      \
    .selectorPattern = &(ConsolePrintSetup){\
        .stdFileStream = stdout,            \
        .color = &(ColorSetup){             \
                .fg = 0,                    \
                .bg = 10                    \
        },                                  \
        .frame = &(FramePrintSetup){        \
            .stdFileStream = stdout,        \
            .color = &(ColorSetup){         \
                .fg = 0,                    \
                .bg = 10                    \
            },                              \
            .horizontal = "═",              \
            .vertical = "║",                \
                                            \
            .leftUpper = "╔",               \
            .rightUpper = "╗",              \
                                            \
            .leftLower = "╚",               \
            .rightLower = "╝"               \
        }                                   \
    },                                      \
    .orientation = HORIZONTAL_ORIENT,       \
    .optionsSpace = 2                       \
}

#define MENU_HORI_FULL_GREEN (MenuSetup){    \
    .textPattern = &(ConsolePrintSetup){     \
        .stdFileStream = stdout,             \
        .color = &(ColorSetup){              \
            .fg = 0,                         \
            .bg = 10                         \
        },                                   \
        .frame = &(FramePrintSetup){         \
            .stdFileStream = stdout,         \
            .color = &(ColorSetup){          \
                .fg = 0,                     \
                .bg = 10                     \
            },                               \
            .horizontal = "═",               \
            .vertical = "║",                 \
                                             \
            .leftUpper = "╔",                \
            .rightUpper = "╗",               \
                                             \
            .leftLower = "╚",                \
            .rightLower = "╝"                \
        }                                    \
    },                                       \
    .selectorPattern = &(ConsolePrintSetup){ \
        .stdFileStream = stdout,             \
        .color = &(ColorSetup){              \
                .fg = 0,                     \
                .bg = 50                     \
        },                                   \
        .frame = &(FramePrintSetup){         \
            .stdFileStream = stdout,         \
            .color = &(ColorSetup){          \
                .fg = 0,                     \
                .bg = 50                     \
            },                               \
            .horizontal = "═",               \
            .vertical = "║",                 \
                                             \
            .leftUpper = "╔",                \
            .rightUpper = "╗",               \
                                             \
            .leftLower = "╚",                \
            .rightLower = "╝"                \
        }                                    \
    },                                       \
    .orientation = HORIZONTAL_ORIENT,        \
    .optionsSpace = 3                        \
}

#define MENU_VERT_GREEN_NO_FRAME (MenuSetup){\
    .textPattern = &(ConsolePrintSetup){     \
        .stdFileStream = stdout,             \
        .color = &(ColorSetup){              \
            .fg = 0,                         \
            .bg = 10                         \
        },                                   \
        .frame = NULL                        \
    },                                       \
    .selectorPattern = &(ConsolePrintSetup){ \
        .stdFileStream = stdout,             \
        .color = &(ColorSetup){              \
                .fg = 0,                     \
                .bg = 50                     \
        },                                   \
        .frame = NULL                        \
    },                                       \
    .orientation = VERTICAL_ORIENT,          \
    .optionsSpace = 2                        \
}

#define MENU_VERT_GREEN_NO_FRAME_SPACE3 (MenuSetup){ \
    .textPattern = &(ConsolePrintSetup){             \
        .stdFileStream = stdout,                     \
        .color = &(ColorSetup){                      \
            .fg = 40,                                \
            .bg = 0                                  \
        },                                           \
        .frame = NULL                                \
    },                                               \
    .selectorPattern = &(ConsolePrintSetup){         \
        .stdFileStream = stdout,                     \
        .color = &(ColorSetup){                      \
                .fg = 0,                             \
                .bg = 50                             \
        },                                           \
        .frame = NULL                                \
    },                                               \
    .orientation = VERTICAL_ORIENT,                  \
    .optionsSpace = 3                                \
}

#define MENU_HORI_RED (MenuSetup){          \
    .textPattern = &(ConsolePrintSetup){    \
        .stdFileStream = stdout,            \
        .color = &(ColorSetup){             \
            .fg = 500,                      \
            .bg = 0                         \
        },                                  \
        .frame = &(FramePrintSetup){        \
            .stdFileStream = stdout,        \
            .color = &(ColorSetup){         \
                .fg = 500,                  \
                .bg = 0                     \
            },                              \
            .horizontal = "═",              \
            .vertical = "║",                \
                                            \
            .leftUpper = "╔",               \
            .rightUpper = "╗",              \
                                            \
            .leftLower = "╚",               \
            .rightLower = "╝"               \
        }                                   \
    },                                      \
    .selectorPattern = &(ConsolePrintSetup){\
        .stdFileStream = stdout,            \
        .color = &(ColorSetup){             \
                .fg = 555,                  \
                .bg = 300                   \
        },                                  \
        .frame = &(FramePrintSetup){        \
            .stdFileStream = stdout,        \
            .color = &(ColorSetup){         \
                .fg = 0,                    \
                .bg = 300                   \
            },                              \
            .horizontal = "═",              \
            .vertical = "║",                \
                                            \
            .leftUpper = "╔",               \
            .rightUpper = "╗",              \
                                            \
            .leftLower = "╚",               \
            .rightLower = "╝"               \
        }                                   \
    },                                      \
    .orientation = HORIZONTAL_ORIENT,       \
    .optionsSpace = 1                       \
}

//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ CONSOLE_PRINT_SETUPS ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓

#define CON_PRINT_ERROR_RED (ConsolePrintSetup){ \
    .stdFileStream = stderr,                     \
    .color = &(ColorSetup){                      \
        .fg = 555,                               \
        .bg = 200                                \
    },                                           \
    .frame = &(FramePrintSetup){                 \
        .stdFileStream = stdout,                 \
        .color = &(ColorSetup){                  \
            .fg = 0,                             \
            .bg = 200                            \
        },                                       \
        .horizontal = "┅",                       \
        .vertical = "┋",                         \
                                                 \
        .leftUpper = "┏",                        \
        .rightUpper = "┓",                       \
                                                 \
        .leftLower = "┗",                        \
        .rightLower = "┛"                        \
    }                                            \
}

#define CON_PRINT_PIC_GREEN (ConsolePrintSetup){ \
    .stdFileStream = stderr,                     \
    .color = &(ColorSetup){                      \
        .fg = 0,                                 \
        .bg = 40                                 \
    },                                           \
    .frame = NULL                                \
}

#define CON_PRINT_PIC_GREEN_INVERSE (ConsolePrintSetup){ \
    .stdFileStream = stderr,                             \
    .color = &(ColorSetup){                              \
        .fg = 40,                                        \
        .bg = 0                                          \
    },                                                   \
    .frame = NULL                                        \
}

#define CON_PRINT_PIC_RED (ConsolePrintSetup){ \
    .stdFileStream = stderr,                   \
    .color = &(ColorSetup){                    \
        .fg = 0,                               \
        .bg = 500                              \
    },                                         \
    .frame = NULL                              \
}

#define CON_PRINT_PIC_RED_INVERSE (ConsolePrintSetup){ \
    .stdFileStream = stderr,                           \
    .color = &(ColorSetup){                            \
        .fg = 500,                                     \
        .bg = 0                                        \
    },                                                 \
    .frame = NULL                                      \
}

#define CON_PRINT_MSG_GREEN (ConsolePrintSetup){ \
    .stdFileStream = stdout,                     \
    .color = &(ColorSetup){                      \
            .fg = 50,                            \
            .bg = 0                              \
    },                                           \
    .frame = &(FramePrintSetup){                 \
        .stdFileStream = stdout,                 \
        .color = &(ColorSetup){                  \
            .fg = 10,                            \
            .bg = 0                              \
        },                                       \
        .horizontal = "═",                       \
        .vertical = "║",                         \
                                                 \
        .leftUpper = "╔",                        \
        .rightUpper = "╗",                       \
                                                 \
        .leftLower = "╚",                        \
        .rightLower = "╝"                        \
    }                                            \
}

#define CON_PRINT_MSG_FULL_GREEN (ConsolePrintSetup){ \
    .stdFileStream = stdout,                          \
    .color = &(ColorSetup){                           \
            .fg = 0,                                  \
            .bg = 10                                  \
    },                                                \
    .frame = &(FramePrintSetup){                      \
        .stdFileStream = stdout,                      \
        .color = &(ColorSetup){                       \
            .fg = 0,                                  \
            .bg = 10                                  \
        },                                            \
        .horizontal = "═",                            \
        .vertical = "║",                              \
                                                      \
        .leftUpper = "╔",                             \
        .rightUpper = "╗",                            \
                                                      \
        .leftLower = "╚",                             \
        .rightLower = "╝"                             \
    }                                                 \
}

#define CON_PRINT_MSG_RED (ConsolePrintSetup){ \
    .stdFileStream = stdout,                   \
    .color = &(ColorSetup){                    \
            .fg = 555,                         \
            .bg = 100                          \
    },                                         \
    .frame = &(FramePrintSetup){               \
        .stdFileStream = stdout,               \
        .color = &(ColorSetup){                \
            .fg = 0,                           \
            .bg = 100                          \
        },                                     \
        .horizontal = "═",                     \
        .vertical = "║",                       \
                                               \
        .leftUpper = "╔",                      \
        .rightUpper = "╗",                     \
                                               \
        .leftLower = "╚",                      \
        .rightLower = "╝"                      \
    }                                          \
}

#define CON_PRINT_NOTE_GREEN (ConsolePrintSetup){ \
    .stdFileStream = stdout,                      \
    .color = &(ColorSetup){                       \
            .fg = 50,                             \
            .bg = 0                               \
    },                                            \
    .frame = NULL                                 \
}

#define CON_PRINT_NOTE_GREEN_INVERSE (ConsolePrintSetup){ \
    .stdFileStream = stdout,                              \
    .color = &(ColorSetup){                               \
            .fg = 0,                                      \
            .bg = 10                                      \
    },                                                    \
    .frame = NULL                                         \
}

#define CON_PRINT_NOTE_RED (ConsolePrintSetup){ \
    .stdFileStream = stdout,                    \
    .color = &(ColorSetup){                     \
            .fg = 555,                          \
            .bg = 100                           \
    },                                          \
    .frame = NULL                               \
}

#define CON_PRINT_INPUT_MSG_GREEN (ConsolePrintSetup){ \
    .stdFileStream = stdout,                           \
    .color = &(ColorSetup){                            \
            .fg = 40,                                  \
            .bg = 0                                    \
    },                                                 \
    .frame = NULL                                      \
}

//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ HEADER_SETUPS ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓

#define HEADER_PIC_GREEN (HeaderColorScheme){ \
    .stdFileStream = stdout,                  \
    .symbols = " 01",                         \
    .colors = (ColorSetup*[]){                \
        &(ColorSetup){.bg = 0   },            \
        &(ColorSetup){.bg = 20  },            \
        &(ColorSetup){.bg = 30  }             \
    },                                        \
    .frame = NULL                             \
}

#define HEADER_PIC_RED (HeaderColorScheme){ \
    .stdFileStream = stdout,                \
    .symbols = " 01",                       \
    .colors = (ColorSetup*[]){              \
        &(ColorSetup){.bg = 0   },          \
        &(ColorSetup){.bg = 500  },         \
        &(ColorSetup){.bg = 0  }            \
    },                                      \
    .frame = NULL                           \
}

#define HEADER_BIG_MSG_ALIGATOR2_GREEN (HeaderColorScheme){ \
    .stdFileStream = stdout,                                \
    .symbols = " :+#",                                      \
    .colors = (ColorSetup*[]){                              \
        &(ColorSetup){.bg = 0   },                          \
        &(ColorSetup){.bg = 50  },                          \
        &(ColorSetup){.bg = 30  },                          \
        &(ColorSetup){.bg = 10  }                           \
    },                                                      \
    .frame = &(FramePrintSetup){                            \
        .stdFileStream = stdout,                            \
        .color = &(ColorSetup){                             \
            .fg = 10,                                       \
            .bg = 0                                         \
        },                                                  \
        .horizontal = "═",                                  \
        .vertical = "║",                                    \
                                                            \
        .leftUpper = "╔",                                   \
        .rightUpper = "╗",                                  \
                                                            \
        .leftLower = "╚",                                   \
        .rightLower = "╝"                                   \
    }                                                       \
}

#define HEADER_BIG_MSG_ALIGATOR2_GREEN_NO_FRAME (HeaderColorScheme){ \
    .stdFileStream = stdout,                                         \
    .symbols = " :+#",                                               \
    .colors = (ColorSetup*[]){                                       \
        &(ColorSetup){.bg = 0   },                                   \
        &(ColorSetup){.bg = 50  },                                   \
        &(ColorSetup){.bg = 30  },                                   \
        &(ColorSetup){.bg = 10  }                                    \
    },                                                               \
    .frame = NULL                                                    \
}

#define HEADER_BIG_MSG_ALIGATOR2_RED (HeaderColorScheme){ \
    .stdFileStream = stdout,                              \
    .symbols = " :+#",                                    \
    .colors = (ColorSetup*[]){                            \
        &(ColorSetup){.bg = 0   },                        \
        &(ColorSetup){.bg = 500  },                       \
        &(ColorSetup){.bg = 300  },                       \
        &(ColorSetup){.bg = 100  }                        \
    },                                                    \
    .frame = &(FramePrintSetup){                          \
        .stdFileStream = stdout,                          \
        .color = &(ColorSetup){                           \
            .fg = 100,                                    \
            .bg = 0                                       \
        },                                                \
        .horizontal = "═",                                \
        .vertical = "║",                                  \
                                                          \
        .leftUpper = "╔",                                 \
        .rightUpper = "╗",                                \
                                                          \
        .leftLower = "╚",                                 \
        .rightLower = "╝"                                 \
    }                                                     \
}

#define HEADER_BIG_MSG_ERROR_RED (HeaderColorScheme){ \
    .stdFileStream = stdout,                          \
    .symbols = " 0123",                               \
    .colors = (ColorSetup*[]){                        \
        &(ColorSetup){.bg = 0   },                    \
        &(ColorSetup){.bg = 500  },                   \
        &(ColorSetup){.bg = 300  },                   \
        &(ColorSetup){.bg = 100  },                   \
        &(ColorSetup){.bg = 200  }                    \
    },                                                \
    .frame = &(FramePrintSetup){                      \
        .stdFileStream = stdout,                      \
        .color = &(ColorSetup){                       \
            .fg = 100,                                \
            .bg = 0                                   \
        },                                            \
        .horizontal = "═",                            \
        .vertical = "║",                              \
                                                      \
        .leftUpper = "╔",                             \
        .rightUpper = "╗",                            \
                                                      \
        .leftLower = "╚",                             \
        .rightLower = "╝"                             \
    }                                                 \
}


//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ SPREADSHEET_SETUPS ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓

#define SPREADSH_GREEN (SpreadsheetSetup){\
    .stdFileStream = stdout,              \
    .color = &(ColorSetup){               \
        .fg = 40,                         \
        .bg = 0                           \
    },                                    \
    .horizontal = "═",                    \
    .vertical = "║",                      \
    .leftUpper = "╔",                     \
    .rightUpper = "╗",                    \
    .leftLower = "╚",                     \
    .rightLower = "╝",                    \
    .leftCentral = "╠",                   \
    .rightCentral = "╣",                  \
    .centralUpper = "╦",                  \
    .centralLower = "╩",                  \
    .central = "╬"                        \
}



//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ FUNCTIONS ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓

//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// ➡ RETURNS: coordiates of lowest right side of string rectangle or
// ➡ (1, 1) cursor coordinates if operation was not successful
//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// prints String formated with (pattern.color)
// (row) and (column) are the coordinates of the start for (string) to be put
//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// 🗒 NOTE: see (enum __Option) shortcuts for better experience       
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
ConsoleCursorPosition printAt(const ConsolePrintSetup * pattern    , 
                                                 char * string     , 
                                                  int   row        , 
                                                  int   column     , 
                                                  int   charsPerRow);


//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// ➡ RETURNS: coordinates of lowest right side of printed frame or
// ➡ (1, 1) cursor coordinates if operation was not successful
//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// prints a frame according to frame print setup (frame)
// starting from (row) and (column) coordinates
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
ConsoleCursorPosition printFrame(FramePrintSetup * frame      , 
                                    const size_t   row        , 
                                    const size_t   column     , 
                                    const size_t   frameHeight, 
                                    const size_t   frameWidth);
    

//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░       
// ➡ RETURNS: extreme right horizontal line coordinates or 
// ➡ (1, 1) coordinates if operation was uncusuccessful
//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░       
// prints a horizontal line made of (unicodeChar) starting from (row) and (column) coordinates
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
ConsoleCursorPosition printHorizontal(FILE * stdFileStream, 
                              const   char   unicodeChar[], 
                              const size_t   row          ,
                              const size_t   column       , 
                              const size_t   length       );


//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// ➡ RETURNS: extreme lower vertical line coordinates or 
// ➡ (1, 1) coordinates if operation was unsuccessful
//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// prints a vertical line made of (unicodeChar) starting from (row) and (column) coordinates
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
ConsoleCursorPosition printVertical(FILE * stdFileStream, 
                            const   char   unicodeChar[], 
                            const size_t   row          ,
                            const size_t   column       , 
                            const size_t   length       );


//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░          
// ➡ RETURNS: TRUE if console color was changed or FALSE if not
//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// sets new console color according to color setup (color)
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
bool setColor(const ColorSetup * color);


//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// vanishes console and sets it to (newConsoleColor)
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
void vanish(const ColorSetup * newConsoleColor);


//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// ➡ RETURNS: quantity of symbols actually read
//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// reads user's input in (buffer) untill have met (terminalSymbol) or buffer end
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
int readString(char * buffer        ,
       const size_t   bufferSize    ,
       const   char   terminalSymbol);



//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// ➡ RETURNS: coordiates of lowest right side of printed list or
// ➡ (1, 1) cursor coordinates if operation was unsuccessful
//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// prints (list) at coordinates (row, column), (maxCharsPerRow) is a max limit for list
// 🗒 NOTE: Min value of (maxCharsPerRow == 13)
// to be sure of exact chars per row value of printed list
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
ConsoleCursorPosition printProviderList(const ProviderList * list, ColorSetup * color,
                                        int row, int column);



//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// ➡ RETURNS: coordiates of lowest right side of printed rectangle or
// ➡ (1, 1) cursor coordinates if operation was unsuccessful
//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// prints a rectangle with frame or without
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
ConsoleCursorPosition printRectangle(const ConsolePrintSetup * pattern, 
                                                         int   row    ,
                                                         int   column ,
                                                         int   height ,
                                                         int   width  );


//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// ➡ RETURNS: maximum string width according to '\n' symbols in it
// ➡ or FAIL if operation was unsuccessful
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
int getStringMaxWidth(char * string);


//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// ➡ RETURNS: index of option user has chosen
//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// 🗒 NOTE: index numeration starts form 0
//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// prints user menu according to options after (optionsQuantity)
// and (menuSetup)
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ 
short userMenu(ConsoleCursorPosition * cursor         ,
                           MenuSetup * menuSetup      ,
                                 int   row            ,
                                 int   column         ,
                                 int   charsPerRow    , 
                               short   optionsQuantity,
                                    ...               );


//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// ➡ RETURNS: TRUE if symbol is a supplementary unicode symbol or FALSE if not
//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// 🗒 NOTE: each unicode supplementary sybmol has two 1 0 bits at it's 
// 🗒 most significant binary digit number
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ 
bool isUnicodeExtraSymbol(char symbol);


//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// prints a header formated according to (colorScheme)
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ 
ConsoleCursorPosition printHeader(HeaderColorScheme * colorScheme,
                                               char * string     ,
                                                int   row        ,
                                                int   column     ,
                                                int   charsPerRow);


//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// small function for header to determine a color from (headerScheme) to be set
// according to (symbol)
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ 
ColorSetup * headerColorDeterminator(HeaderColorScheme * headerScheme, char symbol);


//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// exits from program and executes (func())
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ 
void exitProgram(Option colorTheme, const int exitCode, char * message, void(*func)(void*), const int argQuantity, ...);


//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// prints spreadsheet
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ 
ConsoleCursorPosition printSpreadsheet(const SpreadsheetSetup * setup     ,
                                                          int   row       ,
                                                          int   column    ,
                                                          //
                                                          int   rows      ,
                                                          int   columns   ,
                                                          //
                                                          int   rowSize   ,
                                                          int   columnSize);

//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// reads provider fields from users input
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ 
void getProviderData(Provider * provider);