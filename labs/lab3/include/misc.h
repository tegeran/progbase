
// Copyright © 2017 Vitaly Kryvenko, all rights reserved.
// MISCELLANEOUS FUNCTIONS

#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#ifndef SUCCESS
#define SUCCESS 1
#endif

#ifndef FAIL
#define FAIL -1
#endif


#define MAX_INDX >

#define MIN_INDX <

#define GET_EXTREMUM_INDEX(haystack, haySize, needle, result) \
    if (haystack == NULL || haySize < 1){                     \
        result = -1;                                          \
    }                                                         \
    result = 0;                                               \
    for (int i = 1; i < haySize; i++){                        \
        if (haystack[i] needle haystack[result]){             \
            result = i;                                       \
        }                                                     \
    }        
    
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓ FUNCTIONS ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
//▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓


//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// ➡ RETURNS: 1 if two double (value)s are close enough or 0 if not
//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
short doubleCmp(double value1, double value2, const double accuracy);



//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
// if (func != NULL) and (argQuantity > 0)
// executes (func(...)) (argQuantity) times and than does exit(exitCode)
//░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
void terminateIf(const int value, const int exitCode, void(*func)(void*), const int argQuantity, ...);

int totalArgs(const char * args);