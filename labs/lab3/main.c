#include <progbase/console.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "include/provider.h"
#include "include/user_friendly.h"
#include "include/misc.h"
#include "include/testroom.h"
#include "include/mem.h"


int main(int argc, char * argv[]){
    ConsoleCursorPosition cursor = Console_cursorPosition();
    if (argc == 2 && strlen(argv[1]) == 2 && !strcmp(argv[1], "-t")){
        // TEST SECTION
        testFunc((void(*)())doubleCmp);
        testFunc((void(*)())setPointersToNULL);
        testFunc((void(*)())compareProvider);
        testFunc((void(*)())newProviderDyn);
        testFunc((void(*)())sortProviderList);
        testFunc((void(*)())addToProviderList);
        testFunc((void(*)())removeProviderFromListDyn);
        testFunc((void(*)())rewriteProvider);
        testFunc((void(*)())copyProviderListDyn);
        testFunc((void(*)())highestTariffProvidersDyn);
        testFunc((void(*)())dataCloudToString);
        testFunc((void(*)())providerToString);
        testFunc((void(*)())compareDataCloud);
        testFunc((void(*)())stringToDataCloud);
        testFunc((void(*)())stringToProvider);
        testFunc((void(*)())providerToString);
        testFunc((void(*)())providerListToString);
        testFunc((void(*)())stringToProviderList);
        testFunc((void(*)())getStringMaxWidth);
        
        return EXIT_SUCCESS;
    } else if (argc != 1){
        setColor (&(ColorSetup){
            .fg = -1,                        
            .bg = 500                         
        });
        vanish(&COLOR_DEEP_BLACK);
        // pic "NOPE FACE"
        printAt(&CON_PRINT_PIC_RED_INVERSE,
            "                          ▐█          \n"
            "     ▄                  ▄█▓█▌         \n"
            "    ▐██▄               ▄▓░░▓▓         \n"
            "    ▐█░██▓            ▓▓░░░▓▌         \n"
            "    ▐█▌░▓██          █▓░░░░▓          \n"
            "     ▓█▌░░▓█▄███████▄███▓░▓█          \n"
            "     ▓██▌░▓██░░░░░░░░░░▓█░▓▌          \n"
            "      ▓█████░░░░░░░░░░░░▓██           \n"
            "      ▓██▓░░░░░░░░░░░░░░░▓█           \n"
            "      ▐█▓░░░░░░█▓░░▓█░░░░▓█▌          \n"
            "      ▓█▌░▓█▓▓██▓░█▓▓▓▓▓░▓█▌          \n"
            "      ▓▓░▓██████▓░▓███▓▓▌░█▓          \n"
            "     ▐▓▓░█▄▐▓▌█▓░░▓█▐▓▌▄▓░██          \n"
            "     ▓█▓░▓█▄▄▄█▓░░▓█▄▄▄█▓░██▌         \n"
            "     ▓█▌░▓█████▓░░░▓███▓▀░▓█▓         \n"
            "    ▐▓█░░░▀▓██▀░░░░░ ▀▓▀░░▓█▓         \n"
            "    ▓██░░░░░░░░▀▄▄▄▄▀░░░░░░▓▓         \n"
            "    ▓█▌░░░░░░░░░░▐▌░░░░░░░░▓▓▌        \n"
            "    ▓█░░░░░░░░░▄▀▀▀▀▄░░░░░░░█▓        \n"
            "   ▐█▌░░░░░░░░▀░░░░░░▀░░░░░░█▓▌       \n"
            "   ▓█░░░░░░░░░░░░░░░░░░░░░░░██▓       \n"
            "   ▓█░░░░░░░░░░░░░░░░░░░░░░░▓█▓       \n"
            "   ██░░░░░░░░░░░░░░░░░░░░░░░░█▓       \n"
            "   █▌░░░░░░░░░░░░░░░░░░░░░░░░▐▓▌      \n"
            "  ▐▓░░░░░░░░░░░░░░░░░░░░░░░░░░█▓      \n"
            "  █▓░░░░░░░░░░░░░░░░░░░░░░░░░░▓▓      \n"
            "  █▓░░░░░░░░░░░░░░░░░░░░░░░░░░▓▓▌     \n"
            " ▐█▓░░░░░░░░░░░░░░░░░░░░░░░░░░░██     \n"
            " █▓▌░░░░░░░░░░░░░░░░░░░░░░░░░░░▓█     \n"
            "██████████████████████████████████    \n"
            "█░▀░░░░▀█▀░░░░░░▀█░░░░░░▀█▀░░░░░▀█    \n"
            "█░░▐█▌░░█░░░██░░░█░░██░░░█░░░██░░█    \n"
            "█░░▐█▌░░█░░░██░░░█░░██░░░█░░░██░░█    \n"
            "█░░▐█▌░░█░░░██░░░█░░░░░░▄█░░▄▄▄▄▄█    \n"
            "█░░▐█▌░░█░░░██░░░█░░░░████░░░░░░░█    \n"
            "█░░░█░░░█▄░░░░░░▄█░░░░████▄░░░░░▄█    \n"
            "██████████████████████████████████    "    
        ,2, RIGHT_ALIGN, AUTO_SIZE);

        // pic "FACEPALM"
        cursor = printAt(&CON_PRINT_PIC_RED_INVERSE,
            "                             ▄▄▄       \n"     
            "                            ███▀█      \n"      
            "                            ███░██     \n"       
            "                             ███░░██   \n"    
            "                            ▄██░░░░██  \n"   
            "                ▄▄█▀▀▀▀█▄▄▄▄███▄░░░██  \n"  
            "             ▄█▀░░░░░░░░░▀▀▀▀█░▀█░░██  \n"   
            "            █▀░░░░░░░░░░░░░░░▀█▄█▀▀▀   \n"   
            "           ▄▀░░░░░░░░░░░░░░░░▀█▄       \n" 
            "           ▀█▄░█░░▄▀░░░░░░░▄▄█         \n" 
            "        ▄▄▄▄█▀▀▀██▄▄░░░░░░▄▀           \n"            
            "    ▄▄██▄▀▀░░░░░█▀░░░░░░▄██▄           \n" 
            "  ▄██▀▀░░░░░░░░▄█░░░░░▄█▀▄█░▀▀█▄▄▄▄▄▄▄▄\n"
            " ██▀░░░░░░░░▄██░░░░░░▄▀▄▄▀░░░░░░░░░░░░░\n"
            "█▀▀░░░░░░▄█▀▄░▀▄▄▄▄██▀▀▀░░░░░█░░░░░░░░░\n"
            "█░░░░░▄▄▀    █░░░░░░░░░░░░░░░▀░░░░░░░░░\n"
            " ▀▀▀▀▀▀       █░░░░░░░░░░░░░░░▀▀█▀▀▀▀▀▀\n"
            "              █░░░░░░░░░░░░░░░░█▀      \n"
            "              █░░░░░░░░░░░░░░░▄█       "
        , 2, 2, AUTO_SIZE);

        // pic "WTF!?"
        printAt(&CON_PRINT_PIC_RED_INVERSE,
            "               ▄▄▄█▀▀▀▀▀▀▀▄▄▄          \n"        
            "            ▄█▀▀░░░░░░░░░░░▄█▀█▄▄      \n"           
            "          ▄█▀░░░░░░░░░░░░░░█░█▄░▀█▄    \n"        
            "          █░░░░░░░░░░░▄░▄░░▄░▄▀░░░░█▄  \n"    
            "         █░░░░░░░░░░▄▄▄▀██▄░█▄░▄▄░░░▀▄ \n"  
            "        █▀░░░░░░░░░░░▀█▄░▄██░███▀█▄░░█▄\n"
            "        █░░░░░░░░░░░▀▀▀▀▀░░░░▀████▀░░░█\n"
            "        █░░░░░░░░░░░░░▀▀▀░▄▄▄░░█░░░░░░█\n"
            "        █░░░░░░░░░░░░░░░░░██▀▀▀▀█░░░░░█\n"
            "        █░░░░░░░░░░░░░░░░█░▄▀▄▄▄▀░░░░░█\n"
            "        █░░░░░░░░░░░░░░░░██▀████░░░░░▄█\n"
            "        █▄░░░░░░░░░░░░░░░█▄█████░░░░░█ \n"
            "         █▄░░░░░░░░░░░░░▀▀█████▀░░░░▄▀ \n"     
            "         ▄██▄░░░░░░░░░░▄▄▄▄▄░░▄░░░▄█▀  \n"     
            "       ▄█▀▄░░▀▀▄▄░░▄▄▀░▀░░░░▀▀▀▄▄▀▀    \n"     
            "     ▄█▀░░░▀▄░░░░▀▀▀██▄▄▄▄▄▄█▀▀        \n"         
            "   ▄▀░░░░░░░░█▄░░░░░░░▄█▀▄ ▄▄▄    ▀███ \n"     
            "  █▀░░░░░░░░░░▀█▄░░░░▄█▄▄▄█████▄▄▄▀    \n"       
            "▄▀░░░░░░░░░░░░░░░▀▀▀█▀▀▀    ▀██▀▀      "
        , cursor.row + 3, 2, AUTO_SIZE);

        // "ERROR"
        cursor = printHeader(&HEADER_BIG_MSG_ERROR_RED,"\n"
            " 00000000  0000000   0000000    000000   0000000  \n"  
            " 00000000  00000000  00000000  00000000  00000000 \n"  
            " 001       001  000  001  000  001  000  001  000 \n"  
            " 101       101  010  101  010  101  010  101  010 \n"  
            " 011121    0101101   0101101   010  101  0101101  \n"  
            " 111112    110101    110101    101  111  110101   \n"  
            " 112       112 211   112 211   112  111  112 211  \n"  
            " 212       212  121  212  121  212  121  212  121 \n"  
            "  22 2222  22   222  22   222  22222 22  22   222 \n"  
            " 2 22 22    2   2 2   2   2 2   2 2  2    2   2 2 \n" 
        , 2, CENTER_ALIGN, AUTO_SIZE);

        //
        cursor = printAt(&CON_PRINT_ERROR_RED,"Invalid command line arguments",
                                     cursor.row + 4, CENTER_ALIGN, AUTO_SIZE);

        //
        userMenu(&cursor, &MENU_VERT_RED, cursor.row + 4, 
                        CENTER_ALIGN, AUTO_SIZE, 1, "🚪 PRESS ENTER TO EXIT");
        
        //
        vanish(DEFAULT_COLORS);
        return EXIT_FAILURE;
    }
    ProviderList mainList;
    setPointersToNULL((void**)mainList.list, PROV_LIST_CAPACITY);
    mainList.size = 0;
    while (1){
        const int INPUT_SIZE = 50;
        char input[INPUT_SIZE];
        int goToMain = 1;
        while (goToMain){
            vanish(&COLOR_DEEP_BLACK);
            goToMain = 0;
                // "PROVIDERERR"
            cursor = printHeader(&HEADER_BIG_MSG_ALIGATOR2_GREEN, "\n"
                "  :::::::::  :::::::::   ::::::::  :::     ::: ::::::::::: :::::::::  :::::::::: :::::::::  :::::::::: :::::::::  :::::::::   \n"
                "  :+:    :+: :+:    :+: :+:    :+: :+:     :+:     :+:     :+:    :+: :+:        :+:    :+: :+:        :+:    :+: :+:    :+:  \n"
                "  +:+    +:+ +:+    +:+ +:+    +:+ +:+     +:+     +:+     +:+    +:+ +:+        +:+    +:+ +:+        +:+    +:+ +:+    +:+  \n"
                "  +#++:++#+  +#++:++#:  +#+    +:+ +#+     +:+     +#+     +#+    +:+ +#++:++#   +#++:++#:  +#++:++#   +#++:++#:  +#++:++#:   \n"
                "  +#+        +#+    +#+ +#+    +#+  +#+   +#+      +#+     +#+    +#+ +#+        +#+    +#+ +#+        +#+    +#+ +#+    +#+  \n"
                "  #+#        #+#    #+# #+#    #+#   #+#+#+#       #+#     #+#    #+# #+#        #+#    #+# #+#        #+#    #+# #+#    #+#  \n"
                "  ###        ###    ###  ########      ###     ########### #########  ########## ###    ### ########## ###    ### ###    ###  \n"
            , 2, CENTER_ALIGN, AUTO_SIZE);
    
            // COPYRIGHT NOTATION
            printAt(&CON_PRINT_NOTE_GREEN, 
                "Copyright © 2017 Vitaly Kryvenko     \n"
                "       all rights reserved         "
            , cursor.row + 23, RIGHT_ALIGN, AUTO_SIZE);
    
            // "NAVIGATE WITH WASD"
            printAt(&CON_PRINT_NOTE_GREEN,
                "                         ____        \n"
                "                        ┃┃W ┃┃       \n"
                "                        ┃┃__┃┃       \n"
                "  NAVIGATE WITH:        ┃╱__╲┃       \n"  
                "                   ____  ____  ____  \n"
                "                  ┃┃A ┃┃┃┃S ┃┃┃┃D ┃┃ \n"
                "                  ┃┃__┃┃┃┃__┃┃┃┃__┃┃ \n"
                "                  ┃╱__╲┃┃╱__╲┃┃╱__╲┃ \n"
            ,cursor.row + 19, LEFT_ALIGN, AUTO_SIZE);
    
            // "VITAHA.DEV
            printAt(&CON_PRINT_NOTE_GREEN,
                "┌┐  ┌┐┌──┐┌────┐┌───┐┌┐ ┌┐┌───┐  ┌───┐┌───┐┌┐  ┌┐\n" 
                "│└┐┌┘│└┤├┘│┌┐┌┐││┌─┐│││ │││┌─┐│  └┐┌┐││┌──┘│└┐┌┘│\n" 
                "└┐││┌┘ ││ └┘││└┘││ │││└─┘│││ ││   │││││└──┐└┐││┌┘\n" 
                " │└┘│  ││   ││  │└─┘││┌─┐││└─┘│   │││││┌──┘ │└┘│ \n" 
                " └┐┌┘ ┌┤├┐  ││  │┌─┐│││ │││┌─┐│┌┐┌┘└┘││└──┐ └┐┌┘ \n" 
                "  └┘  └──┘  └┘  └┘ └┘└┘ └┘└┘ └┘└┘└───┘└───┘  └┘  " 
            , cursor.row + 20, CENTER_ALIGN, AUTO_SIZE);
    
            // pic "GLASSES"
            printAt(&CON_PRINT_PIC_GREEN_INVERSE,
                "▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄\n"
                "█        ▀█▄▀▄▀██████ ▀█▄▀▄▀██████\n"                                   
                "            ▀█▄█▄███▀    ▀█▄█▄███"
            , cursor.row + 2, CENTER_ALIGN, AUTO_SIZE);
            //
            switch(userMenu(&cursor, &MENU_VERT_GREEN, cursor.row + 8, CENTER_ALIGN, 
                            AUTO_SIZE, 3,   "🆕  CREATE NEW BLANK PROVIDER LIST ",
                                          "📖  READ PROVIDER LIST FROM FILE ",
                                          "              🚪 EXIT")){
        
                case 0:{
                    break;
                }
                case 1:{
                    short retry = 1;
                    while (retry && !goToMain){
                        retry = 0;
                        vanish(&COLOR_DEEP_BLACK);
                        // "FILE"
                        cursor = printHeader(&HEADER_BIG_MSG_ALIGATOR2_GREEN, "\n"
                            " :::::::::: ::::::::::: :::        :::::::::: \n"
                            " :+:            :+:     :+:        :+:        \n"
                            " +:+            +:+     +:+        +:+        \n"
                            " :#::+::#       +#+     +#+        +#++:++#   \n"
                            " +#+            +#+     +#+        +#+        \n"
                            " #+#            #+#     #+#        #+#        \n"
                            " ###        ########### ########## ########## \n"
                        , 2, CENTER_ALIGN, AUTO_SIZE);
                        //
                        cursor = printAt(&CON_PRINT_INPUT_MSG_GREEN, " Enter file path: \n 📂 ", cursor.row + 2,
                                cursor.column - 48, AUTO_SIZE);
                        setColor(&COLOR_GREEN_FG_BLACK_BG);
                        Console_setCursorPosition(cursor.row, cursor.column - 15);
                        readString(input, INPUT_SIZE, TERMINAL_SYMBOL);
                        if (!fileExists(input)){
                            while (!retry && !goToMain){
                                vanish(&COLOR_DEEP_BLACK);
                                // "ERROR"
                                cursor = printHeader(&HEADER_BIG_MSG_ERROR_RED,"\n"
                                        " 00000000  0000000   0000000    000000   0000000  \n"  
                                        " 00000000  00000000  00000000  00000000  00000000 \n"  
                                        " 001       001  000  001  000  001  000  001  000 \n"  
                                        " 101       101  010  101  010  101  010  101  010 \n"  
                                        " 011121    0101101   0101101   010  101  0101101  \n"  
                                        " 111112    110101    110101    101  111  110101   \n"  
                                        " 112       112 211   112 211   112  111  112 211  \n"  
                                        " 212       212  121  212  121  212  121  212  121 \n"  
                                        "  22 2222  22   222  22   222  22222 22  22   222 \n"  
                                        " 2 22 22    2   2 2   2   2 2   2 2  2    2   2 2 \n" 
                                , 2, CENTER_ALIGN, AUTO_SIZE);
                                cursor = printAt(&CON_PRINT_ERROR_RED, "\n File doesn't exist or you don't have access to it \n",
                                        cursor.row + 3, CENTER_ALIGN, AUTO_SIZE);
                                switch(USER_MENU(NULL, &MENU_VERT_RED, cursor.row + 3, CENTER_ALIGN, AUTO_SIZE,
                                         "       ⭯ RETRY ",
                                         " 🢠 BACK TO MAIN MENU ",
                                         "       🚪 EXIT ")){
                                    //
                                    case 0:{
                                        retry = 1;
                                        break;
                                    }
                                    case 1:{
                                        goToMain = 1;
                                        break;
                                    }
                                    case 2:{
                                        exitProgram(RED_THEME, EXIT_SUCCESS, " Are sure you want to exit? ", NULL, 0);
                                        retry = 0;
                                    }
                                }
                            }     
                        }
                        else {
                            while (!retry && !goToMain){
                                mallocProviderList(&mainList, PROV_LIST_CAPACITY);
                                if (fileToProviderList(&mainList, input) == FAIL){
                                    freeProviderListDyn(&mainList);
                                    vanish(&COLOR_DEEP_BLACK);
                                    // "FAIL"
                                    cursor = printHeader(&HEADER_BIG_MSG_ALIGATOR2_RED,"\n"
                                            " ::::::::::     :::     ::::::::::: :::        \n"
                                            " :+:          :+: :+:       :+:     :+:        \n"
                                            " +:+         +:+   +:+      +:+     +:+        \n"
                                            " :#::+::#   +#++:++#++:     +#+     +#+        \n"
                                            " +#+        +#+     +#+     +#+     +#+        \n"
                                            " #+#        #+#     #+#     #+#     #+#        \n"
                                            " ###        ###     ### ########### ########## \n"
                                    , 2, CENTER_ALIGN, AUTO_SIZE);
                                    cursor = printAt(&CON_PRINT_ERROR_RED, "\n Failed to read providers from file \n"
                                            , cursor.row + 3, CENTER_ALIGN, AUTO_SIZE);
                                    switch(userMenu(NULL, &MENU_VERT_RED, cursor.row + 3, CENTER_ALIGN, AUTO_SIZE, 3,
                                                    "       ⭯ RETRY ",
                                                    " 🢠 BACK TO MAIN MENU ",
                                                    "       🚪 EXIT ")){
                                        //
                                        case 0:{
                                            retry = 1;
                                            break;
                                        }
                                        case 1:{
                                            goToMain = 1;
                                            retry = 0;
                                            break;
                                        }
                                        case 2:{
                                            exitProgram(RED_THEME, EXIT_SUCCESS, " Are sure you want to exit? ", NULL, 0);
                                            retry = 0;
                                        }
                                    }
                                }
                                else {
                                    retry = 0;
                                    goToMain = 0;
                                    freeExessProvidersDyn(&mainList);
                                    break;
                                }
                            // while (!retry && !goToMain)
                            }
                        }
                        
                    // while (!goToMain)
                    }
                    break;
                }
                case 2:{
                    exitProgram(GREEN_THEME, EXIT_SUCCESS, " Are sure you want to exit? ", NULL, 0);
                    goToMain = 1;
                }
                //switch case
            } 
        //Main menu    
        }
        while (!goToMain){
            vanish(&COLOR_DEEP_BLACK);
            ConsoleCursorPosition spreadshPos = {
                .row = 5,
                .column = 40
            };
            printProviderList(&mainList, &COLOR_GREEN_SPREADSH, spreadshPos.row, spreadshPos.column);
            int task = 0;
            if (mainList.size != PROV_LIST_CAPACITY){
                if (mainList.size > 0){
                    task = userMenu(&cursor, &MENU_VERT_GREEN, 5, 6, AUTO_SIZE, 8, 
                        " ADD ",
                        " DELETE ",
                        " REWRITE" ,
                        " UPDATE FIELD ",
                        " SHOW \'K\' HIGHEST \n TARIFF PROVIDERS ",
                        " SAVE TO FILE ",
                        " BACK TO MAIN MENU ",
                        " EXIT ");
                }
                else {
                    switch (userMenu(&cursor, &MENU_VERT_GREEN, 5, 6, AUTO_SIZE, 3, 
                                     " ADD ",
                                     // DELETE
                                     // REWRITE ,
                                     // UPDATE FIELD ,
                                     // SHOW \'K\' HIGHEST \n TARIFF PROVIDERS ,
                                     // SAVE TO FILE //,
                                     " BACK TO MAIN MENU ",
                                     " EXIT ")){
                        case 1:{
                            task = 6;
                            break;
                        }
                        case 2:{
                            task = 7;
                        }
                    }
                }
            }
            else {
                task = userMenu(&cursor, &MENU_VERT_GREEN, 5, 6, AUTO_SIZE, 7, 
                    // ADD ,
                    " DELETE ",
                    " REWRITE" ,
                    " UPDATE FIELD ",
                    " SHOW \'K\' HIGHEST \n TARIFF PROVIDERS ",
                    " SAVE TO FILE ",
                    " BACK TO MAIN MENU ",
                    " EXIT ") + 1; 
            }       
            switch(task){
                case 0:{ // ADD
                    Provider * newProvider = newProviderDyn("Name", 1, 1, 1, 0, 0);
                    getProviderData(newProvider);
                    addToProviderList(newProvider, &mainList);
                    break;
                }
                case 1:{ // DELETE
                    Provider * removedProvider;
                    removeProviderFromListDyn((mainList.size == 1) ? 0 : userMenu(NULL, &MENU_VERT_GREEN_NO_FRAME_SPACE3, 
                                            spreadshPos.row + PROV_SPREADSH_ROW_HEIGHT + 2,
                                            spreadshPos.column + 1, AUTO_SIZE, mainList.size,
                                            "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"),
                                            &mainList, &removedProvider);
                    if (removedProvider != NULL){
                        free(removedProvider);
                    }
                    break;
                }
                case 2:{ // REWRITE
                    short provIndx = (mainList.size == 1) ? 0 : userMenu(NULL, &MENU_VERT_GREEN_NO_FRAME_SPACE3, 
                                            spreadshPos.row + PROV_SPREADSH_ROW_HEIGHT + 2,
                                            spreadshPos.column + 1, AUTO_SIZE, mainList.size,
                                            "1", "2", "3", "4", "5", "6", "7", "8", "9", "10");
                    Provider rewritten;
                    getProviderData(&rewritten);
                    rewriteProvider(mainList.list[provIndx], rewritten.name, rewritten.tariff,
                                    rewritten.loss, rewritten.hostOffer, rewritten.cloud.capacity,
                                    rewritten.cloud.tariff);
                    break;
                }
                case 3:{ // UPDATE
                    const short RECT_WIDTH = 60, RECT_HEIGHT = 25;
                    cursor.row = 10;
                    cursor.column = (Console_size().columns - RECT_WIDTH) / 2 + 1;
                    short provIndx = (mainList.size == 1) ? 0 : userMenu(NULL, &MENU_VERT_GREEN_NO_FRAME_SPACE3, 
                                              spreadshPos.row + PROV_SPREADSH_ROW_HEIGHT + 2,
                                              spreadshPos.column + 1, AUTO_SIZE, mainList.size,
                                              "1", "2", "3", "4", "5", "6", "7", "8", "9", "10");
                    printRectangle(&CON_PRINT_MSG_FULL_GREEN, cursor.row, cursor.column, RECT_HEIGHT, RECT_WIDTH);
                    Console_setCursorAttributes(ATTR_BRIGHT);
                    printAt(&CON_PRINT_MSG_GREEN, "\n Choose a field to update \n",
                                     cursor.row + 2, CENTER_ALIGN, AUTO_SIZE);
                    char field[30] = "name";
                    char message[100] = "Name (2 - 50 symbols, no ':' symbol)";
                    int skip = 0;
                    switch(userMenu(NULL, &MENU_VERT_GREEN_INVERSE, cursor.row + 8, CENTER_ALIGN, AUTO_SIZE, 5,
                                     "NAME", "TARIFF", "PACKETLOSS", "TOTAL DOMENS", "DATA CLOUD")){
                        case 0:{
                            break;
                        }
                        case 1:{
                            strcpy(field, "tariff");
                            strcpy(message, "Tariff ( ⩾ 0)");
                            break;
                        }
                        case 2:{
                            strcpy(field, "loss");
                            strcpy(message, "PacketLoss ( ⩾ 0 & ⩽ 100)");
                            break;
                        }
                        case 3:{
                            strcpy(field, "host offer");
                            strcpy(message, "Total domens ( ⩾ 0)");
                            break;
                        }
                        case 4:{
                            printRectangle(&CON_PRINT_MSG_FULL_GREEN, cursor.row, cursor.column, RECT_HEIGHT, RECT_WIDTH);
                            if (mainList.list[provIndx]->cloud.capacity != 0){
                                switch(userMenu(NULL, &MENU_VERT_GREEN_INVERSE, cursor.row + 8, 
                                        CENTER_ALIGN, AUTO_SIZE, 3, "DELETE CLOUD", "CAPACITY", "TARIFF")){
                                    case 0:{
                                        updateProvider(mainList.list[provIndx], "cloud capacity", "0");
                                        skip = 1;
                                        break;
                                    }
                                    case 1:{
                                        strcpy(field, "cloud capacity");
                                        strcpy(message, "Data cloud capacity ( > 0)");
                                        break;
                                    }
                                    case 2:{
                                        strcpy(field, "cloud tariff");
                                        strcpy(message, "Data cloud tariff ( ⩾ 0)");
                                    }
                                }
                            }
                            else {
                                switch(userMenu(NULL, &MENU_VERT_GREEN_INVERSE, cursor.row + 8, 
                                        CENTER_ALIGN, AUTO_SIZE, 2, "CAPACITY", "TARIFF")){
                                    case 0:{
                                        strcpy(field, "cloud capacity");
                                        strcpy(message, "Data cloud capacity ( > 0)");
                                        break;
                                    }
                                    case 1:{
                                        strcpy(field, "cloud tariff");
                                        strcpy(message, "Data cloud tariff ( ⩾ 0)");
                                    }
                                }
                            }
                        }
                    }
                    if (!skip){
                        printRectangle(&CON_PRINT_MSG_FULL_GREEN, cursor.row, cursor.column, RECT_HEIGHT, RECT_WIDTH);
                        Console_setCursorAttributes(ATTR_BRIGHT);
                        printAt(&CON_PRINT_NOTE_GREEN_INVERSE, message, cursor.row + 8, CENTER_ALIGN, AUTO_SIZE);
                        setColor(&COLOR_BLACK_FG_GREEN_BG);
                        Console_setCursorPosition(cursor.row + 10, cursor.column + 3);
                        while (readString(input, INPUT_SIZE, TERMINAL_SYMBOL) <= 0 || 
                               updateProvider(mainList.list[provIndx], field, input) == FAIL){
                            //
                            printRectangle(&CON_PRINT_MSG_RED, cursor.row, cursor.column, RECT_HEIGHT, RECT_WIDTH);
                            Console_setCursorAttributes(ATTR_BRIGHT);
                            printAt(&CON_PRINT_NOTE_RED, message, cursor.row + 8, CENTER_ALIGN, AUTO_SIZE);
                            setColor(&COLOR_WHITE_FG_RED_BG);
                            Console_setCursorPosition(cursor.row + 10, cursor.column + 3);
                        }
                    }
                    break;
                }
                case 4:{ // SHOW K HIGHEST  TARIFF PROVIDERS 
                    Console_setCursorAttributes(ATTR_BRIGHT);
                    cursor = printAt(&CON_PRINT_MSG_FULL_GREEN, 
                                                     "\n          number of providers          \n\n\n\n\n",
                                                     20, CENTER_ALIGN, AUTO_SIZE);
                    setColor(&COLOR_BLACK_FG_GREEN_BG);
                    int k = 0;
                    ProviderList newList;
                    Console_setCursorPosition(cursor.row - 3, cursor.column - 35);
                    int NoNeedCleanBuffer = 1;
                    while ((NoNeedCleanBuffer = scanf("%i", &k)) == 0 ||
                                     highestTariffProvidersDyn(&mainList, &newList, k) == FAIL){
                        if (!NoNeedCleanBuffer){
                            getchar();
                        } 
                        Console_setCursorAttributes(ATTR_BRIGHT);
                        printAt(&CON_PRINT_MSG_RED, "\n          number of providers          \n\n\n\n\n",
                                         20, CENTER_ALIGN, AUTO_SIZE);
                        setColor(&COLOR_WHITE_FG_RED_BG);
                        Console_setCursorPosition(cursor.row - 3, cursor.column - 35);
                    }
                    vanish(&COLOR_DEEP_BLACK);
                    printProviderList(&newList, &COLOR_GREEN_SPREADSH, spreadshPos.row, spreadshPos.column);
                    userMenu(&cursor, &MENU_VERT_GREEN, 5, 6, AUTO_SIZE, 1, 
                        " PRESS ENTER TO CONTINUE");
                    freeProviderListDyn(&newList);
                    break;
                }
                case 5:{ // SAVE TO FILE
                    short leave = 0;
                    while (!leave){
                        leave = 1;
                        vanish(&COLOR_DEEP_BLACK);
                        // "FILE"
                        cursor = printHeader(&HEADER_BIG_MSG_ALIGATOR2_GREEN, "\n"
                            " :::::::::: ::::::::::: :::        :::::::::: \n"
                            " :+:            :+:     :+:        :+:        \n"
                            " +:+            +:+     +:+        +:+        \n"
                            " :#::+::#       +#+     +#+        +#++:++#   \n"
                            " +#+            +#+     +#+        +#+        \n"
                            " #+#            #+#     #+#        #+#        \n"
                            " ###        ########### ########## ########## \n"
                        , 2, CENTER_ALIGN, AUTO_SIZE);
                        //
                        cursor = printAt(&CON_PRINT_INPUT_MSG_GREEN, " Enter file path: \n 📂 ", cursor.row + 2,
                                cursor.column - 48, AUTO_SIZE);
                        setColor(&COLOR_GREEN_FG_BLACK_BG);
                        Console_setCursorPosition(cursor.row, cursor.column - 15);
                        readString(input, INPUT_SIZE, TERMINAL_SYMBOL);
                        short retry = 0;
                        if (!fileExists(input)){
                            while (!retry && leave){
                                vanish(&COLOR_DEEP_BLACK);
                                // "ERROR"
                                cursor = printHeader(&HEADER_BIG_MSG_ERROR_RED,"\n"
                                        " 00000000  0000000   0000000    000000   0000000  \n"  
                                        " 00000000  00000000  00000000  00000000  00000000 \n"  
                                        " 001       001  000  001  000  001  000  001  000 \n"  
                                        " 101       101  010  101  010  101  010  101  010 \n"  
                                        " 011121    0101101   0101101   010  101  0101101  \n"  
                                        " 111112    110101    110101    101  111  110101   \n"  
                                        " 112       112 211   112 211   112  111  112 211  \n"  
                                        " 212       212  121  212  121  212  121  212  121 \n"  
                                        "  22 2222  22   222  22   222  22222 22  22   222 \n"  
                                        " 2 22 22    2   2 2   2   2 2   2 2  2    2   2 2 \n" 
                                , 2, CENTER_ALIGN, AUTO_SIZE);
                                cursor = printAt(&CON_PRINT_ERROR_RED, "\n File doesn't exist or you don't have access to it \n",
                                                 cursor.row + 3, CENTER_ALIGN, AUTO_SIZE);
                                switch(userMenu(NULL, &MENU_VERT_RED, cursor.row + 3, CENTER_ALIGN, AUTO_SIZE, 2,
                                         "       ⭯ RETRY ",
                                         " 🢠 BACK TO LIST MENU ")){
                                    //
                                    case 0:{
                                        retry = 1;
                                        leave = 0;
                                        break;
                                    }
                                    case 1:{
                                        retry = 1;
                                        leave = 1;
                                        break;
                                    }
                                }
                            }     
                        }
                        if (!retry && leave){
                            vanish(&COLOR_DEEP_BLACK);
                            providerListToFile(&mainList, input);
                            cursor = printHeader(&HEADER_BIG_MSG_ALIGATOR2_GREEN, "\n"
                                " ::::::::  :::    :::  ::::::::   ::::::::  ::::::::::  ::::::::   ::::::::   \n"
                                " :+:    :+: :+:    :+: :+:    :+: :+:    :+: :+:        :+:    :+: :+:    :+: \n" 
                                " +:+        +:+    +:+ +:+        +:+        +:+        +:+        +:+        \n" 
                                " +#++:++#++ +#+    +:+ +#+        +#+        +#++:++#   +#++:++#++ +#++:++#++ \n" 
                                "        +#+ +#+    +#+ +#+        +#+        +#+               +#+        +#+ \n" 
                                " #+#    #+# #+#    #+# #+#    #+# #+#    #+# #+#        #+#    #+# #+#    #+# \n" 
                                "  ########   ########   ########   ########  ##########  ########   ########  \n"
                            , 2, CENTER_ALIGN, AUTO_SIZE);
                        cursor = printAt(&CON_PRINT_MSG_GREEN, "\n Project was successfully saved \n",
                                         cursor.row + 3, CENTER_ALIGN, AUTO_SIZE); 
                        userMenu(NULL, &MENU_VERT_GREEN, cursor.row + 3, CENTER_ALIGN, AUTO_SIZE, 1,
                                         " PRESS ENTER TO CONTINUE ");
                        }
                    }
                    break;
                }
                case 6:{ //  BACK TO MAIN MENU 
                    vanish(&COLOR_DEEP_BLACK);
                    cursor = printAt(&CON_PRINT_MSG_GREEN, "\n       All unsaved data will be lost \n"
                                                           " Are you sure you want to go back to main menu? \n",
                                                  2, CENTER_ALIGN, AUTO_SIZE);
                    if(userMenu(NULL, &MENU_HORI_GREEN, cursor.row + 2, CENTER_ALIGN, AUTO_SIZE, 2,
                        "✔ CONFIRM ", "🗙  CANCEL ") == 0){
                        freeProviderListDyn(&mainList);
                        goToMain = 1;
                    }
                    break;
                }
                case 7:{ //  EXIT 
                    exitProgram(GREEN_THEME, EXIT_SUCCESS, " All unsaved data will be lost \n"
                                                           " Are you sure you want to exit? ", 
                                                            (void(*)(void*))freeProviderListDyn, 1, &mainList);
                }
            }
        }
    // rubicon (while(1))
    }
    return EXIT_SUCCESS;
}